-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.0.17-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Dumping data for table tmpl.tbl_sliders: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_sliders` DISABLE KEYS */;
INSERT INTO `tbl_sliders` (`slide_id`, `slide_title`, `slide_content`, `slide_image_low`, `slide_image_high`, `slide_url`, `slide_status`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(3, 'Ảnh thiên hà 1', 'Ảnh thiên hà 1', 'public/upload/images/slider/low/anh-thien-ha-1-5680cca250984.jpg', 'public/upload/images/slider/high/anh-thien-ha-1-5680cca250984.jpg', 'iraq-tung-don-huy-diet-is-o-thanh-pho-chien-luoc-ramadi', 1, 1, '0000-00-00 00:00:00', '2015-12-28 12:46:10', '2015-12-28 15:33:20'),
	(4, 'Ảnh thiên hà 2', 'Ảnh thiên hà 2', 'public/upload/images/slider/low/anh-thien-ha-2-5680d0410014e.jpg', 'public/upload/images/slider/high/anh-thien-ha-2-5680d0410014e.jpg', 'nga-tuyen-bo-co-the-xuyen-thung-la-chan-phong-thu-ten-lua-my', 1, 1, '0000-00-00 00:00:00', '2015-12-28 13:01:37', '2015-12-28 15:33:18'),
	(5, 'Ảnh thiên hà 3', 'Ảnh thiên hà 3', 'public/upload/images/slider/low/anh-thien-ha-3-5680dbbb6c9a7.jpg', 'public/upload/images/slider/high/anh-thien-ha-3-5680dbbb6c9a7.jpg', 'van-gaal-trut-gian-len-truyen-thong,-bo-ngang-hop-bao', 1, 1, '0000-00-00 00:00:00', '2015-12-28 13:50:35', '2015-12-28 15:33:15'),
	(6, 'Ảnh thiên hà 4', 'Ảnh thiên hà 4', 'public/upload/images/slider/low/anh-thien-ha-4-5680de2318bf0.jpg', 'public/upload/images/slider/high/anh-thien-ha-4-5680de2318bf0.jpg', 'putin:-nga-se-cung-co-vu-khi-hat-nhan-de-ran-de', 1, 1, '0000-00-00 00:00:00', '2015-12-28 14:00:51', '2015-12-28 15:33:12'),
	(7, 'Ảnh thiên hà 5', 'Ảnh thiên hà 5', 'public/upload/images/slider/low/anh-thien-ha-5-5680de8b084ed.jpg', 'public/upload/images/slider/high/anh-thien-ha-5-5680de8b084ed.jpg', 'vi-sao-phat-ngon-ve-quan-doi-my-cua-hoa-hau-hoan-vu-gay-tranh-cai', 1, 1, '0000-00-00 00:00:00', '2015-12-28 14:02:35', '2015-12-28 15:33:09');
/*!40000 ALTER TABLE `tbl_sliders` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
