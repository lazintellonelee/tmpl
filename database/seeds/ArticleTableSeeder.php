<?php

use Illuminate\Database\Seeder;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_users')->insert(array(
            array(
                'article_title'         	=> 'Trên tay Dell XPS 15 (2015)',
                'article_description'       => 'Trên tay Dell XPS 15 (2015): viền màn hình 15" 4K siêu mỏng, nhỏ như máy 14", Thunderbolt 3 / USB-C',
                'article_content'     		=> '<p>Hôm nay mình ghé Microsoft Store thì thấy được chiếc&nbsp;<a href="https://tinhte.vn/tags/dell-xps-15/" title="">Dell XPS 15</a>&nbsp;đời mới (2015). Tương tự như bản&nbsp;<a href="https://tinhte.vn/tags/xps-13-2/" title="">XPS 13</a>&nbsp;lúc trước, viền màn hình của&nbsp;<a href="https://tinhte.vn/tags/xps-15/" title="">XPS 15</a>&nbsp;mới rất mỏng với 2 tác dụng chính: vừa làm đẹp về mặt thẩm mĩ, vừa giúp máy nhỏ gọn hơn (máy 15&quot; mà chỉ nhỏ như một cái 14&quot; bình thường). Màn hình này cũng có độ phân giải lên tới 4K chứ không chỉ là Quad HD+ 3200 x 1800, khá là ấn tượng. Thiết kế của XPS 15 cũng rất đẹp và độc đáo với rất nhiều nét được kế thừa từ người anh XPS 13, ví dụ như vỏ trên dưới bằng nhôm, lớp hoàn thiện với hoa văn carbon vừa đẹp vừa êm tay.</p>

												<div class="embededContent oembed-provider- oembed-provider-youtube" data-align="center" data-maxheight="500" data-maxwidth="700" data-oembed="https://youtu.be/8jq9LsciFOU" data-oembed_provider="youtube" data-resizetype="responsive" style="text-align: center;"><iframe allowfullscreen="true" allowscriptaccess="always" frameborder="0" height="349" scrolling="no" src="//www.youtube.com/embed/8jq9LsciFOU?wmode=transparent&amp;jqoemcache=XfkRv" width="425"></iframe></div>

												<p>Hiện nay ít có laptop nào trên thị trường có viền màn hình mỏng như là gia đình XPS mới của&nbsp;<a href="https://tinhte.vn/tags/dell/" title="">Dell</a>&nbsp;(hãng gọi đây là InfinityEdge). Phần viền này chỉ mỏng như màn hình điện thoại nên nhìn cực kì ấn tượng, nhất là khi tấm nền của XPS 15&quot; lên đến 15,6&quot; thì còn đã hơn cả XPS 13 hồi trước. Như mình đã nói ở trên, ngoài việc làm cho máy đẹp hơn thì viền màn hình siêu mỏng còn giúp thu gọn kích thước laptop. Ở đây Dell XPS 15 mặc dù màn hình 15&quot; nhưng chỉ lớn như một cái máy 14&quot; thôi, tức là bạn sẽ mang vác nó một cách dễ dàng hơn, tiện lợi hơn. Mình biết có rất nhiều anh em thích làm việc trên màn hình to nhưng lại ngại mang vác, nhất là khi đi xa nên đành thỏa hiệp chọn bản 13&quot;. Giờ thì có Dell XPS 15 rồi nên anh em sẽ có thêm một lựa chọn ngon lành hơn.<br />
												<br />
												Màn hình nãy cũng không phải là màn hình Full-HD bình thường mà nó sở hữu độ phân giải lên đến 4K và có thêm tính năng cảm ứng. Với độ phân giải cao như thế này thì chúng ta có thể sử dụng máy rất đã, không còn thấy hiện tượng rỗ pixel nữa. Máy độ phân giải 2560 x 1440 là đã không còn thấy rồi chứ nói gì đến 4K. Sắp tới khi mà các thiết bị ghi hình 4K trở nên đại tràn như Full-HD hiện nay thì bạn sẽ thấy màn hình này hữu ích ra sao, còn hiện tại chủ yếu là để xử lý ảnh và nhìn các đối tượng cho đã mắt.<br />
												<br />
												Dell XPS 15 sở hữu vỏ nhôm nhưng không nguyên khối, chỉ có phần nắp và phần mặt đáy là làm từ nhôm mà thôi. Tuy nhiên, nói như vậy không có nghĩa là Dell XPS 15 có chất lượng hoàn thiện kém mà ngược lại máy cầm lên rất đã, sướng, chắc chắn. Mặt dưới của máy có sẵn hai thanh cao su giúp đáy XPS 15 không bị trầy khi bạn có lỡ kéo lê máy trên bề mặt nào đó. Ở đây còn có cái nắp ghi chữ XPS, bên dưới là số serial và các biểu tượng pháp lý. Mình thích cách xử lý này, nó giúp chúng ta thoát khỏi những cái tem xấu xì thường thấy trên những chiếc máy tính Windows (máy người ta đang đẹp mà lại dán tem lên thấy ghê). Thế nhưng, do để nhiều lỗ tản nhiệt rồi lại có 2 thanh cao su rồi cái nắp này nên nhìn mặt dưới máy khá rối.<br />
												<br />
												<img alt="Tinhte_tren_tay_Dell_XPS_15_doi_2015_4K_vien_mong-23." src="https://tinhte.cdnforo.com/store/2016/01/3585640_Tinhte_tren_tay_Dell_XPS_15_doi_2015_4K_vien_mong-23.jpg" /><br />
												<br />
												Ngay chỗ chiếu nghỉ tay và chạy dọc lên hầu hết phần body của XPS 15 là lớp hoàn thiện với hoa văn carbon. Nó giúp ít bám mồ hôi hơn và rờ lên cũng êm ái hơn. Chi tiết này thì ở Dell XPS 13 cũng có và mình đã thích từ trước rồi. Gần đó là touchpad với diện tích rộng rãi, rộng hơn khá nhiều so với XPS 13 và tất nhiên là có hỗ trợ multi touch. Bàn phím của XPS 15 vẫn sử dụng kiểu chiclet với đèn nền bên dưới.<br />
												<br />
												<img alt="Tinhte_tren_tay_Dell_XPS_15_doi_2015_4K_vien_mong-6." src="https://tinhte.cdnforo.com/store/2016/01/3585623_Tinhte_tren_tay_Dell_XPS_15_doi_2015_4K_vien_mong-6.jpg" /><br />
												<br />
												Dell XPS 15 còn có một chi tiết rất đáng giá: Thunderbolt 3 / USB-C. Có dấu gạch như vậy là bởi vì Thunderbolt 3 đã chuyển sang sử dụng phần cổng kết nối của USB-C rồi chứ không còn xài cổng mini DisplayPort như hồi trước nữa. Kết nối này cho tốc độ tối đa là 40Gbps, nhanh gấp đôi so với Thunderbolt 2 mà lại có ưu điểm là cắm cáp mặt nào cũng được, hỗ trợ cả việc truyền điện, tín hiệu, hình ảnh,... Trong thời gian tới khi phụ kiện USB-C hoặc Thunderbolt 3 trở nên phổ biến thì anh em dùng XPS 15 coi như đã có sẵn kết nối này, không còn phải lo gì nữa.<br />
												<br />
												<img alt="Tinhte_tren_tay_Dell_XPS_15_doi_2015_4K_vien_mong-19." src="https://tinhte.cdnforo.com/store/2016/01/3585636_Tinhte_tren_tay_Dell_XPS_15_doi_2015_4K_vien_mong-19.jpg" /><br />
												<br />
												Mặc dù sở hữu thân hình mỏng nhẹ nhưng không đồng nghĩa với việc XPS 15 yếu. Cấu hình bên trong nó bao gồm CPU Core i5 Skylake (có thể nâng thành Core i7), card đồ họa rời GTX 960M, RAM 8GB (hỗ trợ nâng thành 16GB) và ổ SSD 256 / 512GB. Các cổng kết nối thông dụng như USB 3.0, HDMI cũng có đủ cả. Giá cho bản Core i5 mà mình trên tay ở đây là 1699$, nếu mua bản cấu hình mạnh hơn thì bạn phải trả nhiều tiền hơn. Cũng có bản rẻ hơn, chỉ 999$ thôi, khi đó thì chip yếu hơn, màn hình chỉ Full-HD.<br />
												<br />
												<i>Cấu hình cơ bản của&nbsp;<a href="https://tinhte.vn/tags/dell-xps-15-2015/" title="">Dell XPS 15 (2015)</a></i></p>

												<ul>
													<li>Màn hình: 15,6&quot; độ phân giải 4K (tùy chọn Full-HD), màn hình gương (Full-HD có bản màn hình mờ), cảm ứng</li>
													<li>CPU: CPU Core i5 Skylake</li>
													<li>GPU: NVIDIA Geforce GTX 960M</li>
													<li>RAM: 8GB</li>
													<li>Pin: 16 tiếng</li>
													<li>Hệ điều hành: Windows 10</li>
													<li>Tính năng đặc biệt: viền màn hình rất mỏng</li>
													<li>Kết nối: Bluetooth 4.0, Wi-Fi a/b/g/n/ac, USB, HDMI, Thunderbolt 3 / USB-C</li>
													<li>Bộ nhớ trong/Thẻ nhớ: SSD 256GB hoặc 512GB, có khe SD</li>
													<li>Độ mỏng: 11-17mm</li>
													<li>Trọng lượng: khoảng 2kg</li>
												</ul>
												',
                'article_author'         	=> 'Duy Luân',
                'article_source_name'    	=> 'Trên tay Dell XPS 15 (2015)',
                'article_source_url'       	=> 'https://tinhte.vn/threads/tren-tay-dell-xps-15-2015-vien-man-hinh-15-4k-sieu-mong-nho-nhu-may-14-thunderbolt-3-usb-c.2541304/',
                'article_url'       		=> 'tren-tay-dell-xps-15-(2015)',
				'article_thumbnail_url'		=> '',
				'article_thumbnail_image'   => 'public/upload/images/article/tren-tay-dell-xps-15-(2015)-56932e3e79877.jpg',
				'article_status'       		=> 0,
				'article_verified'       	=> 0,
				'article_view'       		=> 0,
				'created_by'       			=> 1,
				'deleted_at'       			=> '',
				'created_at'       			=> '2016-01-11 10:59:13',
				'updated_at'       			=> '2016-01-11 11:23:26'
            ),
            array(
                'article_title'         	=> '[Trên tay] Surface Book',
                'article_description'       => '[Trên tay] Surface Book: máy đẹp nhưng nặng, bàn phím ngon, bản lề ngầu nhưng không cứng, giá cao',
                'article_content'     		=> '<p><a href="https://tinhte.vn/tags/surface-book/" title="">Surface Book</a>&nbsp;là chiếc&nbsp;<a href="https://tinhte.vn/tags/may-tinh/" title="">máy tính</a>&nbsp;cao cấp nhất của&nbsp;<a href="https://tinhte.vn/tags/microsoft/" title="">Microsoft</a>, cao hơn cả Surface Pro 4 và có một cái bản lề vô cùng ảo diệu. Cảm giác đầu tiên của khi cầm máy lên là máy hơi nặng so với các máy 13&quot; nhưng chất lượng hoàn thiện tổng thể khá tốt, bàn phím rất nẩy, bàn rê mượt mà, màn hình rất mịn và nổi, cấu hình cao, máy chạy nhanh, có GPU rời nhưng bù lại cái giá cũng khá chát so với mặt bằng chung,&nbsp;<a href="http://www.amazon.com/s/ref=as_li_ss_tl?_encoding=UTF8&amp;camp=1789&amp;creative=390957&amp;field-keywords=microsoft%20surface%20book&amp;linkCode=ur2&amp;rh=i%3Aaps%2Ck%3Amicrosoft%20surface%20book&amp;tag=fefewfrgjwe-20&amp;url=search-alias%3Daps&amp;linkId=FB67PR2APR4PV3MZ" target="_blank">từ 1499$ là bản rẻ nhất tới 3199$</a>.</p>

												<div class="embededContent oembed-provider- oembed-provider-youtube" data-align="center" data-maxheight="500" data-maxwidth="700" data-oembed="https://youtu.be/s4WQbKug6uc" data-oembed_provider="youtube" data-resizetype="responsive" style="text-align: center;"><iframe allowfullscreen="true" allowscriptaccess="always" frameborder="0" height="349" scrolling="no" src="//www.youtube.com/embed/s4WQbKug6uc?wmode=transparent&amp;jqoemcache=fxB9R" width="425"></iframe></div>

												<p>Surface Book vẫn là một chiếc&nbsp;<a href="https://tinhte.vn/tags/tablet/" title="">tablet</a>&nbsp;lai máy tính với phần màn hình có thể tháo rời. Cách tháo màn hình bây giờ khá tân tiến nhờ có phím cứng ở góc phải bàn phím điều khiển bằng cơ điện, chỉ cần nhấn giữ phím này, 2 tiếng tách vang lên kèm với một thông báo xuất hiện trên màn hình là bạn đã có thể nhấc máy ra. Nói là nhấc lên chứ thật ra bạn vẫn phải một tay giữ bàn phím một tay nhấc máy lên, hơi khựng chỗ này chứ chưa mượt mà. Bàn phím nối với màn hình phải thông qua tới 5 cái ngàm, 2 ngàm ở hai đầu là 2 ngàm giữ màn hình, còn 3 cái ngàm ở chính giữa dùng để chuyển dữ liệu và pin giữa hai thành phần. Khi muốn gắn lại, bạn để màn hình gần lại ngàm, hai ngàm ở đầu có nam châm nên sẽ tự hút lấy nhau.<br />
												&nbsp;</p>

												<p><img alt="surface-book-tinhte-2." src="https://tinhte.cdnforo.com/store/2015/11/3544391_surface-book-tinhte-2.jpg" />​</p>

												<p><b>Nói thêm về cái bản lề</b><br />
												Bản lề làm bằng kim loại chứ không phải cao su hay bất cứ vật liệu dẻo vào hết. Nhìn vào bạn thấy nó bị bẻ cong lúc gập máy lại nhưng thực tế không có chi tiết nào bị bẻ cong cả. Bản lề được gộp từ nhiều trục lại với nhau, giống như lấy nhiều cây đũa bó gộp lại vậy, khi gập màn hình đến đâu thì trục/cây đũa chỗ đó sẽ tự xoay tại chỗ và cứ thế tiếp tục cho đến khi cái trục/cây đũa cuối cùng xoay hết thì cũng là lúc màn hình gập lại 100%. Cái hay là Microsoft đã khéo léo thiết kế toàn bộ cụm xoay này rất tinh tế và sắp xếp hợp lý, làm cho khi xoay sẽ thấy nó rất ảo diệu chứ không giống bất kỳ chiếc&nbsp;<a href="https://tinhte.vn/tags/laptop/" title="">laptop</a>&nbsp;nào trước đây.<br />
												&nbsp;</p>

												<p><img alt="surface-book-tinhte-36." src="https://tinhte.cdnforo.com/store/2015/11/3544425_surface-book-tinhte-36.jpg" />​</p>

												<p><b>Phần màn hình</b><br />
												Kích thước 13.5&quot; độ phân giải siêu đẹp 3.000 x 2.000, hình ảnh mịn màng và rất nổi, màu sắc tươi tắn, cả phía trước lẫn sau đều có camera, 2 loa ngoài nằm ở mặt trước gần cạnh trên, nút nguồn và volume cũng nằm ở cạnh trên màn hình. Trên máy không hề có bất kỳ cổng kết nối nào ngoại trừ cổng tai nghe 3.5mm ở cạnh phải. Màn hình cũng là một tablet độc lập nên bạn có thể dùng nó không cần đến bàn phím.<br />
												&nbsp;</p>

												<p><img alt="surface-book-tinhte-20." src="https://tinhte.cdnforo.com/store/2015/11/3544409_surface-book-tinhte-20.jpg" />​</p>

												<p><b>Phần bàn phím/dock</b><br />
												Ngoài việc là bàn phím nó còn chứa nhiều cổng kết nối như 2 x USB 3.0, mini DisplayPort, thẻ SD và cổng sạc chung duy nhất cho cả màn hình lẫn bàn phím. Trong bàn phím này từ phiên bản 1.899 USD trở lên còn được tích hợp card đồ họa rời của NVIDIA và dĩ nhiên là nó cũng có pin dự phòng bên trong, giúp máy tính xài được lâu hơn khi cắm vào bàn phím.<br />
												<br />
												Bàn phím có chất lượng rất cao, độ nổi cao và đàn hồi cực tốt, mỗi lần gõ xuống là ngay lập tức có một lực ngược chiều đẩy lên lại cho nên bạn có thể bấm rất nhẹ nhàng và không cần dùng nhiều lực. Kích thước phím cũng khá to, khoảng cách hợp lý.<br />
												&nbsp;</p>

												<p><img alt="surface-book-tinhte-9." src="https://tinhte.cdnforo.com/store/2015/11/3544398_surface-book-tinhte-9.jpg" />​</p>

												<p>Bàn rê cũng mượt, cảm giác rê không bị rít nhiều, tốc độ phản hồi giữa bàn rê với máy rất nhanh nên mọi thao tác đều rất trơn tru, có thể dùng 2 ngón để cuộn và nhấp chuột phải như bên MacBook nên hoàn toàn có thể dùng để thay thế cho chuột rời.<br />
												<br />
												Nhìn chung thì Surface Book là một chiếc máy tính đẹp nhưng hơi nặng, nếu phải đem cả máy lẫn bàn phím để di chuyển thì chưa hay lắm. Vỏ làm bằng hợp kim có độ hoàn thiện cao, bản thân cái bản lề thì rất chắc chắn nhưng màn hình khi gắn vào thì không đủ cứng mà vẫn bị rung lắc khá nhiều. Bản lề này cũng không mở 180 hay 360 độ được. Ngoài ra bàn phím máy cũng rất tốt, bấm êm, cấu hình cao giúp máy chạy mượt, có GPU để xử lý các công việc nặng nhưng đáng tiếc là giá hơi cao, giá thấp nhất không có GPU là 1.499 USD, muốn có GPU thì phải bỏ ra từ 1.899 USD trở lên (chưa thuế), một mức giá rất đáng phải suy nghĩ nếu đem so với MacBook Pro Retina.<br />
												<br />
												Cám ơn anh&nbsp;<a href="https://www.facebook.com/benexttoday" target="_blank">KTS Quân Nguyễn và BeNext</a>&nbsp;đã cho mình mượn máy&nbsp;<a href="https://tinhte.vn/tags/tren-tay/" title="">trên tay</a>.<br />
												&nbsp;</p>

												<p><b>Xem thêm:&nbsp;</b><br />
												<a href="https://tinhte.vn/threads/so-cau-hinh-surface-book-surface-pro-4-surface-pro-3.2515066/">So cấu hình: Surface Book - Surface Pro 4 - Surface Pro 3</a>​</p>

												<p>&nbsp;</p>

												<p><img alt="surface-book-tinhte-1." src="https://tinhte.cdnforo.com/store/2015/11/3544390_surface-book-tinhte-1.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-3." src="https://tinhte.cdnforo.com/store/2015/11/3544392_surface-book-tinhte-3.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-4." src="https://tinhte.cdnforo.com/store/2015/11/3544393_surface-book-tinhte-4.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-5." src="https://tinhte.cdnforo.com/store/2015/11/3544394_surface-book-tinhte-5.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-6." src="https://tinhte.cdnforo.com/store/2015/11/3544395_surface-book-tinhte-6.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-7." src="https://tinhte.cdnforo.com/store/2015/11/3544396_surface-book-tinhte-7.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-8." src="https://tinhte.cdnforo.com/store/2015/11/3544397_surface-book-tinhte-8.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-10." src="https://tinhte.cdnforo.com/store/2015/11/3544399_surface-book-tinhte-10.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-11." src="https://tinhte.cdnforo.com/store/2015/11/3544400_surface-book-tinhte-11.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-12." src="https://tinhte.cdnforo.com/store/2015/11/3544401_surface-book-tinhte-12.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-13." src="https://tinhte.cdnforo.com/store/2015/11/3544402_surface-book-tinhte-13.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-14." src="https://tinhte.cdnforo.com/store/2015/11/3544403_surface-book-tinhte-14.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-15." src="https://tinhte.cdnforo.com/store/2015/11/3544404_surface-book-tinhte-15.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-16." src="https://tinhte.cdnforo.com/store/2015/11/3544405_surface-book-tinhte-16.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-17." src="https://tinhte.cdnforo.com/store/2015/11/3544406_surface-book-tinhte-17.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-18." src="https://tinhte.cdnforo.com/store/2015/11/3544407_surface-book-tinhte-18.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-19." src="https://tinhte.cdnforo.com/store/2015/11/3544408_surface-book-tinhte-19.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-21." src="https://tinhte.cdnforo.com/store/2015/11/3544410_surface-book-tinhte-21.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-22." src="https://tinhte.cdnforo.com/store/2015/11/3544411_surface-book-tinhte-22.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-23." src="https://tinhte.cdnforo.com/store/2015/11/3544412_surface-book-tinhte-23.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-24." src="https://tinhte.cdnforo.com/store/2015/11/3544413_surface-book-tinhte-24.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-25." src="https://tinhte.cdnforo.com/store/2015/11/3544414_surface-book-tinhte-25.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-26." src="https://tinhte.cdnforo.com/store/2015/11/3544415_surface-book-tinhte-26.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-27." src="https://tinhte.cdnforo.com/store/2015/11/3544416_surface-book-tinhte-27.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-28." src="https://tinhte.cdnforo.com/store/2015/11/3544417_surface-book-tinhte-28.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-29." src="https://tinhte.cdnforo.com/store/2015/11/3544418_surface-book-tinhte-29.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-30." src="https://tinhte.cdnforo.com/store/2015/11/3544419_surface-book-tinhte-30.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-31." src="https://tinhte.cdnforo.com/store/2015/11/3544420_surface-book-tinhte-31.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-32." src="https://tinhte.cdnforo.com/store/2015/11/3544421_surface-book-tinhte-32.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-33." src="https://tinhte.cdnforo.com/store/2015/11/3544422_surface-book-tinhte-33.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-34." src="https://tinhte.cdnforo.com/store/2015/11/3544423_surface-book-tinhte-34.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-35." src="https://tinhte.cdnforo.com/store/2015/11/3544424_surface-book-tinhte-35.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-37." src="https://tinhte.cdnforo.com/store/2015/11/3544426_surface-book-tinhte-37.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-38." src="https://tinhte.cdnforo.com/store/2015/11/3544427_surface-book-tinhte-38.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-39." src="https://tinhte.cdnforo.com/store/2015/11/3544428_surface-book-tinhte-39.jpg" /><br />
												<br />
												<img alt="surface-book-tinhte-40." src="https://tinhte.cdnforo.com/store/2015/11/3544429_surface-book-tinhte-40.jpg" /><br />
												​</p>
',
                'article_author'         	=> 'TDNC',
                'article_source_name'    	=> '[Trên tay] Surface Book: máy đẹp nhưng nặng, bàn phím ngon, bản lề ngầu nhưng không cứng, giá cao',
                'article_source_url'        => 'https://tinhte.vn/threads/tren-tay-surface-book-may-dep-nhung-nang-ban-phim-ngon-ban-le-ngau-nhung-khong-cung-gia-cao.2526547/',
                'article_url'       		=> '[tren-tay]-surface-book',
				'article_thumbnail_url'     => 'https://tinhte.cdnforo.com/store/2015/11/3544433_cv-surface-book-tinhte.jpg',
				'article_thumbnail_image'	=> '',
				'article_status'       		=> 1,
				'article_verified'       	=> 1,
				'article_view'       		=> 1,
				'created_by'       			=> 1,
				'deleted_at'       			=> '',
				'created_at'       			=> '2016-01-11 11:43:32',
				'updated_at'       			=> '2016-01-11 11:43:43'
            )
        ));
    }
}
