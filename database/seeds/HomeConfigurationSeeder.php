<?php

use Illuminate\Database\Seeder;

class HomeConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_home_configuration')->insert(array(
            'home_configuration_title'              => 'My Website',
            'home_configuration_meta_description'   => 'My Website',
            'home_configuration_meta_keywords'      => 'My Website',
            'home_configuration_meta_author'        => 'Lê Huy Vũ'
        ));
    }
}
