<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_users')->insert(array(
            array(
                'user_name'         => 'huyvule92',
                'user_email'        => 'huyvule92@gmail.com',
                'user_password'     => '1589739f7385360f60a7a529918b69ee',
                'user_salt'         => '9c0c0ff8879aab7016d27b9aa5d5aec0',
                'user_fullname'    => 'Lê Huy Vũ',
                'user_type'         => 'admin',
                'user_status'       => 1
            ),
            array(
                'user_name'         => 'dnmt92',
                'user_email'        => 'dnmt92@gmail.com',
                'user_password'     => '1589739f7385360f60a7a529918b69ee',
                'user_salt'         => '9c0c0ff8879aab7016d27b9aa5d5aec0',
                'user_fullname'     => 'Đỗ Nguyễn Minh Trang',
                'user_type'         => 'admin',
                'user_status'       => 1
            )
        ));
    }
}
