<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblArticles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tbl_articles', function (Blueprint $table) {
            $table->increments('article_id');
            $table->string('article_title')->nullable();
            $table->string('article_description')->nullable();
            $table->text('article_content')->nullable();
            $table->string('article_author')->nullable();
            $table->string('article_source_name')->nullable();
            $table->string('article_source_url')->nullable();
            $table->string('article_url')->nullable();
            $table->string('article_thumbnail_url')->nullable();
            $table->string('article_thumbnail_image')->nullable();
            $table->tinyInteger('article_status')->default(0);
            $table->tinyInteger('article_verified')->default(0);
            $table->bigInteger('article_view')->nullable();
            $table->integer('created_by')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tbl_articles');
    }
}
