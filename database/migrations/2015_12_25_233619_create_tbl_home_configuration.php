<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblHomeConfiguration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_home_configuration', function (Blueprint $table) {
            $table->increments('id');
            $table->string('home_configuration_title')->nullable();
            $table->string('home_configuration_shortcut')->nullable();
            $table->string('home_configuration_logo')->nullable();
            $table->text('home_configuration_meta_description')->nullable();
            $table->text('home_configuration_meta_keywords')->nullable();
            $table->text('home_configuration_meta_author')->nullable();
            $table->tinyInteger('home_configuration_refresh_state')->default(0);
            $table->integer('home_configuration_refresh_second')->default(0);
            $table->string('home_facebook')->nullable();
            $table->string('home_google')->nullable();
            $table->string('home_twitter')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_home_configuration');
    }
}
