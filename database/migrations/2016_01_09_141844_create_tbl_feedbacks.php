<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblFeedbacks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tbl_feedbacks', function (Blueprint $table) {
            $table->increments('feedback_id');
            $table->string('feedback_name')->nullable();
            $table->string('feedback_email')->nullable();
            $table->string('feedback_address')->nullable();
            $table->string('feedback_phone')->nullable();
            $table->string("feedback_title")->nullable();
            $table->text('feedback_description')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tbl_feedbacks');
    }
}
