<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblRecruits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_recruits', function (Blueprint $table) {
            $table->increments('recruit_id');
            $table->string('recruit_name')->nullable();
            $table->integer('recruit_quantity')->nullable();
            $table->text('recruit_rank')->nullable();
            $table->text('recruit_type_work')->nullable();
            $table->string('recruit_salary')->nullable();
            $table->text('recruit_experience')->nullable();
            $table->integer('recruit_degree')->nullable();
            $table->integer('gender_id')->nullable();
            $table->text('recruit_address')->nullable();
            $table->text('recruit_major')->nullable();
            $table->text('recruit_description')->nullable();
            $table->text('recruit_opportunity')->nullable();
            $table->text('recruit_requirement')->nullable();
            $table->text('recruit_curriculum_vitae')->nullable();
            $table->string('recruit_form_register')->nullable();
            $table->timestamp('recruit_expire')->nullable();
            $table->tinyInteger('recruit_status')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_recruits');
    }
}
