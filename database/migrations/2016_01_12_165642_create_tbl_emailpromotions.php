<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblEmailpromotions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tbl_emailpromotions', function (Blueprint $table) {
            $table->increments('emailpromotion_id');
            $table->string('emailpromotion_name')->nullable();
            $table->string('emailpromotion_email')->nullable();
            $table->string('emailpromotion_gender')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tbl_emailpromotions');
    }
}
