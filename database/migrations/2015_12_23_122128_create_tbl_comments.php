<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_comments', function (Blueprint $table) {
            $table->bigIncrements('comment_id');
            $table->integer('user_id')->default(0);
            $table->string('user_fullname')->nullable();
            $table->string('user_email')->nullabble();
            $table->string('comment_title')->nullable();
            $table->text('comment_content')->nullable();
            $table->bigInteger('comment_forward_id')->default(0);
            $table->tinyInteger('comment_status')->default(0);
            $table->tinyInteger('comment_verified')->default(0);
            $table->enum('comment_target_type', array('news'))->nulllable();
            $table->bigInteger('cooment_target_id')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_comments');
    }
}
