<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblNewsCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_news_categories', function (Blueprint $table) {
            $table->increments('news_category_id');
            $table->integer('news_category_parent_id')->default(0);
            $table->string('news_category_name')->nullable();
            $table->string('news_category_avatar_high')->nullable();
            $table->string('news_category_avatar_medium')->nullable();
            $table->string('news_category_url')->nullable();
            $table->bigInteger('news_category_view')->nullable();
            $table->tinyInteger('news_category_status')->default(0);
            $table->integer('created_by')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_news_categories');
    }
}
