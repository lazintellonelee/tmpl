<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAdminActions extends Migration
{
    /**
     * Run the migrations.
     * Table Admin Action
     * Properties action for admin group
     * @return void
     */
    public function up()
    {
         Schema::create('tbl_admin_actions', function (Blueprint $table) {
            $table->increments('admin_action_id');
            $table->string('admin_action_name')->nullable();
            $table->char('admin_action_query')->nullable();
            $table->text('admin_action_content')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_admin_actions');
    }
}
