<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblSliders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_sliders', function (Blueprint $table) {
            $table->increments('slide_id');
            $table->string('slide_title')->nullable();
            $table->text('slide_content')->nullable();
            $table->enum('slide_type', array('image','video'))->default('image');
            $table->string('slide_video')->nullable();
            $table->string('slide_image_low')->nullable();
            $table->string('slide_image_high')->nullable();
            $table->string('slide_url')->nullable();
            $table->integer('slide_status')->default(0);
            $table->integer('created_by')->default(0);
            $table->timestamp('deleted_at')->nullale();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_sliders');
    }
}
