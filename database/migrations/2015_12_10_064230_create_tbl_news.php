<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_news', function (Blueprint $table) {
            $table->increments('news_id');
            $table->integer('news_category_id')->default(0);
            $table->string('news_avatar_url')->nullable();
            $table->string('news_avatar_high')->nullable();
            $table->string('news_avatar_low')->nullable();
            $table->string('news_title')->nullable();
            $table->string('news_description')->nullable();
            $table->text('news_content')->nullable();
            $table->string('news_author')->nullable();
            $table->string('news_source_name')->nullable();
            $table->string('news_source_url')->nullable();
            $table->string('news_url')->nullable();
            $table->tinyInteger('news_status')->default(0);
            $table->tinyInteger('news_verified')->default(0);
            $table->integer('news_likes')->nullable();
            $table->bigInteger('news_view')->nullable();
            $table->integer('created_by')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_news');
    }
}
