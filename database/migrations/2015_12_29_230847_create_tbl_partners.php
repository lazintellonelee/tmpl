<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPartners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tbl_partners', function (Blueprint $table) {
            $table->increments('partner_id');
            $table->string('partner_name')->nullable();
            $table->string('partner_email')->nullable();
            $table->string('partner_phone')->nullable();
            $table->string('partner_address')->nullable();
            $table->text('partner_description')->nullable();
            $table->string('partner_homepage')->nullable();
            $table->string('partner_link')->nullable();
            $table->string('partner_thumbnail_url')->nullable();
            $table->string('partner_thumbnail_image')->nullable();
            $table->string('partner_url')->nullable();
            $table->tinyInteger('partner_status')->default(0);
            $table->integer('created_by')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tbl_partners');
    }
}
