<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblFaqs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tbl_faqs', function (Blueprint $table) {
            $table->increments('faq_id');
            $table->text('faq_question')->nullable();
            $table->text('faq_answer')->nullable();
            $table->tinyInteger('faq_status')->default(0);
            $table->integer('created_by')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tbl_faqs');
    }
}
