<?php
    namespace App\Plugins\PHPThumb;
    
    use App\Plugins\PHPThumb\GD;
    
    class Thumbnail
    {
        
        private $filename,
                $destination,
                $GD;
        
        public function __construct($filename, $destination) 
        {
            $this->destination  = $destination;
            $this->filename     = $filename;
            $this->GD           = new GD($destination . $filename);
        }
        
        public function CropAdapter()
        {
            $dimension  = $this->GD->getCurrentDimensions();
            $size       = $dimension['height'];
            if($dimension['width'] < $dimension['height']) {
                $size   = $dimension['width'];
            }
            $this->GD->cropFromCenter((int) $size);
            return $this;
        }
        
        public function Scale($width, $height)
        {
            $this->GD->adaptiveResize($width, $height);
            return $this;
        }
        
        public function SaveWithoutThumb()
        {
            $this->GD->save($this->destination . $this->filename);
            return $this->filename;
        }
        
        public function Save($destination='')
        {
            if($destination != '')
            {
                $this->GD->save($destination . $this->filename);
                return $this->filename;
            }
            $this->GD->save($this->destination . 'thumb_' . $this->filename);
            return 'thumb_' . $this->filename;
        }
    }
?>