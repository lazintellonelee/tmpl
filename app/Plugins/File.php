<?php
// Cái này là của Vũ đại ca viết
namespace App\Plugins;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class File
{
    protected   $file,
                $imageMimetype = array(
                    'image/gif',
                    'image/jpeg',
                    'image/png'
                ),
                $videoMimetype = array(
                    'video/mp4',
                    'video/mpeg'
                );
    
    
    public function setFile(UploadedFile $file) {
        $this->file = $file;
    }
    
    public function isImage()
    {
        if( !in_array($this->file->getClientOriginalExtension(), $this->imageMimetype) &&
            $this->file->getClientOriginalExtension() !== 'jpg' &&
            $this->file->getClientOriginalExtension() !== 'png' && 
            $this->file->getClientOriginalExtension() !== 'gif' )
        {
            return false;
        }
        return true;
    }
    
    public function isVideo()
    {
        if( !in_array($this->file->getClientOriginalExtension(), $this->videoMimetype) &&
            strtolower($this->file->getClientOriginalExtension()) !== 'mp4' &&
            strtolower($this->file->getClientOriginalExtension()) !== 'mpe' && 
            strtolower($this->file->getClientOriginalExtension()) !== 'mpeg' )
        {
            return false;
        }
        return true;
    }
    
    public function move($destination, $fileName)
    {
        $this->file->move($destination, $fileName . '.' . $this->file->getClientOriginalExtension());
        return $fileName . '.' . $this->file->getClientOriginalExtension();
    }
    
    public function delete($destination, $file)
    {
        if(gettype($file) == 'array')
        {
            foreach($file as $item)
            {
                if($item == '') 
                {
                    continue;
                }
                if(file_exists($destination . $item))
                {
                    unlink($destination . $item);
                }
            }
            return true;
        }
        if(gettype($file) == 'string')
        {
            if($file == '')
            {
                return true;
            }
            if(file_exists($destination . $file))
            {
                unlink($destination . $file);
            }
            return true;
        }
    }
    
    public static function BuildImageFromBase64($data, $fileName, $destination)
    {
        $image  = imagecreatefrompng($data);
        imagejpeg($image, $destination . $fileName . '.jpg', 100);
        imagedestroy($image);
        return $fileName . '.jpg';
    }
}