<?php
// Hàm dùng đề check user đã đăng nhập hay chưa
if(!function_exists('check_user_logined')) 
{
    function check_user_logined()
    {
        if(request()->cookie('user_info'))
        {
            return true;
        }
        
        if(request()->session()->has('user_info')) 
        {
            return true;
        }
        
        return false;
    }
}
// Hàm đề lấy thông tin user
if(!function_exists('get_user_info')) 
{
    function get_user_info()
    {
        if(request()->cookie('user_info'))
        {
            return request()->cookie('user_info');
        }
        
        if(request()->session()->has('user_info')) 
        {
            return request()->session()->get('user_info');
        }
    }
}
//Hàm để lấy thông tin user theo key
if(!function_exists('get_user_info_key'))
{
    function get_user_info_key($key)
    {
        $info = get_user_info();
        return array_get($info, $key);
    }
}
//Hàm để lấy user id
if(!function_exists('get_user_id'))
{
    function get_user_id()
    {
        return get_user_info_key('user_id');
    }
}
// Hàm dùng để biến đổi date sang tiếng việt
if(!function_exists('change_vn_date'))
{
    function change_vn_date($timestamp)
    {
        $one_week   = 7*24*3600;
        if(time() - $timestamp > $one_week)
        {
            return date('H:i d/m/Y', $timestamp);
        }
        
        $day        = "";
        switch(date('N', $timestamp)) {
            case "1":
                $day = "Thứ hai";
                break;
            case "2":
                $day = "Thứ ba";
                break;
            case "3":
                $day = "Thứ tư";
                break;
            case "4":
                $day = "Thứ năm";
                break;
            case "5":
                $day = "Thứ sáu";
                break;
            case "6":
                $day = "Thứ bảy";
                break;
            case "7":
                $day = "Chủ nhật";
                break;
        }
        return $day . ', ngày ' . date('d/m/Y', $timestamp) . ' vào lúc ' . date('H:i', $timestamp);
    }
}