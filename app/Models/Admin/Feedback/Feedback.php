<?php

namespace App\Models\Admin\Feedback;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected   $table      = "tbl_feedbacks",
                $primaryKey = "feedback_id",
                $fillable   = array(
                    "feedback_name",
                    "feedback_email",
                    "feedback_address",
                    "feedback_phone",
                    "feedback_title",
                    "feedback_description",
                    "created_by"
                );
    public      $timestamps = true;
    
    public static function GetFeedback(){
        $feedback = self::orderBy('created_at', 'DESC')
                        ->get(array(
                            "feedback_id",
                            "feedback_name",
                            "feedback_email",
                            "feedback_address",
                            "feedback_phone",
                            "feedback_title",
                            "feedback_description",
                            "created_at"
                        ));
        if($feedback){
            return $feedback->toArray();
        }
        return false;
    }
    
    public static function GetFeedbackByID($feedback_id){
        $feedback = self::where('feedback_id', $feedback_id)
                        ->first();
        if($feedback){
            return $feedback->toArray();
        }
        return false;
    }
    
    public static function GetFeedbackInID($arrID){
        $feedback = self::whereIn('feedback_id', $arrID)
                        ->get();
        if($feedback){
            return $feedback->toArray();
        }
        return false;
    }
    
    public static function DeleteInID($arrFeedbackID){
        return  self::whereIn('feedback_id', $arrFeedbackID)
                    ->delete();
    }
}