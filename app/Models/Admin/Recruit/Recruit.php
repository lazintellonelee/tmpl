<?php

namespace App\Models\Recruit;

use Illuminate\Database\Eloquent\Model;

class Recruit extends Model
{
    protected   $table      = "tbl_recruits",
                $primaryKey = "recruit_id",
                $fillable   = array(
                    
                );
    
    public      $timestamps = true;
    
    public static function GetRecruits()
    {
        $recruits = self::get(array(
            'recruit_id',
            'recruit_name',
            'created_at'
        ));
        
        if($recruits)
        {
            return $recruits->toArray();
        }
        return false;
    }
}