<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class User extends Model 
{
    protected   $table      = 'tbl_users',
                $primaryKey = 'user_id',
                $fillable   = array(
                    
                );
    public      $timestamps = true;
    
    public static function GetUserByEmailOrName($user_email, $user_name)
    {
        $user = self::where('user_email', $user_email)
                    ->orWhere('user_name', $user_name)
                    ->first();
        
        if($user) 
        {
            return $user->toArray();
        }
        return false;
    }
}