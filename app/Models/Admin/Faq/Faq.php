<?php

namespace App\Models\Admin\Faq;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model 
{
    protected   $table      = "tbl_faqs",
                $primaryKey = "faq_id",
                $fillable   = array(
                    "faq_question",
                    "faq_answer",
                    "faq_status",
                    "created_by",
                    "deleted_at"
                );
    public      $timestamps = true;
    
    public static function GetFaq(){
        $faq = self::orderBy('created_at', 'DESC')
                       ->get(array(
                            "faq_id",
                            "faq_question",
                            "faq_answer",
                            "faq_status",
                            "created_at"
                        ));
        if($faq){
            return $faq->toArray();
        }
        return false;
    }
    
    public static function GetFaqByID($faq_id){
        $faq = self::where('faq_id', $faq_id)
                       ->first();
        if($faq){
            return $faq->toArray();
        }
        return false;
    }
    
    public static function GetFaqInID($arrID){
        $faq = self::whereIn('faq_id', $arrID)
                       ->get();
        if($faq){
            return $faq->toArray();
        }
        return false;
    }
    
    public static function AddFaq($data){
        return self::create($data);
    }
    
    public static function EditFaqByID($faqID, $data){
        $data = self::FilterDataUpdate($data);
        if ($data){
            return  self::where('faq_id', $faqID)
                        ->update($data);
        }
        return false;
    }
    
    public static function DeleteInID($arrFaqID){
        return  self::whereIn('faq_id', $arrFaqID)
                    ->delete();
    }
    
    public static function FilterDataUpdate($data){
        $update_data = array();
        
        if(isset($data['faq_question'])){
            $update_data['faq_question'] = $data['faq_question'];
        }
        
        if(isset($data['faq_answer'])){
            $update_data['faq_answer'] = $data['faq_answer'];
        }
        
        if(isset($data['faq_status'])){
            $update_data['faq_status'] = $data['faq_status'];
        }
        
        if(isset($data['created_by'])){
            $update_data['created_by'] = $data['created_by'];
        }
        
        if(isset($data['deleted_at'])){
            $update_data['deleted_at'] = $data['deleted_at'];
        }
        
        return $update_data;
    }
}