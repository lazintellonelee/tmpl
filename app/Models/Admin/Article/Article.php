<?php

namespace App\Models\Admin\Article;

use Illuminate\Database\Eloquent\Model;

class Article extends Model 
{
    protected   $table      = 'tbl_articles',
                $primaryKey = 'article_id',
                $fillable   = array(
                    'article_title',
                    'article_description',
                    'article_content',
                    'article_author',
                    'article_source_name',
                    'article_source_url',
                    'article_url',
                    'article_thumbnail_url',
                    'article_thumbnail_image',
                    'article_status',
                    'article_verified',
                    'article_view',
                    'created_by'
                );
    public      $timestamps = true;
    
    public static function GetArticle()
    {
        $article = self::orderBy('created_at', 'DESC')
                    ->get(array(
                        'article_id',
                        'article_title',
                        'article_description',
                        'article_content',
                        'article_author',
                        'article_source_name',
                        'article_source_url',
                        'article_url',
                        'article_thumbnail_url',
                        'article_thumbnail_image',
                        'article_status',
                        'article_verified',
                        'article_view',
                        'created_at'
                    ));
        if($article)
        {
            return $article->toArray();
        }
        return false;
    }
    
    public static function GetArticleByID($article_id){
        $article = self::where('article_id', $article_id)
                       ->first();
        
        if($article){
            return $article->toArray();
        }
        return false;
    }
    
    public static function GetArticleInID($arrID){
        $article = self::whereIn('article_id', $arrID)
                    ->get();
        
        if($article){
            return $article->toArray();
        }
        return false;
    }
    
    public static function AddArticle($data){
        return self::create($data);
    }
    
    public static function EditArticleByID($articleID, $data){
        $data = self::FilterDataUpdate($data);
        return  self::where('article_id', $articleID)
                    ->update($data);
    }
    
    public static function DeleteInID($arrArticleID)
    {
        return  self::whereIn('article_id', $arrArticleID)
                    ->delete();
    }
    
    public static function FilterDataUpdate($data)
    {
        $update_data = array();
        if(isset($data['article_title'])) $update_data['article_title'] = $data['article_title'];
        if(isset($data['article_description'])) $update_data['article_description'] = $data['article_description'];
        if(isset($data['article_content'])) $update_data['article_content'] = $data['article_content'];
        if(isset($data['article_author'])) $update_data['article_author'] = $data['article_author'];
        if(isset($data['article_source_name'])) $update_data['article_source_name'] = $data['article_source_name'];
        if(isset($data['article_source_url'])) $update_data['article_source_url'] = $data['article_source_url'];
        if(isset($data['article_url'])) $update_data['article_url'] = $data['article_url'];
        if(isset($data['article_thumbnail_url'])) $update_data['article_thumbnail_url'] = $data['article_thumbnail_url'];
        if(isset($data['article_thumbnail_image'])) $update_data['article_thumbnail_image'] = $data['article_thumbnail_image'];
        if(isset($data['article_status'])) $update_data['article_status'] = $data['article_status'];
        if(isset($data['article_verified'])) $update_data['article_verified'] = $data['article_verified'];
        if(isset($data['article_view'])) $update_data['article_view'] = $data['article_view'];
        if(isset($data['created_by'])) $update_data['created_by'] = $data['created_by'];
        if(isset($data['deleted_at'])) $update_data['deleted_at'] = $data['deleted_at'];
        return $update_data;
    }
}