<?php

namespace App\Models\Admin\News;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NewsRelationTags extends Model
{
    protected   $table      = 'tbl_news_relation_tags',
                $primaryKey = 'news_relation_tag_id',
                $fillable   = array(
                    'news_tag_id',
                    'news_id'
                );
    
    public      $timestamps = true;
    
    public static function AddNewsRelationTags($data)
    {
        return DB::table('tbl_news_relation_tags')->insert($data);
    }
    
    public static function GetTagInfoByNewsID($newsID)
    {
        $newsTags = self::join('tbl_news_tags', 'tbl_news_relation_tags.news_tag_id', '=', 'tbl_news_tags.news_tag_id')
                        ->where('tbl_news_relation_tags.news_id', $newsID)
                        ->get();
        
        if($newsTags)
        {
            return $newsTags->toArray();
        }
        return false;
    }
    
    public static function DeleteRelationNewsTag($newsID, $newsTagID)
    {
        return  self::where('news_id', $newsID)
                    ->where('news_tag_id', $newsTagID)
                    ->delete();
    }
    
    public static function DeleteRelationByNewsTagID($newsTagID)
    {
        return  self::where('news_tag_id', $newsTagID)
                    ->delete();
    }
    
    public static function DeleteInNewsID($arrNewsID)
    {
        if(!self::whereIn('news_id', $arrNewsID)->get()->all())
        {
            return true;
        }
        
        return  self::whereIn('news_id', $arrNewsID)
                    ->delete();
    }
    
    public static function DeleteInNewsTagID($arrNewsTagID)
    {
        if(!self::whereIn('news_tag_id', $arrNewsTagID)->get()->all())
        {
            return true;
        }
        
        return  self::whereIn('news_tag_id', $arrNewsTagID)
                    ->delete();
    }
}