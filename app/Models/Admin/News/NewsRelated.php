<?php

namespace App\Models\Admin\News;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NewsRelated extends Model 
{
    protected   $table      = 'tbl_news_related',
                $primaryKey = 'news_related_id',
                $fillable   = array(
                    'news_id',
                    'related_news_id'
                );
    public      $timestamps = true;
    
    public static function AddNewsRelated($data)
    {
        return DB::table('tbl_news_related')->insert($data);
    }
    
    public static function GetRelatedNewsInfoByNewsID($newsID)
    {
        $relatedNews =  self::rightJoin('tbl_news', 'tbl_news.news_id', '=', 'tbl_news_related.related_news_id')
                            ->where('tbl_news_related.news_id', $newsID)
                            ->get(array(
                                'tbl_news.news_id',
                                'tbl_news.news_title'
                            ));
        if($relatedNews)
        {
            return $relatedNews->toArray();
        }
        return false;
    }
    
    public static function DeleteRelated($newsID, $relatedNewsID)
    {
        return  self::where('news_id', $newsID)
                    ->where('related_news_id', $relatedNewsID)
                    ->delete();
    }
    
    public static function DeleteInNewsID($arrNewsID)
    {
        if(!self::whereIn('news_id', $arrNewsID)->get()->all())
        {
            return false;
        }
        
        return  self::whereIn('news_id', $arrNewsID)
                    ->delete();
    }
    
    public static function DeleteInRelatedNewsID($arrRelatedNewsID)
    {
        if(!self::whereIn('related_news_id', $arrRelatedNewsID)->get()->all())
        {
            return false;
        }
        
        return  self::whereIn('related_news_id', $arrRelatedNewsID)
                    ->delete();
    }
}