<?php

namespace App\Models\Admin\News;

use Illuminate\Database\Eloquent\Model;

class News extends Model 
{
    protected   $table      = 'tbl_news',
                $primaryKey = 'news_id',
                $fillable   = array(
                    'news_category_id',
                    'news_avatar_url',
                    'news_avatar_high',
                    'news_avatar_low',
                    'news_title',
                    'news_description',
                    'news_content',
                    'news_author',
                    'news_source_name',
                    'news_source_url',
                    'news_url',
                    'created_by'
                );
    public      $timestamps = true;
    
    public static function GetNews()
    {
        $news = self::orderBy('created_at', 'DESC')
                    ->get(array(
                        'news_id',
                        'news_title',
                        'news_status',
                        'news_verified',
                        'news_avatar_url',
                        'news_avatar_low',
                        'created_at'
                    ));
        if($news)
        {
            return $news->toArray();
        }
        return false;
    }
    
    public static function GetNewsByID($news_id)
    {
        $news = self::where('news_id', $news_id)
                    ->first();
        
        if($news)
        {
            return $news->toArray();
        }
        return false;
    }
    
    public static function GetNewsInID($arrID)
    {
        $news = self::whereIn('news_id', $arrID)
                    ->get();
        
        if($news)
        {
            return $news->toArray();
        }
        return false;
    }
    
    public static function AddNews($data)
    {
        return self::create($data);
    }
    
    public static function EditNewsByID($newsID, $data)
    {
        $data = self::FilterDataUpdate($data);
        return  self::where('news_id', $newsID)
                    ->update($data);
    }
    
    public static function DeleteInID($arrNewsID)
    {
        return  self::whereIn('news_id', $arrNewsID)
                    ->delete();
    }
    
    public static function FilterDataUpdate($data)
    {
        $update_data = array();
        if(isset($data['news_category_id'])) $update_data['news_category_id'] = $data['news_category_id'];
        if(isset($data['news_avatar_url'])) $update_data['news_avatar_url'] = $data['news_avatar_url'];
        if(isset($data['news_avatar_high'])) $update_data['news_avatar_high'] = $data['news_avatar_high'];
        if(isset($data['news_avatar_low'])) $update_data['news_avatar_low'] = $data['news_avatar_low'];
        if(isset($data['news_title'])) $update_data['news_title'] = $data['news_title'];
        if(isset($data['news_description'])) $update_data['news_description'] = $data['news_description'];
        if(isset($data['news_content'])) $update_data['news_content'] = $data['news_content'];
        if(isset($data['news_author'])) $update_data['news_author'] = $data['news_author'];
        if(isset($data['news_source_name'])) $update_data['news_source_name'] = $data['news_source_name'];
        if(isset($data['news_source_url'])) $update_data['news_source_url'] = $data['news_source_url'];
        if(isset($data['news_url'])) $update_data['news_url'] = $data['news_url'];
        if(isset($data['news_status'])) $update_data['news_status'] = $data['news_status'];
        if(isset($data['news_verified'])) $update_data['news_verified'] = $data['news_verified'];
        if(isset($data['news_likes'])) $update_data['news_likes'] = $data['news_likes'];
        if(isset($data['news_view'])) $update_data['news_view'] = $data['news_view'];
        if(isset($data['created_by'])) $update_data['created_by'] = $data['created_by'];
        if(isset($data['deleted_at'])) $update_data['deleted_at'] = $data['deleted_at'];
        return $update_data;
    }
}