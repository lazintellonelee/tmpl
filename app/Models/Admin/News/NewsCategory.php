<?php

namespace App\Models\Admin\News;

use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model
{
    protected   $table      = 'tbl_news_categories',
                $primaryKey = 'news_category_id',
                $fillable   = array(
                    'news_category_parent_id',
                    'news_category_name',
                    'news_category_url',
                    'news_category_avatar_high',
                    'news_category_avatar_medium'
                );
    public      $timestamps = true;
    
    public static function AddNewsCategory($data)
    {
        return  self::create($data);
    }
    
    public static function EditNewsCategoryByID($news_category_id, $data)
    {
        return  self::where('news_category_id', $news_category_id)
                    ->update($data);
    }
    
    public static function GetNewsCategories()
    {
        $newsCategories = self::get();
        if($newsCategories)
        {
            return $newsCategories->toArray();
        }
        return false;
    }
    
    public static function GetNewsCategoryByID($id)
    {
        $newsCategory = self::where('news_category_id', $id)
                            ->first();
        if($newsCategory)
        {
            return $newsCategory->toArray();
        }
        return false;
    }
    
    public static function GetNewsCategoriesInID($arrID)
    {
        $newsCategory = self::whereIn('news_category_id', $arrID)
                            ->get();
        if($newsCategory)
        {
            return $newsCategory->toArray();
        }
        return false;
    }
    
    public static function GetNewsCategoriesSorted($parent_id=0, $lvl=0)
    {
        $categories =   self::where('news_category_parent_id', $parent_id)
                            ->get(array('news_category_id', 'news_category_parent_id', 'news_category_name', 'news_category_status', 'created_at'));
        
        if($categories)
        {
            $categories = $categories->toArray();
            $arr        = array();
            foreach($categories as &$category)
            {
                $category['news_category_name'] = self::AppendText('&nbsp;&nbsp;&nbsp;&nbsp;', $lvl*3) . $category['news_category_name'];
                $arr[]                          = $category;
                $sub_category                   = self::GetNewsCategoriesSorted($category['news_category_id'], $lvl+1);
                if($sub_category) $arr = array_merge($arr, $sub_category);
            }
            return $arr;
        }
        return false;
    }
    
    public static function GetNewsCategoriesOptionHTML($parent_id=0, $news_category_id=0, $noGroup=false, $lvl=0)
    {
        $categories =   self::where('news_category_parent_id', $parent_id)
                            ->get(array('news_category_id', 'news_category_parent_id', 'news_category_name'));
        if($categories)
        {
            $string     = "";
            $categories = $categories->toArray();
            foreach($categories as $category)
            {
                if($noGroup)
                {
                    $string = $string . '<option ' . (($news_category_id!=0&&$news_category_id==$category['news_category_id'])?'selected':'') . ' value="' . $category['news_category_id'] . '">' . self::AppendText('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $lvl*2) . $category['news_category_name'] . '</option>';
                    $string = $string . self::GetNewsCategoriesOptionHTML($category['news_category_id'], $news_category_id, $noGroup, $lvl+1);
                }
                else 
                {
                    
                    $s_string   = self::GetNewsCategoriesOptionHTML($category['news_category_id'], $news_category_id, $noGroup);
                    if($s_string != '')
                    {
                        $string = $string . '<optgroup label="' . $category['news_category_name'] . '">';
                        $string = $string . $s_string;
                        $string = $string . '</optgroup>';
                        continue;
                    }
                    $string     = $string . '<option ' . ($news_category_id!=0&&$news_category_id==$category['news_category_id']?'selected':'') . ' value="' . $category['news_category_id'] . '">' . $category['news_category_name'] . '</option>';
                }
                
            }
            return $string;
        }
        return '';
    }
    
    public static function GetNewsCategoriesTree($parent_id=0)
    {
        $categories =   self::where('news_category_parent_id', $parent_id)
                            ->get(array('news_category_id', 'news_category_parent_id', 'news_category_name'));
        if($categories)
        {
            $categories = $categories->toArray();
            foreach($categories as &$category)
            {
                $child = self::GetNewsCategoriesTree($category['news_category_id']);
                if($child) $category['childrens'] = $child;
            }
            return $categories;
        }
        return false;
    }
    
    public static function DeleteNewsCategoriesInID($arrID)
    {
        return  self::whereIn('news_category_id', $arrID)
                    ->delete();
    }
    
    public static function AppendText($text, $times)
    {
        $string = '';
        for($i=0; $i<$times; $i++) 
        {
            $string = $string . $text;
        }
        return $string;
    }
}