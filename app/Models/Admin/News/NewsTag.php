<?php

namespace App\Models\Admin\News;

use Illuminate\Database\Eloquent\Model;

class NewsTag extends Model
{
    protected   $table      = 'tbl_news_tags',
                $primaryKey = 'news_tag_id',
                $fillable   = array(
                    'news_tag_name',
                    'news_tag_url'
                );
    
    public      $timestamps = true;
    
    public static function GetNewsTags()
    {
        $newsTags = self::get(array(
            'news_tag_id',
            'news_tag_name',
            'news_tag_url',
            'created_at'
        ));
        
        if($newsTags)
        {
            return $newsTags->toArray();
        }
        return false;
    }
    
    public static function GetNewsByID($news_tag_id)
    {
        $newsTag =  self::where('news_tag_id', $news_tag_id)
                        ->first();
        
        if($newsTag) 
        {
            return $newsTag->toArray();
        }
        return false;
    }
    
    public static function AddNewsTag($data)
    {
        return self::create($data);
    }
    
    public static function EditNewsTag($news_tag_id, $data)
    {
        return  self::where('news_tag_id', $news_tag_id)
                    ->update($data);
    }
    
    public static function DeleteNewsTagsInID($arrID)
    {
        return  self::whereIn('news_tag_id', $arrID)
                    ->delete();
    }
}