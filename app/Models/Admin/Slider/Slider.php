<?php

namespace App\Models\Admin\Slider;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model 
{
    protected   $table      = 'tbl_sliders',
                $primaryKey = 'slide_id',
                $fillable   = array(
                    'slide_title',
                    'slide_content',
                    'slide_type',
                    'slide_video',
                    'slide_image_low',
                    'slide_image_high',
                    'slide_url',
                    'created_by'
                );
    
    public      $timestamps = true;
    
    public static function GetSliders()
    {
        $sliders =  self::orderBy('created_at', 'DESC')
                        ->get(array(
                            'slide_id',
                            'slide_title',
                            'slide_status',
                            'slide_image_low',
                            'created_at'
                        ));
        
        if($sliders)
        {
            return $sliders->toArray();
        }
        return false;
    }
    
    public static function GetSlidersActive() 
    {
        $sliders =  self::where('slide_status', 1)
                        ->get();
        
        if($sliders)
        {
            return $sliders->toArray();
        }
        return false;
    }
    
    public static function GetSliderByID($slide_id)
    {
        $slider =   self::where('slide_id', $slide_id)
                        ->first();
        if(!$slider) {
            return false;
        }
        return $slider->toArray();
    }
    
    public static function GetSlidersInID($arrId)
    {
        $sliders =  self::whereIn('slide_id', $arrId)
                        ->get();
        if(!$sliders)
        {
            return false;
        }
        return $sliders->toArray();
    }
    
    public static function AddSlide($data)
    {
        return self::create($data);
    }
    
    public static function EditSlideByID($slide_id, $data)
    {
        $data = self::FilterDataUpdate($data);
        return  self::where('slide_id', $slide_id)
                    ->update($data);
    }
    
    public static function DeleteSlidersInID($arrId)
    {
        return  self::whereIn('slide_id', $arrId)
                    ->delete();
    }
    
    public static function FilterDataUpdate($data)
    {
        $update_data = array();
        if(isset($data['slide_title'])) $update_data['slide_title'] = $data['slide_title'];
        if(isset($data['slide_content'])) $update_data['slide_content'] = $data['slide_content'];
        if(isset($data['slide_image_high'])) $update_data['slide_image_high'] = $data['slide_image_high'];
        if(isset($data['slide_image_low'])) $update_data['slide_image_low'] = $data['slide_image_low'];
        if(isset($data['slide_url'])) $update_data['slide_url'] = $data['slide_url'];
        if(isset($data['slide_status'])) $update_data['slide_status'] = $data['slide_status'];
        return $update_data;
    }
}
