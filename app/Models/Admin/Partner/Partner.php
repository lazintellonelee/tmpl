<?php

namespace App\Models\Admin\Partner;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model 
{
    protected   $table      = "tbl_partners",
                $primaryKey = "partner_id",
                $fillable   = array(
                    "partner_name",
                    "partner_email",
                    "partner_phone",
                    "partner_address",
                    "partner_description",
                    "partner_homepage",
                    "partner_link",
                    "partner_thumbnail_url",
                    "partner_thumbnail_image",
                    'partner_url',
                    "partner_status",
                    "created_by"
                );
    public      $timestamps = true;
    
    public static function GetPartner(){
        $partner = self::orderBy('created_at', 'DESC')
                       ->get(array(
                            "partner_id",
                            "partner_name",
                            "partner_email",
                            "partner_phone",
                            "partner_address",
                            "partner_description",
                            "partner_homepage",
                            "partner_link",
                            "partner_thumbnail_url",
                            "partner_thumbnail_image",
                            "partner_url",
                            "partner_status",
                            "created_at"
                        ));
        if($partner){
            return $partner->toArray();
        }
        return false;
    }
    
    public static function GetPartnerByID($partner_id){
        $partner = self::where('partner_id', $partner_id)
                       ->first();
        if($partner){
            return $partner->toArray();
        }
        return false;
    }
    
    public static function GetPartnerInID($arrID){
        $partner = self::whereIn('partner_id', $arrID)
                       ->get();
        if($partner){
            return $partner->toArray();
        }
        return false;
    }
    
    public static function AddPartner($data){
        return self::create($data);
    }
    
    public static function EditPartnerByID($partnerID, $data){
        $data = self::FilterDataUpdate($data);
        if ($data){
            return  self::where('partner_id', $partnerID)
                        ->update($data);
        }
        return false;
    }
    
    public static function DeleteInID($arrPartnerID){
        return  self::whereIn('partner_id', $arrPartnerID)
                    ->delete();
    }
    
    public static function FilterDataUpdate($data){
        $update_data = array();
        
        if(isset($data['partner_name'])){
            $update_data['partner_name'] = $data['partner_name'];
        }
        
        if(isset($data['partner_email'])){
            $update_data['partner_email'] = $data['partner_email'];
        }
        
        if(isset($data['partner_phone'])){
            $update_data['partner_phone'] = $data['partner_phone'];
        }
        
        if(isset($data['partner_address'])){
            $update_data['partner_address'] = $data['partner_address'];
        }
        
        if(isset($data['partner_description'])){
            $update_data['partner_description'] = $data['partner_description'];
        }
        
        if(isset($data['partner_homepage'])){
            $update_data['partner_homepage'] = $data['partner_homepage'];
        }
        
        if(isset($data['partner_link'])){
            $update_data['partner_link'] = $data['partner_link'];
        }
        
        if(isset($data['partner_thumbnail_url'])){
            $update_data['partner_thumbnail_url'] = $data['partner_thumbnail_url'];
        }
        
        if(isset($data['partner_thumbnail_image'])){
            $update_data['partner_thumbnail_image'] = $data['partner_thumbnail_image'];
        }
        
        if(isset($data['partner_url'])){
            $update_data['partner_url'] = $data['partner_url'];
        }
        
        if(isset($data['partner_status'])){
            $update_data['partner_status'] = $data['partner_status'];
        }
        
        if(isset($data['created_by'])){
            $update_data['created_by'] = $data['created_by'];
        }
        
        if(isset($data['deleted_at'])){
            $update_data['deleted_at'] = $data['deleted_at'];
        }
        
        return $update_data;
    }
}