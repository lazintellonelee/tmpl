<?php

namespace App\Models\Admin\EmailPromotion;

use Illuminate\Database\Eloquent\Model;

class EmailPromotion extends Model
{
    protected   $table      = "tbl_emailpromotions",
                $primaryKey = "emailpromotion_id",
                $fillable   = array(
                    "emailpromotion_name",
                    "emailpromotion_email",
                    "emailpromotion_gender"
                );
    public      $timestamps = true;
    
    public static function GetEmailPromotion(){
        $emailpromotion = self::orderBy('created_at', 'DESC')
                        ->get(array(
                            "emailpromotion_id",
                            "emailpromotion_name",
                            "emailpromotion_email",
                            "emailpromotion_gender",
                            "created_at"
                        ));
        if($emailpromotion){
            return $emailpromotion->toArray();
        }
        return false;
    }
    
    public static function GetEmailPromotionByID($emailpromotion_id){
        $emailpromotion = self::where('emailpromotion_id', $emailpromotion_id)
                              ->first();
        if($emailpromotion){
            return $emailpromotion->toArray();
        }
        return false;
    }
    
    public static function GetEmailPromotionInID($arrID){
        $emailpromotion = self::whereIn('emailpromotion_id', $arrID)
                              ->get();
        if($emailpromotion){
            return $emailpromotion->toArray();
        }
        return false;
    }
    
    public static function DeleteInID($arrEmailPromotionID){
        return  self::whereIn('emailpromotion_id', $arrEmailPromotionID)
                    ->delete();
    }
}