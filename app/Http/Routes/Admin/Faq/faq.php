<?php

Route::group(array(
    'prefix'    => 'faq',
    'namespace' => 'Faq'
), function() {
    Route::get('/home.html', array(
        'as'    => 'admin-faq-home',
        'uses'  => 'IndexController@index'
    ));
    
    Route::get('/{id}/view.html', array(
        'as'    => 'admin-faq-view',
        'uses'  => 'ViewController@index'
    ))->where('id', '[0-9]+');
    
    Route::match(array('get', 'post'), '/add.html', array(
        'as'    => 'admin-faq-add',
        'uses'  => 'AddController@index'
    ));
    
    Route::match(array('get', 'post'), '/{id}/edit.html', array(
        'as'    => 'admin-faq-edit',
        'uses'  => 'EditController@index'
    ))->where('id', '[0-9]+');

    Route::post('/delete.html', array(
        'as'    => 'admin-faq-delete',
        'uses'  => 'DeleteController@index'
    ));
    
    Route::post('/change-status.html', array(
        'as'    => 'admin-faq-change-status',
        'uses'  => 'ChangeStatusController@index'
    ));
    
    Route::post('/get-data-chart-faq-views.html', array(
        'as'    => 'admin-faq-get-chart-faq-views',
        'uses'  => 'ChartController@faqView'
    ));
});
