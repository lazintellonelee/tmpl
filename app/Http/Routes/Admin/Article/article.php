<?php

Route::group(array(
    'prefix'    => 'article',
    'namespace' => 'Article'
), function() {
    Route::get('/home.html', array(
        'as'    => 'admin-article-home',
        'uses'  => 'IndexController@index'
    ));
    
    Route::get('/{id}/view.html', array(
        'as'    => 'admin-article-view',
        'uses'  => 'ViewController@index'
    ))->where('id', '[0-9]+');
    
    Route::match(array('get', 'post'), '/add.html', array(
        'as'    => 'admin-article-add',
        'uses'  => 'AddController@index'
    ));
    
    Route::match(array('get', 'post'), '/{id}/edit.html', array(
        'as'    => 'admin-article-edit',
        'uses'  => 'EditController@index'
    ))->where('id', '[0-9]+');
    
    Route::get('/json-article.html', array(
        'as'    => 'admin-json-article',
        'uses'  => 'IndexController@jsonArticle'
    ));
    
    Route::post('/delete.html', array(
        'as'    => 'admin-article-delete',
        'uses'  => 'DeleteController@index'
    ));
    
    Route::post('/change-status.html', array(
        'as'    => 'admin-article-change-status',
        'uses'  => 'ChangeStatusController@index'
    ));
    
    Route::post('/change-verified.html', array(
        'as'    => 'admin-article-change-verified',
        'uses'  => 'ChangeVerifyController@index'
    ));
    
    Route::post('/get-data-chart-article-views.html', array(
        'as'    => 'admin-article-get-chart-article-views',
        'uses'  => 'ChartController@articleView'
    ));
});
