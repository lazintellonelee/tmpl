<?php

Route::group(array(
    'namespace' => 'Slider',
    'prefix'    => 'slider'
), function() {
    Route::get('/home.html', array(
        'as'    => 'admin-slider-home',
        'uses'  => 'IndexController@index'
    ));
    
    Route::match(array('get','post'), '/add.html', array(
        'as'    => 'admin-slider-add',
        'uses'  => 'AddController@index'
    ));
    
    Route::match(array('get', 'post'), '/{id}/edit.html', array(
        'as'    => 'admin-slider-edit',
        'uses'  => 'EditController@index'
    ))->where('id', '[0-9]+');
    
    Route::post('/delete.html', array(
        'as'    => 'admin-slider-delete',
        'uses'  => 'DeleteController@index'
    ));
    
    Route::post('/change-status.html', array(
        'as'    => 'admin-slider-change-status',
        'uses'  => 'ChangeStatusController@index'
    ));
    
    Route::get('/{type}/get-data-slider-url.html', array(
        'as'    => 'admin-slider-url-get-data',
        'uses'  => 'UrlController@index'
    )); 
});