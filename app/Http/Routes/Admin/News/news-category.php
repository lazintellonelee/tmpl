<?php

Route::group(array(
    'prefix'    => 'news-category',
    'namespace' => 'NewsCategory'
), function() {
    Route::get('/home.html', array(
        'as'    => 'admin-news-category-home',
        'uses'  => 'IndexController@index'
    ));
    
    Route::match(array('get','post'), '/add.html', array(
        'as'    => 'admin-news-category-add',
        'uses'  => 'AddController@index'
    ));
    
    Route::match(array('get','post'), '/{id}/edit.html', array(
        'as'    => 'admin-news-category-edit',
        'uses'  => 'EditController@index'
    ));
    
    Route::post('/delete.html', array(
        'as'    => 'admin-news-category-delete',
        'uses'  => 'DeleteController@index'
    ));
    
    Route::post('/change-status.html', array(
        'as'    => 'admin-news-category-change-status',
        'uses'  => 'ChangeStatusController@index'
    ));
    
    Route::post('/get-data-chart.html', array(
        'as'    => 'admin-news-category-get-data-chart',
        'uses'  => 'ChartController@index'
    ));
});