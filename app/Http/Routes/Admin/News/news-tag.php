<?php

Route::group(array(
    'prefix'    => 'news-tag',
    'namespace' => 'NewsTag'
), function() {
    Route::get('/home.html', array(
        'as'    => 'admin-news-tag-home',
        'uses'  => 'IndexController@index'
    ));
    
    Route::post('/add.html', array(
        'as'    => 'admin-news-tag-add',
        'uses'  => 'AddController@index'
    ));
    
    Route::post('/get.html', array(
        'as'    => 'admin-news-tag-get',
        'uses'  => 'EditController@get'
    ));
    
    Route::post('/edit.html', array(
        'as'    => 'admin-news-tag-edit',
        'uses'  => 'EditController@index'
    ));
    
    Route::post('/delete.html', array(
        'as'    => 'admin-news-tag-delete',
        'uses'  => 'DeleteController@index'
    ));
    
    Route::post('/get-data-chart.html', array(
        'as'    => 'admin-news-tag-get-data-chart',
        'uses'  => 'ChartController@index'
    ));
});