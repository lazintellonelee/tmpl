<?php

Route::group(array(
    'prefix'    => 'news',
    'namespace' => 'News'
), function() {
    Route::get('/home.html', array(
        'as'    => 'admin-news-home',
        'uses'  => 'IndexController@index'
    ));
    
    Route::get('/{id}/view.html', array(
        'as'    => 'admin-news-view',
        'uses'  => 'ViewController@index'
    ))->where('id', '[0-9]+');
    
    Route::match(array('get', 'post'), '/add.html', array(
        'as'    => 'admin-news-add',
        'uses'  => 'AddController@index'
    ));
    
    Route::match(array('get', 'post'), '/{id}/edit.html', array(
        'as'    => 'admin-news-edit',
        'uses'  => 'EditController@index'
    ))->where('id', '[0-9]+');
    
    Route::get('/json-news-tags.html', array(
        'as'    => 'admin-json-news-categories',
        'uses'  => 'IndexController@jsonNewsTags'
    ));
    
    Route::get('/json-news.html', array(
        'as'    => 'admin-json-news',
        'uses'  => 'IndexController@jsonNews'
    ));
    
    Route::post('/delete.html', array(
        'as'    => 'admin-news-delete',
        'uses'  => 'DeleteController@index'
    ));
    
    Route::post('/change-status.html', array(
        'as'    => 'admin-news-change-status',
        'uses'  => 'ChangeStatusController@index'
    ));
    
    Route::post('/change-verified.html', array(
        'as'    => 'admin-news-change-verified',
        'uses'  => 'ChangeVerifyController@index'
    ));
    
    Route::post('/get-data-chart-news-views.html', array(
        'as'    => 'admin-news-get-chart-news-views',
        'uses'  => 'ChartController@newsView'
    ));
});
