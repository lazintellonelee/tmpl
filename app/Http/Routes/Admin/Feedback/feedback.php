<?php

Route::group(array(
    'prefix'    => 'feedback',
    'namespace' => 'Feedback'
), function() {
    Route::get('/home.html', array(
        'as'    => 'admin-feedback-home',
        'uses'  => 'IndexController@index'
    ));
    
    Route::get('/{id}/view.html', array(
        'as'    => 'admin-feedback-view',
        'uses'  => 'ViewController@index'
    ))->where('id', '[0-9]+');
    
    Route::post('/delete.html', array(
        'as'    => 'admin-feedback-delete',
        'uses'  => 'DeleteController@index'
    ));

    Route::post('/get-data-chart-feedback-views.html', array(
        'as'    => 'admin-feedback-get-chart-feedback-views',
        'uses'  => 'ChartController@feedbackView'
    ));
});
