<?php

Route::group(array(
    'prefix'    => 'partner',
    'namespace' => 'Partner'
), function() {
    Route::get('/home.html', array(
        'as'    => 'admin-partner-home',
        'uses'  => 'IndexController@index'
    ));
    
    Route::get('/{id}/view.html', array(
        'as'    => 'admin-partner-view',
        'uses'  => 'ViewController@index'
    ))->where('id', '[0-9]+');
    
    Route::match(array('get', 'post'), '/add.html', array(
        'as'    => 'admin-partner-add',
        'uses'  => 'AddController@index'
    ));
    
    Route::match(array('get', 'post'), '/{id}/edit.html', array(
        'as'    => 'admin-partner-edit',
        'uses'  => 'EditController@index'
    ))->where('id', '[0-9]+');
    
    Route::get('/json-partner-tags.html', array(
        'as'    => 'admin-json-partner-categories',
        'uses'  => 'IndexController@jsonPartnerTags'
    ));
    
    Route::get('/json-partner.html', array(
        'as'    => 'admin-json-partner',
        'uses'  => 'IndexController@jsonPartner'
    ));
    
    Route::post('/delete.html', array(
        'as'    => 'admin-partner-delete',
        'uses'  => 'DeleteController@index'
    ));
    
    Route::post('/change-status.html', array(
        'as'    => 'admin-partner-change-status',
        'uses'  => 'ChangeStatusController@index'
    ));
    
    Route::post('/get-data-chart-partner-views.html', array(
        'as'    => 'admin-partner-get-chart-partner-views',
        'uses'  => 'ChartController@partnerView'
    ));
});
