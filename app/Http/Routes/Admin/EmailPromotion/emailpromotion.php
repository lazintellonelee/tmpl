<?php

Route::group(array(
    'prefix'    => 'emailpromotion',
    'namespace' => 'EmailPromotion'
), function() {
    Route::get('/home.html', array(
        'as'    => 'admin-emailpromotion-home',
        'uses'  => 'IndexController@index'
    ));
    
    Route::get('/{id}/view.html', array(
        'as'    => 'admin-emailpromotion-view',
        'uses'  => 'ViewController@index'
    ))->where('id', '[0-9]+');
    
    Route::post('/delete.html', array(
        'as'    => 'admin-emailpromotion-delete',
        'uses'  => 'DeleteController@index'
    ));

    Route::post('/get-data-chart-emailpromotion-views.html', array(
        'as'    => 'admin-emailpromotion-get-chart-emailpromotion-views',
        'uses'  => 'ChartController@emailpromotionView'
    ));
});