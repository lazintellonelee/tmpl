<?php

namespace App\Http\Controllers\Admin\Feedback;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\Admin\Feedback\Feedback;

class ChartController extends Controller {
    public function feedbackView(Request $request){
        $data   = $request->input();
        $date   = date('m');
        
        if($request->has('date')) {
            $date = $data['date'];
        }
        
        $feedback = Feedback::select(DB::raw('COUNT(feedback_id) AS feedback, EXTRACT(DAY FROM created_at) AS date'))
                    ->whereRaw(DB::raw('EXTRACT(MONTH FROM created_at) = ' . $date))
                    ->groupBy(DB::raw('EXTRACT(DAY FROM created_at)'))
                    ->get();
        
        if($feedback){
            return response()->json($feedback->toArray());
        }
        return response()->json(array());
    }
}