<?php

namespace App\Http\Controllers\Admin\Feedback;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Plugins\File;
use App\Models\Admin\Feedback\Feedback;

class DeleteController extends Controller {
    public function index(Request $request){
        $data   = $request->input();
        
        DB::beginTransaction();
        if(!Feedback::DeleteInID($data['feedback_id'])){
            DB::rollback();
            
            echo json_encode(array(
                'error' => 'error1'
            )); 
            exit();
        }
        
        DB::commit();
        echo json_encode(array(
            'success'   => 'success'
        ));
        exit();
    }
}