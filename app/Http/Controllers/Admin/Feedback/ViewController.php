<?php

namespace App\Http\Controllers\Admin\Feedback;

use App\Http\Controllers\Controller;

use App\Models\Admin\Feedback\Feedback;

class ViewController extends Controller 
{
    public function index($id)
    {
        $feedback = Feedback::where('feedback_id', $id)
                          ->first();
        
        return response()->view('admin.feedback.view', array(
            'feedback' => $feedback
        ));
    }
}