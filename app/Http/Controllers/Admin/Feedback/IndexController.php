<?php

namespace App\Http\Controllers\Admin\Feedback;

use App\Http\Controllers\Controller;

use App\Models\Admin\Feedback\Feedback;

class IndexController extends Controller
{
    public function index() {
        return response()->view('admin.feedback.home', array(
            'feedback'  => Feedback::GetFeedback()
        ));
    }
    
    public function jsonFeedback(){
        $feedback = Feedback::GetFeedback();
        if(!$feedback){
            $feedback = array();
        }
        return response()->json($feedback);
    }
}