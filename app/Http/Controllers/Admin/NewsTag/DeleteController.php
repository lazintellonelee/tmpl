<?php

namespace App\Http\Controllers\Admin\NewsTag;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Admin\News\NewsTag;
use App\Models\Admin\News\NewsRelationTags;

class DeleteController extends Controller 
{
    public function index(Request $request)
    {
        $data = $request->input();
        DB::beginTransaction();
        if(!NewsTag::DeleteNewsTagsInID($data['news_tag_id']))
        {
            DB::rollback();
            echo json_encode(array(
                'error' => 'error'
            ));
            exit();
        }
        
        foreach($data['news_tag_id'] as $news_tag_id)
        {
            if(!NewsRelationTags::DeleteRelationByNewsTagID($news_tag_id))
            {
                DB::rollback();
                echo json_encode(array(
                    'error' => 'error'
                ));
                exit();
            }
        }
        
        DB::commit();
        echo json_encode(array(
            'success'   => 'success'
        ));
        exit();
    }
}