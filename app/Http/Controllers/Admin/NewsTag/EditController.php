<?php

namespace App\Http\Controllers\Admin\NewsTag;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\News\NewsTag;
use App\Plugins\SEO;

class EditController extends Controller 
{
    public function get(Request $request)
    {
        $data = $request->input();
        echo json_encode(NewsTag::GetNewsByID($data['news_tag_id']));
        exit();
    }
    
    public function index(Request $request)
    {
        $data = $request->input();
        if(!NewsTag::EditNewsTag($data['news_tag_id'], array(
            'news_tag_name' => $data['news_tag_name'],
            'news_tag_url'  => str_replace(' ', '-', strtolower(SEO::RemoveVN($data['news_tag_name'])))
        )))
        {
            echo json_encode(array(
                'error' => 'error'
            ));
            exit();
        }
        echo json_encode(NewsTag::GetNewsByID($data['news_tag_id']));
        exit();
    }
}
