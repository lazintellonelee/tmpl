<?php

namespace App\Http\Controllers\Admin\NewsTag;

use App\Http\Controllers\Controller;

use App\Models\Admin\News\NewsTag;

class IndexController extends Controller 
{
    public function index()
    {
        return response()->view('admin.news-tag.index', array(
            'newsTags'  => NewsTag::GetNewsTags()
        ));
    }
}