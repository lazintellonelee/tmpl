<?php

namespace App\Http\Controllers\Admin\NewsTag;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\News\NewsTag;
use App\Plugins\SEO;

class AddController extends Controller 
{
    public function index(Request $request)
    {
        $data                   = $request->input();
        $data['news_tag_url']   = str_replace(' ', '-', strtolower(SEO::RemoveVN($data['news_tag_name'])));
        $add                    = NewsTag::AddNewsTag($data);
        if($add) 
        {
            $add                = $add->toArray();
            $add['created_at']  = date('H:i d/m/Y', strtotime($add['created_at']));
            echo json_encode($add);
            exit();
        }
        echo json_encode(array(
            'error' => 'error'
        ));
        exit();
    }
}
