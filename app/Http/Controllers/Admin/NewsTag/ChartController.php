<?php

namespace App\Http\Controllers\Admin\NewsTag;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Models\Admin\News\NewsRelationTags;

class ChartController extends Controller
{
    public function index()
    {
        $data = NewsRelationTags::select(DB::raw('COUNT(news_id) AS news, news_tag_name'))
                                ->join('tbl_news_tags', 'tbl_news_relation_tags.news_tag_id', '=', 'tbl_news_tags.news_tag_id')
                                ->groupBy(DB::raw('tbl_news_tags.news_tag_name'))
                                ->orderBy('news', 'desc')
                                ->take(10)
                                ->get();
        
        if($data)
        {
            $data = $data->toArray();
            shuffle($data);
            return response()->json($data);
        }
        return response()->json(array());
    }
}