<?php

namespace App\Http\Controllers\Admin\News;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Admin\News\NewsRelationTags;
use App\Models\Admin\News\NewsRelated;
use App\Models\Admin\News\NewsTag;
use App\Models\Admin\News\News;
use App\Plugins\File;
use App\Plugins\PHPThumb\Thumbnail;
use App\Plugins\SEO;

class AddController extends Controller
{
    public function index(Request $request)
    {
        if($request->isMethod('post'))
        {
            $data       = $request->input();
            $file       = new File();
            $data       = $request->input();
            DB::beginTransaction();
            $imageName  = str_replace(' ', '-', strtolower(SEO::RemoveVN($data['news_title'])).'-'.  uniqid());
            if($request->hasFile('news_avatar') && $data['news_avatar_base64'] == '')
            {
                $file->setFile($request->file('news_avatar'));
                if(!$file->isImage())
                {
                    return response()->view('admin.news.add', array(
                        'errors'    => array(
                            'Tập tin tải lên không phải là hình ảnh!'
                        )
                    ));
                }
                $data['news_avatar_high'] = $file->move('./public/upload/images/news/high', $imageName);
            }
            
            if($data['news_avatar_base64'] != '')
            {
                $data['news_avatar_high'] = File::BuildImageFromBase64($data['news_avatar_base64'], $imageName, './public/upload/images/news/high/');
            }
            
            if(isset($data['news_avatar_high']))
            {
                $thumbnail = new Thumbnail($data['news_avatar_high'], './public/upload/images/news/high/');
                $thumbnail->Scale(175, 175*3/6);
                $thumbnail->Save('./public/upload/images/news/low/');

                $data['news_avatar_low']   = 'public/upload/images/news/low/' . $data['news_avatar_high'];
                $data['news_avatar_high']  = 'public/upload/images/news/high/' . $data['news_avatar_high'];
            }
            
            $data['news_url']   = str_replace(' ', '-', strtolower(SEO::RemoveVN($data['news_title'])));
            $data['created_by'] = get_user_id();
            $news               = News::AddNews($data);
            if(!$news) 
            {
                DB::rollback();
                $file->delete('./', $data['news_avatar_low']);
                $file->delete('./', $data['news_avatar_high']);
                return response()->view('admin.news.add', array(
                    'errors'    => array(
                        'Tạo tin tức không thành công! Vui lòng thử lại.'
                    )
                ));
            }
            
            if(!$this->AddNewsRelationTags(explode(',', $data['news_tags']), $news->news_id))
            {
                DB::rollback();
                $file->delete('./', $data['news_avatar_low']);
                $file->delete('./', $data['news_avatar_high']);
                return response()->view('admin.news.add', array(
                    'errors'    => array(
                        'Tạo tin tức không thành công! Vui lòng thử lại.'
                    )
                ));
            }
            
            if(!$this->AddNewsRelated(explode(',', $data['news_related']), $news->news_id))
            {
                DB::rollback();
                $file->delete('./', $data['news_avatar_low']);
                $file->delete('./', $data['news_avatar_high']);
                return response()->view('admin.news.add', array(
                    'errors'    => array(
                        'Tạo tin tức không thành công! Vui lòng thử lại.'
                    )
                ));
            }
            
            DB::commit();
            $request->session()->flash('success', 'Tạo tin tức thành công.');
            return redirect()->route('admin-news-home');
        }
        return response()->view('admin.news.add');
    }
    
    private function AddNewsRelationTags($arrTagName, $newsID)
    {
        DB::beginTransaction();
        $newsTags   =   NewsTag
                        ::whereIn('news_tag_name', $arrTagName)
                        ->get(array(
                            'news_tag_id',
                            'news_tag_name',
                        ))
                        ->toArray();
        
        $arr_diff   = array_diff($arrTagName, array_column($newsTags, 'news_tag_name'));
        foreach($arr_diff as $newsTagName) 
        {
            $newsTag = NewsTag::AddNewsTag(array(
                'news_tag_name' => trim($newsTagName),
                'news_tag_url'  => SEO::RemoveVN(str_replace(' ', '-', strtolower(trim($newsTagName))))
            ));
            if(!$newsTag)
            {
                DB::rollback();
                return false;
            }
            $newsTags = array_merge($newsTags, array($newsTag->toArray()));
        }
        
        foreach($newsTags as &$newsTag)
        {
            unset($newsTag['updated_at']);
            unset($newsTag['created_at']);
            unset($newsTag['news_tag_url']);
            unset($newsTag['news_tag_name']);
            $newsTag['news_id'] = $newsID;
        }
        
        if(!NewsRelationTags::AddNewsRelationTags($newsTags))
        {
            DB::rollback();
            return false;
        }
        DB::commit();
        return true;
    }
    
    private function AddNewsRelated($arrNews, $newsID)
    {
        $data = array();
        foreach($arrNews as $news)
        {
            $data[] = array(
                'news_id'           => $newsID,
                'related_news_id'   => $news
            );
        }
        
        return NewsRelated::AddNewsRelated($data);
    }
}