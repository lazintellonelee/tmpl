<?php

namespace App\Http\Controllers\Admin\News;

use App\Http\Controllers\Controller;

use App\Models\Admin\News\News;
use App\Models\Admin\News\NewsTag;

class IndexController extends Controller 
{
    public function index() 
    {
        $news = News::orderBy('created_at', 'DESC');
        if(request()->query('ct'))
        {
            $news = $news->where('news_category_id', request()->query('ct'));
        }
        $news = $news->paginate(20);
        
        return response()->view('admin.news.home', array(
            'news'  => $news
        ));
    }
    
    public function jsonNewsTags() 
    {
        $newsTags = NewsTag::GetNewsTags();
        if(!$newsTags)
        {
            $newsTags = array();
        }
        return response()->json($newsTags);
    }
    
    public function jsonNews()
    {
        $news = News::GetNews();
        if(!$news)
        {
            $news = array();
        }
        return response()->json($news);
    }
}