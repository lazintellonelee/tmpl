<?php

namespace App\Http\Controllers\Admin\News;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\Admin\News\News;

class ChartController extends Controller 
{
    
    public function newsView(Request $request)
    {
        $data   = $request->input();
        $date   = date('m');
        $year   = date('Y');
        if($request->has('date')) {
            $date = $data['date'];
        }
        if($request->has('year')) {
            $year = $data['year'];
        }
        $news = News::select(DB::raw('COUNT(news_id) AS news, EXTRACT(DAY FROM created_at) AS date'))
                    ->whereRaw(DB::raw('EXTRACT(MONTH FROM created_at) = ' . $date))
                    ->whereRaw(DB::raw('EXTRACT(YEAR FROM created_at) = ' . $year))
                    ->groupBy(DB::raw('EXTRACT(DAY FROM created_at)'))
                    ->get();
        
        if($news)
        {
            return response()->json($news->toArray());
        }
        return response()->json(array());
    }
}