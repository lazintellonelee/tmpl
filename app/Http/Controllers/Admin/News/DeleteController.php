<?php

namespace App\Http\Controllers\Admin\News;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Plugins\File;
use App\Models\Admin\News\News;
use App\Models\Admin\News\NewsRelated;
use App\Models\Admin\News\NewsRelationTags;

class DeleteController extends Controller 
{
    public function index(Request $request)
    {
        $data   = $request->input();
        $file   = new File();
        $news   = News::GetNewsInID($data['news_id']);
        DB::beginTransaction();
        if(!News::DeleteInID($data['news_id']))
        {
            DB::rollback();
            echo json_encode(array(
                'error' => 'error1'
            )); 
            exit();
        }
        
        if(!NewsRelationTags::DeleteInNewsID($data['news_id']))
        {
            DB::rollback();
            echo json_encode(array(
                'error' => 'error2'
            )); 
            exit();
        }
        
        if(!NewsRelated::DeleteInNewsID($data['news_id']))
        {
            DB::rollback();
            echo json_encode(array(
                'error' => 'error3'
            ));
            exit();
        }
        
        foreach(array_column($news, 'news_avatar_high') as $image)
        {
            if($image != '')
            {
                $file->delete('./', $image);
            }
        }
        foreach(array_column($news, 'news_avatar_low') as $image)
        {
            if($image != '')
            {
                $file->delete('./', $image);
            }
        }
        
        DB::commit();
        echo json_encode(array(
            'success'   => 'success'
        ));
        exit();
    }
}