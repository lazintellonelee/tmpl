<?php

namespace App\Http\Controllers\Admin\News;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\News\News;

class ChangeStatusController extends Controller 
{
    public function index(Request $request)
    {
        $data = $request->input();
        if(!News::EditNewsByID($data['news_id'], $data))
        {
            echo json_encode(array(
               'error'  => 'error'
            ));
            exit();
        }
        echo json_encode(array(
            'success'   => 'success'
        ));
        exit();
    }
}