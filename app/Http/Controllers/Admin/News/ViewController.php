<?php

namespace App\Http\Controllers\Admin\News;

use App\Http\Controllers\Controller;

use App\Models\Admin\News\News;

class ViewController extends Controller 
{
    public function index($id)
    {
        $news = News::where('news_id', $id)
                    ->leftJoin('tbl_news_categories', 'tbl_news.news_category_id', '=', 'tbl_news_categories.news_category_id')
                    ->first();
        
        return response()->view('admin.news.view', array(
            'news' => $news
        ));
    }
}