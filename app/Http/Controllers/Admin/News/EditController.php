<?php

namespace App\Http\Controllers\Admin\News;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Plugins\SEO;
use App\Plugins\File;
use App\Plugins\PHPThumb\Thumbnail;
use App\Models\Admin\News\News;
use App\Models\Admin\News\NewsTag;
use App\Models\Admin\News\NewsRelated;
use App\Models\Admin\News\NewsRelationTags;

class EditController extends Controller 
{
    public function index(Request $request, $id) {
        $news           = News::GetNewsByID($id);
        if(!$news)
        {
            return redirect()->route('admin-news-home');
        }
        $newsTags       = NewsRelationTags::GetTagInfoByNewsID($id);
        $newsRelated    = NewsRelated::GetRelatedNewsInfoByNewsID($id);
        
        if($request->isMethod('post'))
        {
            $data       = $request->input();
            $file       = new File();
            $data       = $request->input();
            DB::beginTransaction();
            $imageName  = str_replace(' ', '-', strtolower(SEO::RemoveVN($data['news_title'])).'-'.  uniqid());
            if($request->hasFile('news_avatar') && $data['news_avatar_base64'] == '')
            {
                $file->setFile($request->file('news_avatar'));
                if(!$file->isImage())
                {
                    return response()->view('admin.news.edit', array(
                        'news'          => $news,
                        'newsTags'      => $newsTags,
                        'newsRelated'   => $newsRelated,
                        'errors'        => array(
                            'Tập tin tải lên không phải là hình ảnh!'
                        )
                    ));
                }
                $data['news_avatar_high'] = $file->move('./public/upload/images/news/high', $imageName);
            }
            
            if($data['news_avatar_base64'] != '')
            {
                $data['news_avatar_high'] = File::BuildImageFromBase64($data['news_avatar_base64'], $imageName, './public/upload/images/news/high/');
            }
            
            if(isset($data['news_avatar_high']))
            {
                $thumbnail = new Thumbnail($data['news_avatar_high'], './public/upload/images/news/high/');
                $thumbnail->Scale(175, 175*3/6);
                $thumbnail->Save('./public/upload/images/news/low/');

                $data['news_avatar_low']    = 'public/upload/images/news/low/' . $data['news_avatar_high'];
                $data['news_avatar_high']   = 'public/upload/images/news/high/' . $data['news_avatar_high'];
            }
            
            if(isset($data['news_avatar_url']) && $data['news_avatar_url'] != '')
            {
                $data['news_avatar_low']    = '';
                $data['news_avatar_high']   = '';
            }
            
            if(isset($data['news_avatar_low']) &&
               isset($data['news_avatar_high']) && 
               $data['news_avatar_low'] != '' && 
               $data['news_avatar_high'] != '')
            {
                $data['news_avatar_url']    = '';
            }
            
            $data['news_url']   = str_replace(' ', '-', strtolower(SEO::RemoveVN($data['news_title'])));
            $update             = News::EditNewsByID($id, $data);
            if(!$update)
            {
                DB::rollback();
                $file->delete('./', $data['news_avatar_low']);
                $file->delete('./', $data['news_avatar_high']);
                return response()->view('admin.news.edit', array(
                    'news'          => $news,
                    'newsTags'      => $newsTags,
                    'newsRelated'   => $newsRelated,
                    'errors'        => array(
                        'Cập nhật tin tức không thành công! Vui lòng thử lại'
                    )
                ));
            }
            
            if(!$this->UpdateNewsRelationTags(explode(',', $data['news_tags']), $newsTags, $id))
            {
                DB::rollback();
                $file->delete('./', $data['news_avatar_low']);
                $file->delete('./', $data['news_avatar_high']);
                return response()->view('admin.news.edit', array(
                    'news'          => $news,
                    'newsTags'      => $newsTags,
                    'newsRelated'   => $newsRelated,
                    'errors'        => array(
                        'Cập nhật tin tức không thành công! Vui lòng thử lại'
                    )
                ));
            }
            
            if(!$this->UpdateNewsRelated(explode(',', $data['news_related']), array_column($newsRelated, 'news_id'), $id))
            {
                DB::rollback();
                $file->delete('./', $data['news_avatar_low']);
                $file->delete('./', $data['news_avatar_high']);
                return response()->view('admin.news.edit', array(
                    'news'          => $news,
                    'newsTags'      => $newsTags,
                    'newsRelated'   => $newsRelated,
                    'errors'        => array(
                        'Cập nhật tin tức không thành công! Vui lòng thử lại'
                    )
                ));
            }
            
            if((isset($data['news_avatar_high']) && 
                $news['news_avatar_high'] != '') ||
                isset($data['news_avatar_url'])) 
            {
                $file->delete('./', $news['news_avatar_low']);
                $file->delete('./', $news['news_avatar_high']);
            }
            
            DB::commit();
            $request->session()->flash('success', 'Cập nhật tin tức thành công.');
            return redirect()->route('admin-news-home');
        }
        
        return response()->view('admin.news.edit', array(
            'news'          => $news,
            'newsTags'      => $newsTags,
            'newsRelated'   => $newsRelated
        ));
    }
    
    private function UpdateNewsRelationTags($arrTagName, $newsTags, $newsID)
    {
        DB::beginTransaction();
        // Thêm các tag mới
        $new_tags   = array_diff($arrTagName, array_column($newsTags, 'news_tag_name'));
        $tags       = NewsTag
                      ::whereIn('news_tag_name', $new_tags)
                      ->get(array(
                          'news_tag_id',
                          'news_tag_name',
                      ))
                      ->toArray();
        
        $arr_diff   = array_diff($new_tags, array_column($tags, 'news_tag_name'));
        foreach($arr_diff as $newsTagName) 
        {
            $newsTag = NewsTag::AddNewsTag(array(
                'news_tag_name' => trim($newsTagName),
                'news_tag_url'  => SEO::RemoveVN(str_replace(' ', '-', strtolower(trim($newsTagName))))
            ));
            if(!$newsTag)
            {
                DB::rollback();
                return false;
            }
            $tags = array_merge($tags, array($newsTag->toArray()));
        }
        
        foreach($tags as &$newsTag)
        {
            unset($newsTag['updated_at']);
            unset($newsTag['created_at']);
            unset($newsTag['news_tag_url']);
            unset($newsTag['news_tag_name']);
            $newsTag['news_id'] = $newsID;
        }
        
        if(!NewsRelationTags::AddNewsRelationTags($tags))
        {
            DB::rollback();
            return false;
        }
        // /Thêm cái tag mới
        
        // /Gỡ các tag xoá
        $del_tags   = array_diff(array_column($newsTags, 'news_tag_name'), $arrTagName);
        $tags       = NewsTag
                      ::whereIn('news_tag_name', $del_tags)
                      ->get(array(
                          'news_tag_id',
                          'news_tag_name',
                      ))
                      ->toArray();
        foreach($tags as $tag)
        {
            if(!NewsRelationTags::DeleteRelationNewsTag($newsID, $tag['news_tag_id']))
            {   
                DB::rollback();
                return false;
            }
        }
        // /Gỡ các tag xoá
        
        DB::commit();
        return true;
    }
    
    private function UpdateNewsRelated($arrNews, $relatedNews, $newsID)
    {
        DB::beginTransaction();
        $new_related    = array_diff($arrNews, $relatedNews);
        $data           = array();
        foreach($new_related as $news)
        {
            $data[] = array(
                'news_id'           => $newsID,
                'related_news_id'   => $news
            );
        }
        if(!NewsRelated::AddNewsRelated($data))
        {
            DB::rollback();
            return false;
        }
        
        $delete_related = array_diff($relatedNews, $arrNews);
        foreach($delete_related as $relatedNewsID)
        {
            if(!NewsRelated::DeleteRelated($newsID, $relatedNewsID))
            {
                DB::rollback();
                return false;
            }
        }
        
        DB::commit();
        return true;
    }
}

