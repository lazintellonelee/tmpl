<?php

namespace App\Http\Controllers\Admin\Article;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Plugins\SEO;
use App\Plugins\File;
use App\Plugins\PHPThumb\Thumbnail;
use App\Models\Admin\Article\Article;

class EditController extends Controller 
{
    public function index(Request $request, $id) {
        $article           = Article::GetArticleByID($id);
        
        if(!$article){
            return redirect()->route('admin-article-home');
        }
        
        if($request->isMethod('post')){
            $data   = $request->input();
            $file   = new File();
            $path   = "public/upload/images/article/";
            DB::beginTransaction();
            
            $imageName  = str_replace(' ', '-', strtolower(SEO::RemoveVN($data['article_title'])).'-'.  uniqid());
            if($request->hasFile('article_thumbnail_image') && $data['article_thumbnail_base64'] == ''){
                $file->setFile($request->file('article_thumbnail_image'));
                if(!$file->isImage()){
                    return response()->view('admin.article.edit', array(
                        'article'   => $article,
                        'errors'    => array(
                            'Tập tin tải lên không phải là hình ảnh!'
                        )
                    ));
                }
                $data['article_thumbnail_image'] = $path . $file->move('./public/upload/images/article', $imageName);
            }
            
            if($data['article_thumbnail_base64'] != ''){
                $data['article_thumbnail_image'] = $path . File::BuildImageFromBase64($data['article_thumbnail_base64'], $imageName, './public/upload/images/article/');
            }
            
            if(isset($data['article_thumbnail_url']) && $data['article_thumbnail_url'] != ''){
                $data['article_thumbnail_image']    = '';
            }
            
            if(isset($data['article_thumbnail_image']) && $data['article_thumbnail_image'] != ''){
                $data['article_thumbnail_url']    = '';
            }
            
            $data['article_url']    = str_replace(' ', '-', strtolower(SEO::RemoveVN($data['article_title'])));
            $update                 = Article::EditArticleByID($id, $data);
            if(!$update){
                DB::rollback();
                $file->delete('./', $data['article_thumbnail_image']);
                return response()->view('admin.article.edit', array(
                    'article'   => $article,
                    'errors'    => array(
                        'Cập nhật bài viết không thành công! Vui lòng thử lại'
                    )
                ));
            }
            
            if(isset($data['article_thumbnail_url'])) {
                $file->delete('./', $article['article_thumbnail_image']);
            }
            
            DB::commit();
            $request->session()->flash('success', 'Cập nhật bài viết thành công.');
            return redirect()->route('admin-article-home');
        }
        
        return response()->view('admin.article.edit', array(
            'article'          => $article,
        ));
    }
}