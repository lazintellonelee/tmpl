<?php

namespace App\Http\Controllers\Admin\Article;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\Admin\Article\Article;

class ChartController extends Controller {
    public function articleView(Request $request){
        $data   = $request->input();
        $date   = date('m');
        
        if($request->has('date')) {
            $date = $data['date'];
        }
        
        $article = Article::select(DB::raw('COUNT(article_id) AS article, EXTRACT(DAY FROM created_at) AS date'))
                    ->whereRaw(DB::raw('EXTRACT(MONTH FROM created_at) = ' . $date))
                    ->groupBy(DB::raw('EXTRACT(DAY FROM created_at)'))
                    ->get();
        
        if($article){
            return response()->json($article->toArray());
        }
        return response()->json(array());
    }
}