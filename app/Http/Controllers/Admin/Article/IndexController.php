<?php

namespace App\Http\Controllers\Admin\Article;

use App\Http\Controllers\Controller;

use App\Models\Admin\Article\Article;

class IndexController extends Controller
{
    public function index() {
        return response()->view('admin.article.home', array(
            'article'  => Article::GetArticle()
        ));
    }
    
    public function jsonArticle(){
        $article = Article::GetArticle();
        if(!$article){
            $article = array();
        }
        return response()->json($article);
    }
}