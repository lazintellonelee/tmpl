<?php

namespace App\Http\Controllers\Admin\Article;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Admin\Article\Article;
use App\Plugins\File;
use App\Plugins\PHPThumb\Thumbnail;
use App\Plugins\SEO;

class AddController extends Controller
{
    public function index(Request $request){
        if($request->isMethod('post')){
            $data   = $request->input();
            $file   = new File();
            $path   = "public/upload/images/article/" ;
            DB::beginTransaction();
            
            $imageName  = str_replace(' ', '-', strtolower(SEO::RemoveVN($data['article_title'])).'-'.  uniqid());
            if($request->hasFile('article_thumbnail_image') && $data['article_thumbnail_base64'] == ''){
                $file->setFile($request->file('article_thumbnail_image'));
                if(!$file->isImage()){
                    return response()->view('admin.article.add', array(
                        'errors'    => array(
                            'Tập tin tải lên không phải là hình ảnh!'
                        )
                    ));
                }
                $data['article_thumbnail_image'] = $path . $file->move('./public/upload/images/article', $imageName);
            }
            
            if($data['article_thumbnail_base64'] != ''){
                $data['article_thumbnail_image'] = $path . File::BuildImageFromBase64($data['article_thumbnail_base64'], $imageName, './public/upload/images/article/');
            }
            
            $data['article_url']    = str_replace(' ', '-', strtolower(SEO::RemoveVN($data['article_title'])));
            $data['created_by']     = get_user_id();
            
            $article    = Article::AddArticle($data);
            
            if(!$article) {
                DB::rollback();
                $file->delete('./', $data['article_thumbnail_image']);
                return response()->view('admin.article.add', array(
                    'errors'    => array(
                        'Tạo bài viết không thành công! Vui lòng thử lại.'
                    )
                ));
            }
            
            DB::commit();
            $request->session()->flash('success', 'Tạo bài viết thành công.');
            return redirect()->route('admin-article-home');
        }
        return response()->view('admin.article.add');
    }
}