<?php

namespace App\Http\Controllers\Admin\Article;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\Article\Article;

class ChangeStatusController extends Controller 
{
    public function index(Request $request)
    {
        $data = $request->input();
        if(!Article::EditArticleByID($data['article_id'], $data))
        {
            echo json_encode(array(
               'error'  => 'error'
            ));
            exit();
        }
        echo json_encode(array(
            'success'   => 'success'
        ));
        exit();
    }
}