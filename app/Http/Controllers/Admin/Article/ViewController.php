<?php

namespace App\Http\Controllers\Admin\Article;

use App\Http\Controllers\Controller;

use App\Models\Admin\Article\Article;

class ViewController extends Controller 
{
    public function index($id)
    {
        $article = Article::where('article_id', $id)
                          ->first();
        
        return response()->view('admin.article.view', array(
            'article' => $article
        ));
    }
}