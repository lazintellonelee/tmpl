<?php

namespace App\Http\Controllers\Admin\Article;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Plugins\File;
use App\Models\Admin\Article\Article;

class DeleteController extends Controller {
    public function index(Request $request){
        $data   = $request->input();
        $file   = new File();
        
        $article   = Article::GetArticleInID($data['article_id']);
        
        DB::beginTransaction();
        if(!Article::DeleteInID($data['article_id'])){
            DB::rollback();
            
            echo json_encode(array(
                'error' => 'error1'
            )); 
            exit();
        }
        
        foreach(array_column($article, 'article_thumbnail_image') as $image){
            if($image != ''){
                $file->delete('./', $image);
            }
        }
        
        DB::commit();
        echo json_encode(array(
            'success'   => 'success'
        ));
        exit();
    }
}