<?php

namespace App\Http\Controllers\Admin\Partner;

use App\Http\Controllers\Controller;

use App\Models\Admin\Partner\Partner;

class IndexController extends Controller
{
    public function index() {
        return response()->view('admin.partner.home', array(
            'partner'  => Partner::GetPartner()
        ));
    }
    
    public function jsonPartner(){
        $partner = Partner::GetPartner();
        if(!$partner){
            $partner = array();
        }
        return response()->json($partner);
    }
}