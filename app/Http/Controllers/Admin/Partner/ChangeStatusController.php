<?php

namespace App\Http\Controllers\Admin\Partner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\Partner\Partner;

class ChangeStatusController extends Controller 
{
    public function index(Request $request)
    {
        $data = $request->input();
        if(!Partner::EditPartnerByID($data['partner_id'], $data))
        {
            echo json_encode(array(
               'error'  => 'error'
            ));
            exit();
        }
        echo json_encode(array(
            'success'   => 'success'
        ));
        exit();
    }
}