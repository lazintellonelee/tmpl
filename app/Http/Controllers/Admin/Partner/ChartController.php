<?php

namespace App\Http\Controllers\Admin\Partner;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\Admin\Partner\Partner;

class ChartController extends Controller {
    public function partnerView(Request $request){
        $data   = $request->input();
        $date   = date('m');
        
        if($request->has('date')) {
            $date = $data['date'];
        }
        
        $partner = Partner::select(DB::raw('COUNT(partner_id) AS partner, EXTRACT(DAY FROM created_at) AS date'))
                    ->whereRaw(DB::raw('EXTRACT(MONTH FROM created_at) = ' . $date))
                    ->groupBy(DB::raw('EXTRACT(DAY FROM created_at)'))
                    ->get();
        
        if($partner){
            return response()->json($partner->toArray());
        }
        return response()->json(array());
    }
}