<?php

namespace App\Http\Controllers\Admin\Partner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Admin\Partner\Partner;
use App\Plugins\File;
use App\Plugins\PHPThumb\Thumbnail;
use App\Plugins\SEO;

class AddController extends Controller
{
    public function index(Request $request){
        if($request->isMethod('post')){
            $data   = $request->input();
            $file   = new File();
            $path   = "public/upload/images/partner/" ;
            DB::beginTransaction();
            
            $imageName  = str_replace(' ', '-', strtolower(SEO::RemoveVN($data['partner_name'])).'-'.  uniqid());
            if($request->hasFile('partner_thumbnail_image') && $data['partner_thumbnail_base64'] == ''){
                $file->setFile($request->file('partner_thumbnail_image'));
                if(!$file->isImage()){
                    return response()->view('admin.partner.add', array(
                        'errors'    => array(
                            'Tập tin tải lên không phải là hình ảnh!'
                        )
                    ));
                }
                $data['partner_thumbnail_image'] = $path . $file->move('./public/upload/images/partner', $imageName);
            }
            
            if($data['partner_thumbnail_base64'] != ''){
                $data['partner_thumbnail_image'] = $path . File::BuildImageFromBase64($data['partner_thumbnail_base64'], $imageName, './public/upload/images/partner/');
            }
            
            $data['partner_url']            = str_replace(' ', '-', strtolower(SEO::RemoveVN($data['partner_name'])));
            $data['created_by']             = get_user_id();
            
            $partner    = Partner::AddPartner($data);
            
            if(!$partner) {
                DB::rollback();
                $file->delete('./', $data['partner_thumbnail_image']);
                return response()->view('admin.partner.add', array(
                    'errors'    => array(
                        'Tạo đối tác không thành công! Vui lòng thử lại.'
                    )
                ));
            }
            
            DB::commit();
            $request->session()->flash('success', 'Tạo đối tác thành công.');
            return redirect()->route('admin-partner-home');
        }
        return response()->view('admin.partner.add');
    }
}