<?php

namespace App\Http\Controllers\Admin\Partner;

use App\Http\Controllers\Controller;

use App\Models\Admin\Partner\Partner;

class ViewController extends Controller 
{
    public function index($id)
    {
        $partner = Partner::where('partner_id', $id)
                          ->first();
        
        return response()->view('admin.partner.view', array(
            'partner' => $partner
        ));
    }
}