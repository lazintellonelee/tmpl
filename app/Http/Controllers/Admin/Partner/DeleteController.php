<?php

namespace App\Http\Controllers\Admin\Partner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Plugins\File;
use App\Models\Admin\Partner\Partner;

class DeleteController extends Controller {
    public function index(Request $request){
        $data   = $request->input();
        $file   = new File();
        
        $partner   = Partner::GetPartnerInID($data['partner_id']);
        
        DB::beginTransaction();
        if(!Partner::DeleteInID($data['partner_id'])){
            DB::rollback();
            
            echo json_encode(array(
                'error' => 'error1'
            )); 
            exit();
        }
        
        foreach(array_column($partner, 'partner_thumbnail_image') as $image){
            if($image != ''){
                $file->delete('./', $image);
            }
        }
        
        DB::commit();
        echo json_encode(array(
            'success'   => 'success'
        ));
        exit();
    }
}