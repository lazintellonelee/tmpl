<?php

namespace App\Http\Controllers\Admin\NewsCategory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Plugins\File;
use App\Plugins\SEO;
use App\Plugins\PHPThumb\Thumbnail;
use App\Models\Admin\News\NewsCategory;

class AddController extends Controller
{
    public function index(Request $request)
    {
        if($request->isMethod('post'))
        {
            $file       = new File();
            $data       = $request->input();
            $imageName  = str_replace(' ', '-', strtolower(SEO::RemoveVN($data['news_category_name'])).'-'.  uniqid());
            if($request->hasFile('news_category_avatar') && $data['news_category_avatar_base64'] == '')
            {
                $file->setFile($request->file('news_category_avatar'));
                if(!$file->isImage())
                {
                    return response()->view('admin.news-category.add', array(
                        'errors'    => array(
                            'Tập tin tải lên không phải là hình ảnh!'
                        )
                    ));
                }
                $data['news_category_avatar_high'] = $file->move('./public/upload/images/news-category/high', $imageName);
            }
            
            if($data['news_category_avatar_base64'] != '')
            {
                $data['news_category_avatar_high'] = File::BuildImageFromBase64($data['news_category_avatar_base64'], $imageName, './public/upload/images/news-category/high/');
            }
            
            if(isset($data['news_category_avatar_high']))
            {
                $thumbnail = new Thumbnail($data['news_category_avatar_high'], './public/upload/images/news-category/high/');
                $thumbnail->Scale(175, 175*3/6);
                $thumbnail->Save('./public/upload/images/news-category/medium/');

                $data['news_category_avatar_medium']    = 'public/upload/images/news-category/medium/' . $data['news_category_avatar_high'];
                $data['news_category_avatar_high']      = 'public/upload/images/news-category/high/' . $data['news_category_avatar_high'];
            }
            
            $data['news_category_url'] = str_replace(' ', '-', strtolower(SEO::RemoveVN($data['news_category_name'])));
            if(!NewsCategory::AddNewsCategory($data)) 
            {
                $file->delete('./', $data['news_category_avatar_medium']);
                $file->delete('./', $data['news_category_avatar_high']);
                return response()->view('admin.news-category.add', array(
                    'errors'    => array(
                        'Tạo danh mục tin tức không thành công! Vui lòng thử lại.'
                    )
                ));
            }
            
            $request->session()->flash('success', 'Tạo danh mục tin tức thành công.');
            return redirect()->route('admin-news-category-home');
        }
        return response()->view('admin.news-category.add');
    }
}