<?php

namespace App\Http\Controllers\Admin\NewsCategory;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
//use Illuminate\Http\Request;

use App\Models\Admin\News\NewsCategory;
use App\Models\Admin\News\News;

class ChartController extends Controller
{
    public function index()
    {
        $parents =  NewsCategory::where('news_category_parent_id', 0)
                                ->get(array(
                                    'news_category_id',
                                    'news_category_name'
                                ));
        
        if($parents) 
        {
            $parents = $parents->toArray();
            foreach($parents as &$parent)
            {
                $parent['news_count'] = array_get(current($this->CountNewsInParents($parent['news_category_id'])), 'news');
            }
            return response()->json($parents);
        }
        return response()->json(array());
    }
    
    private function CountNewsInParents($news_category_parent_id)
    {
        $news = News::select(DB::raw('COUNT(news_id) AS news'))
                    ->whereIn('news_category_id', array_merge(
                        array($news_category_parent_id),
                        $this->GetArrayNewsCategoryIDByParentID($news_category_parent_id)
                    ))
                    ->get();
        if($news)
        {
            $news = $news->toArray();
            return $news;
        }
        return false;
    }
    
    public function GetArrayNewsCategoryIDByParentID($news_category_parent_id)
    {
        $parents =  NewsCategory::where('news_category_parent_id', $news_category_parent_id)
                                ->get();
        if(!$parents) return array();
        $arrID = array();
        foreach($parents->toArray() as $parent) 
        {
            $arrID[]    = $parent['news_category_id'];
            $arrID      = array_merge($arrID, $this->GetArrayNewsCategoryIDByParentID($parent['news_category_id']));
        }
        return $arrID;
    }
}