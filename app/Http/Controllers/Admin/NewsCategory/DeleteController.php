<?php

namespace App\Http\Controllers\Admin\NewsCategory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\News\NewsCategory;
use App\Plugins\File;

class DeleteController extends Controller 
{
    public function index(Request $request)
    {
        $data           = $request->input();
        $newsCategories = NewsCategory::GetNewsCategoriesInID($data['news_category_id']);
        $images         = array_merge(
                array_column($newsCategories, 'news_category_avatar_high'),
                array_column($newsCategories, 'news_category_avatar_medium')
        );
        
        if(!NewsCategory::DeleteNewsCategoriesInID($data['news_category_id']))
        {
            echo json_encode(array(
                'error'   => 'error'
            ));
            exit();
        }
        $file = new File();
        $file->delete('./', $images);
        echo json_encode(array(
            'success'   => 'success'
        ));
        exit();
    }
}