<?php

namespace App\Http\Controllers\Admin\NewsCategory;

use App\Http\Controllers\Controller;

use App\Models\Admin\News\NewsCategory;

class IndexController extends Controller 
{
    public function index()
    {
        return response()->view('admin.news-category.index', array(
            'newsCategories'   => NewsCategory::GetNewsCategoriesSorted()
        ));
    }
}