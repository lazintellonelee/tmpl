<?php

namespace App\Http\Controllers\Admin\NewsCategory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\News\NewsCategory;

class ChangeStatusController extends Controller 
{
    public function index(Request $request) 
    {
        $data = $request->input();
        if(!NewsCategory::EditNewsCategoryByID($data['news_category_id'], array(
            'news_category_status' => $data['news_category_status']
        )))
        {
            echo json_encode(array(
                'error' => 'error'
            ));
            exit();
        }
        echo json_encode(array(
            'success' => 'success'
        ));
        exit();
    }
}