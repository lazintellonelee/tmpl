<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\User;

class IndexController extends Controller 
{
    public function index()
    {
        return response()->view('admin.home');
    }
    
    public function login(Request $request)
    {
        if(check_user_logined()) 
        {
            return redirect()->route('admin-home');
        }
        
        if($request->isMethod('post'))
        {
            $data   = $request->input();
            $user   = User::GetUserByEmailOrName($data['user_name'], $data['user_name']);
            if(!$user)
            {
                return response()->view('admin.login', array(
                    'errors'    => array(
                        'Tài khoản đăng nhập không tồn tại.'
                    ),
                    'data'      => $data
                ));
            }
            if(md5($data['user_password'].$user['user_salt']) != $user['user_password'])
            {
                return response()->view('admin.login', array(
                    'errors'    => array(
                        'Mật khẩu đăng nhập không chính xác.'
                    ),
                    'data'      => $data
                ));
            }
            
            $request->session()->put('user_info', array(
                'user_id'       => $user['user_id'],
                'user_name'     => $user['user_name'],
                'user_fullname' => $user['user_fullname'],
                'user_type'     => $user['user_type']
            ));
            if(isset($data['remember_me']))
            {
                return redirect()->route('admin-home')->withCookie(cookie()->forever('user_info', array(
                    'user_id'       => $user['user_id'],
                    'user_name'     => $user['user_name'],
                    'user_fullname' => $user['user_fullname'],
                    'user_type'     => $user['user_type']
                )));
            }
            return redirect()->route('admin-home');
        }
        return response()->view('admin.login');
    }
    
    public function logout()
    {
        if(request()->session()->has('user_info'))
        {
            request()->session()->clear();
        }
        
        return redirect()->route('admin-login')->withCookie(cookie()->forget('user_info'));
    }
}