<?php

namespace App\Http\Controllers\Admin\Faq;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\Admin\Faq\Faq;

class ChartController extends Controller {
    public function faqView(Request $request){
        $data   = $request->input();
        $date   = date('m');
        
        if($request->has('date')) {
            $date = $data['date'];
        }
        
        $faq = Faq::select(DB::raw('COUNT(faq_id) AS faq, EXTRACT(DAY FROM created_at) AS date'))
                    ->whereRaw(DB::raw('EXTRACT(MONTH FROM created_at) = ' . $date))
                    ->groupBy(DB::raw('EXTRACT(DAY FROM created_at)'))
                    ->get();
        
        if($faq){
            return response()->json($faq->toArray());
        }
        return response()->json(array());
    }
}