<?php

namespace App\Http\Controllers\Admin\Faq;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Admin\Faq\Faq;

class EditController extends Controller 
{
    public function index(Request $request, $id) {
        $faq           = Faq::GetFaqByID($id);

        if(!$faq){
            return redirect()->route('admin-faq-home');
        }
        
        if($request->isMethod('post')){
            $data   = $request->input();
            DB::beginTransaction();
            
            $update = Faq::EditFaqByID($id, $data);
            if(!$update){
                DB::rollback();
                return response()->view('admin.faq.edit', array(
                    'faq'   => $faq,
                    'errors'    => array(
                        'Cập nhật câu hỏi không thành công! Vui lòng thử lại'
                    )
                ));
            }
            
            DB::commit();
            $request->session()->flash('success', 'Cập nhật câu hỏi thành công.');
        }
        
        return response()->view('admin.faq.edit', array(
            'faq'   => $faq,
        ));
    }
}