<?php

namespace App\Http\Controllers\Admin\Faq;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Admin\Faq\Faq;

class DeleteController extends Controller {
    public function index(Request $request){
        $data   = $request->input();

        $faq   = Faq::GetFaqInID($data['faq_id']);
        
        DB::beginTransaction();
        if(!Faq::DeleteInID($data['faq_id'])){
            DB::rollback();
            echo json_encode(array(
                'error' => 'error1'
            )); 
            exit();
        }
        
        DB::commit();
        echo json_encode(array(
            'success'   => 'success'
        ));
        exit();
    }
}