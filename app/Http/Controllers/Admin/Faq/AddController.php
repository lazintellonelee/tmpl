<?php

namespace App\Http\Controllers\Admin\Faq;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Admin\Faq\Faq;

class AddController extends Controller
{
    public function index(Request $request){
        if($request->isMethod('post')){
            $data   = $request->input();
            DB::beginTransaction();
            
            $data['created_by'] = get_user_id();
            
            $faq    = Faq::AddFaq($data);
            
            if(!$faq) {
                DB::rollback();
                return response()->view('admin.faq.add', array(
                    'errors'    => array(
                        'Tạo câu hỏi không thành công! Vui lòng thử lại.'
                    )
                ));
            }
            
            DB::commit();
            $request->session()->flash('success', 'Tạo câu hỏi thành công!');
        }
        return response()->view('admin.faq.add');
    }
}