<?php

namespace App\Http\Controllers\Admin\Faq;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\Faq\Faq;

class ChangeStatusController extends Controller 
{
    public function index(Request $request)
    {
        $data = $request->input();
        if(!Faq::EditFaqByID($data['faq_id'], $data))
        {
            echo json_encode(array(
               'error'  => 'error'
            ));
            exit();
        }
        echo json_encode(array(
            'success'   => 'success'
        ));
        exit();
    }
}