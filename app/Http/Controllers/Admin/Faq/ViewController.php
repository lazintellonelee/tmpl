<?php

namespace App\Http\Controllers\Admin\Faq;

use App\Http\Controllers\Controller;

use App\Models\Admin\Faq\Faq;

class ViewController extends Controller 
{
    public function index($id)
    {
        $faq = Faq::where('faq_id', $id)
                          ->first();
        
        return response()->view('admin.faq.view', array(
            'faq' => $faq
        ));
    }
}