<?php

namespace App\Http\Controllers\Admin\Faq;

use App\Http\Controllers\Controller;

use App\Models\Admin\Faq\Faq;

class IndexController extends Controller
{
    public function index() {
        return response()->view('admin.faq.home', array(
            'faq'  => Faq::GetFaq()
        ));
    }
}