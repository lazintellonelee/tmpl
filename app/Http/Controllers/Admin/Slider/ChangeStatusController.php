<?php

namespace App\Http\Controllers\Admin\Slider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\Slider\Slider;

class ChangeStatusController extends Controller 
{
    public function index(Request $request)
    {
        $data = $request->input();
        if(!Slider::EditSlideByID($data['slide_id'], $data))
        {
            return response()->json(array('error' => 'error'));
        }
        return response()->json(Slider::GetSliderByID($data['slide_id']));
    }
}