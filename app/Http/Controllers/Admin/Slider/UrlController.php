<?php

namespace App\Http\Controllers\Admin\Slider;

use App\Http\Controllers\Controller;

use App\Models\Admin\News\News;
use App\Models\Admin\News\NewsCategory;

class UrlController extends Controller 
{
    public function index($type)
    {
        switch($type) {
            case "news" :
                $news = News::get(array(
                    'news_id',
                    'news_title',
                    'news_url'
                ));
                if($news) 
                {
                    $news = $news->toArray();
                    return response()->json($news);
                }
                return response()->json(array());
            case "newsCategory" :
                $newsCategories = NewsCategory::get(array(
                    'news_category_id',
                    'news_category_name',
                    'news_category_url',
                ));
                if($newsCategories)
                {
                    $newsCategories = $newsCategories->toArray();
                    return response()->json($newsCategories);
                }
                return response()->json(array());
        }
    }
}