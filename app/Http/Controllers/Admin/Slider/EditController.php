<?php

namespace App\Http\Controllers\Admin\Slider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Plugins\SEO;
use App\Plugins\File;
use App\Plugins\PHPThumb\Thumbnail;
use App\Models\Admin\Slider\Slider;

class EditController extends Controller 
{
    public function index(Request $request, $id) 
    {
        if($id == 0) return redirect()->route('admin-home-slider');
        $slider = Slider::GetSliderByID($id);
        if(!$slider) return redirect()->route('admin-home-slider');
        
        if($request->isMethod('post'))
        {
            $data       = $request->input();
            $file       = new File();
            $imageName  = str_replace(' ', '-', strtolower(SEO::RemoveVN($data['slide_title'])).'-'.  uniqid());
            if($request->hasFile('slide_image') && $data['slide_image_base64'] == '')
            {
                $file->setFile($request->file('slide_image'));
                if(!$file->isImage())
                {
                    return response()->view('admin.slider.add', array(
                        'errors'    => array(
                            'Tập tin tải lên không phải là hình ảnh!'
                        )
                    ));
                }
                $data['slide_image_high'] = $file->move('./public/upload/images/slider/high', $imageName);
            }
            
            if($data['slide_image_base64'] != '')
            {
                $data['slide_image_high'] = File::BuildImageFromBase64($data['slide_image_base64'], $imageName, './public/upload/images/slider/high/');
            }
            
            if(isset($data['slide_image_high']))
            {
                $thumbnail = new Thumbnail($data['slide_image_high'], './public/upload/images/slider/high/');
                $thumbnail->Scale(175, 175*3/6);
                $thumbnail->Save('./public/upload/images/slider/low/');

                $data['slide_image_low']   = 'public/upload/images/slider/low/' . $data['slide_image_high'];
                $data['slide_image_high']  = 'public/upload/images/slider/high/' . $data['slide_image_high'];
            }
            
            if(!Slider::EditSlideByID($id, $data))
            {
                $file->delete('./', $data['slide_image_low']);
                $file->delete('./', $data['slide_image_high']);
                return response()->view('admin.slider.edit', array(
                    'errors'    => array(
                        'Cập nhật slide không thành công! Vui lòng thử lại.'
                    )
                ));
            }
            
            if(isset($data['slide_image_high'])) 
            {
                if($slider['slide_image_high']) 
                {
                    $file->delete('./', $slider['slide_image_high']);
                    $file->delete('./', $slider['slide_image_low']);
                }
            }
            
            $request->session()->flash('success', 'Cập nhật slide thành công.');
            return redirect()->route('admin-slider-home');
        }
        
        return response()->view('admin.slider.edit', array(
            'slider'    => $slider
        ));
    }
}