<?php

namespace App\Http\Controllers\Admin\Slider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Plugins\File;
use App\Models\Admin\Slider\Slider;

class DeleteController extends Controller 
{
    public function index(Request $request)
    {
        $data       = $request->input();
        $sliders    = Slider::GetSlidersInID($data['slide_id']);
        if(!$sliders) 
            return response()->json(array('success' => 'success'));
        if(!Slider::DeleteSlidersInID($data['slide_id']))
            return response()->json(array('error' => 'error'));
        $file       = new File();
        foreach(array_column($sliders, 'slide_image_high') as $image)
        {
            if($image != '')
            {
                $file->delete('./', $image);
            }
        }
        foreach(array_column($sliders, 'slide_image_low') as $image)
        {
            if($image != '')
            {
                $file->delete('./', $image);
            }
        }
        return response()->json(array('success' => 'success'));
    }
}