<?php

namespace App\Http\Controllers\Admin\Slider;

use App\Http\Controllers\Controller;

use App\Models\Admin\Slider\Slider;

class IndexController extends Controller 
{
    public function index() 
    {
        return response()->view('admin.slider.index', array(
            'sliders_active'    => Slider::GetSlidersActive(),
            'sliders'           => Slider::GetSliders()
        ));
    }
}

