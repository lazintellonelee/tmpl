<?php

namespace App\Http\Controllers\Admin\EmailPromotion;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\Admin\EmailPromotion\EmailPromotion;

class ChartController extends Controller {
    public function emailpromotionView(Request $request){
        $data   = $request->input();
        $date   = date('m');
        
        if($request->has('date')) {
            $date = $data['date'];
        }
        
        $emailpromotion = EmailPromotion::select(DB::raw('COUNT(emailpromotion_id) AS emailpromotion, EXTRACT(DAY FROM created_at) AS date'))
                                        ->whereRaw(DB::raw('EXTRACT(MONTH FROM created_at) = ' . $date))
                                        ->groupBy(DB::raw('EXTRACT(DAY FROM created_at)'))
                                        ->get();
        
        if($emailpromotion){
            return response()->json($emailpromotion->toArray());
        }
        return response()->json(array());
    }
}