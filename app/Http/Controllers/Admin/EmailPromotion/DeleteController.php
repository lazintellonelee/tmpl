<?php

namespace App\Http\Controllers\Admin\EmailPromotion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Admin\EmailPromotion\EmailPromotion;

class DeleteController extends Controller {
    public function index(Request $request){
        $data   = $request->input();
        
        DB::beginTransaction();
        if(!EmailPromotion::DeleteInID($data['emailpromotion_id'])){
            DB::rollback();
            
            echo json_encode(array(
                'error' => 'error1'
            )); 
            exit();
        }
        
        DB::commit();
        echo json_encode(array(
            'success'   => 'success'
        ));
        exit();
    }
}