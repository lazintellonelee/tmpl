<?php

namespace App\Http\Controllers\Admin\EmailPromotion;

use App\Http\Controllers\Controller;

use App\Models\Admin\EmailPromotion\EmailPromotion;

class IndexController extends Controller
{
    public function index() {
        return response()->view('admin.emailpromotion.home', array(
            'emailpromotion'  => EmailPromotion::GetEmailPromotion()
        ));
    }
    
    public function jsonEmailPromotion(){
        $emailpromotion = EmailPromotion::GetEmailPromotion();
        if(!$emailpromotion){
            $emailpromotion = array();
        }
        return response()->json($emailpromotion);
    }
}