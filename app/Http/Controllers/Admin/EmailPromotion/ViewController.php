<?php

namespace App\Http\Controllers\Admin\EmailPromotion;

use App\Http\Controllers\Controller;

use App\Models\Admin\EmailPromotion\EmailPromotion;

class ViewController extends Controller 
{
    public function index($id)
    {
        $emailpromotion = EmailPromotion::where('emailpromotion_id', $id)
                          ->first();
        
        return response()->view('admin.emailpromotion.view', array(
            'emailpromotion' => $emailpromotion
        ));
    }
}