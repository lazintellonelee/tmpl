<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// For Login
    Route::match(array('get', 'post'), 'admin/login.html', array(
        'as'    => 'admin-login',
        'uses'  => 'Admin\IndexController@login'
    ));
    // /For Login

Route::group(array(
    'prefix'        => 'admin',
    'namespace'     => 'Admin',
    'middleware'    => array('admin.auth')
), function() {
    Route::get('/home.html', array(
        'as'    => 'admin-home',
        'uses'  => 'IndexController@index'
    )); 
    
    // For Logout
    Route::get('/logout.html', array(
        'as'    => 'admin-logout',
        'uses'  => 'IndexController@logout'
    ));
    // /For Logout
    
    // For News
    require_once 'Routes/Admin/News/news.php';
    require_once 'Routes/Admin/News/news-category.php';
    require_once 'Routes/Admin/News/news-tag.php';
    // /For News
    
    // For Slider
    require_once 'Routes/Admin/Slider/slider.php';
    // /For Slider
    
    // /For Partner
    require_once "Routes/Admin/Partner/partner.php";
    // /For Partner
    
    // /For Feedback
    require_once "Routes/Admin/Feedback/feedback.php";
    // /For Feedback
    
    // /For Article
    require_once "Routes/Admin/Article/article.php";
    // /For Article
    
    // /For Faq
    require_once "Routes/Admin/Faq/faq.php";
    // /For Faq
    
    // /For Faq
    require_once "Routes/Admin/EmailPromotion/emailpromotion.php";
    // /For Faq
});
