<?php

namespace App\Http\Middleware;

use Closure;

class AuthenticateAdmin
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!request()->session()->has('user_info'))
        {
            if(!request()->cookie('user_info'))
            {
                return redirect()->route('admin-login');
            }
        }

        return $next($request);
    }

}
