<ul class="sidebar-menu">
    <li><a href=""><i class="fa fa-fw fa-dashboard"></i> <span>Tổng quan</span></a></li>
    
    <li><a href="{{ route('admin-slider-home') }}"><i class="fa fa-fw fa-sliders"></i> <span>Quản lý sliders trang chủ</span></a></li>
    
    <li class="treeview">
        <a href="#"><i class="fa fa-fw fa-newspaper-o"></i> <span>Quản lý Tin Tức</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
            <li><a href="{{ route('admin-news-home') }}"><i class="ion ion-ios-list-outline"></i> Danh sách Tin Tức</a></li>
            <li><a href="{{ route('admin-news-category-home') }}"><i class="ion ion-android-list"></i> Danh mục Danh Mục</a></li>
            <li><a href="{{ route('admin-news-tag-home') }}"><i class="ion ion-ios-pricetag"></i> Danh mục Thẻ Tags</a></li>
        </ul>
    </li>
    
    <li><a href="{{ route('admin-article-home') }}"><i class="fa fa-fw fa-file-text-o"></i> <span>Quản lý góc chia sẽ</span></a></li>
    
    <li><a href="{{ route('admin-faq-home') }}"><i class="fa fa-fw fa-question"></i> <span>Quản lý hỏi đáp</span></a></li>
    
    <li><a href="{{ route('admin-partner-home') }}"><i class="fa fa-fw fa-share-alt"></i> <span>Quản lý đối tác</span></a></li>
    
    <li><a href="{{ route('admin-emailpromotion-home') }}"><i class="fa fa-fw fa-envelope-o"></i> <span>Quản lý email khuyến mãi</span></a></li>
    
    <li><a href=""><i class="ion ion-person-add"></i> <span>Quản lý tuyển dụng</span></a></li>
    
    <li><a href=""><i class="ion ion-android-chat"></i> <span>Quản lý liên hệ</span></a></li>
    
    <li><a href="{{ route('admin-feedback-home') }}"><i class="fa fa-fw fa-commenting"></i> <span>Quản lý phản hồi</span></a></li>
    
    <li class="treeview">
        <a href=""><i class="fa fa-fw fa-lock"></i> <span>Quản lý tài khoản quản trị</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
            <li><a href=""><i class="ion ion-person"></i> Danh sách tài khoản</a></li>
        </ul>
    </li>
    
    <li><a href=""><i class="fa fa-fw fa-cogs"></i> <span>Quản lý cấu hình website</span></a></li>
</ul>