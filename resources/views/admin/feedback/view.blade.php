@extends('layouts.layout-admin')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <a href="{{ route('admin-feedback-home') }}"><i class="fa fa-fw fa-chevron-circle-left"></i></a> Thông tin chi tiết
                        <i class="fa fa-fw fa-edit"></i>
                    </a>
                </h3>
            </div>
            <div class="box-body">
                <div class="clearfix"></div>

                <div class="col-sm-12">
                    <table class="table table-responsive">
                        <tbody>
                            <tr>
                                <td style="vertical-align:middle"><b>Tên người gửi phản hồi</b></td>
                                <td style="vertical-align:middle">{{ $feedback['feedback_name'] }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align:middle"><b>Email</b></td>
                                <td style="vertical-align:middle">{{ $feedback['feedback_email'] }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align:middle"><b>Điện thoại</b></td>
                                <td style="vertical-align:middle">{{ $feedback['feedback_phone'] }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align:middle"><b>Địa chỉ</b></td>
                                <td style="vertical-align:middle">{{ $feedback['feedback_address'] }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align:middle"><b>Ngày gửi</b></td>
                                <td style="vertical-align:middle">{{ change_vn_date(strtotime($feedback['created_at'])) }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align:middle"><b>Tiêu đề</b></td>
                                <td style="vertical-align:middle">{{ $feedback['feedback_title'] }}</td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default" style="border-radius: 0px">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a>
                                        Nội dung phản hồi
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    {!! $feedback['feedback_description'] !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

