@extends('layouts.layout-admin')

@section('javascripts')
<script src="{{ url('public/plugins/cropper-master/dist/cropper.min.js') }}"></script>
<script src="{{ url('public/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ url('public/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ url('public/plugins/ckeditor/config.js') }}"></script>
<script src="{{ url('public/plugins/typeahead/typeahead.bundle.js') }}"></script>
<script src="{{ url('public/plugins/angular/angular.min.js') }}"></script>
<script src="{{ url('public/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}" ></script>
<script src="{{ url('public/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput-angular.min.js') }}"></script>
<script src="{{ url('public/javascripts/admin/news/add-script.js') }}"></script>
@endsection

@section('stylesheets')
<link href="{{ url('public/plugins/cropper-master/dist/cropper.min.css') }}" rel="stylesheet" />
<link href="{{ url('public/plugins/select2/select2.min.css') }}" rel="stylesheet" />
<link href="{{ url('public/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<style type="text/css">
    strong.select2-results__group {margin-left: 7px}
    ul.select2-results__options--nested li {padding-left: 50px !important}
    .bootstrap-tagsinput {width: 100%;border-radius: 0px !important;}
    .tt-menu {background-color: #fff;width: 500px !important;top: 27px !important;left:-7px !important}
    .tt-menu .tt-suggestion {padding: 5px 10px 5px 10px;}
    .tt-cursor {background-color: #31B0D5;color: #fff;}
</style>
@endsection

@section('content-header')
<h1>
    Trang tổng quát
    <small>Trình quản lý</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('admin-home') }}"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
    <li class="active"><a href="{{ route('admin-news-home') }}">Danh sách tin tức</a></li>
    <li class="active">Thêm tin tức</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        @if(session()->get('success'))
        <div class="alert alert-success" role="alert">
            <i class="fa fa-fw fa-check"></i>{{ session()->get('success') }}
        </div>
        @endif
        
        @if(count($errors)>0)
        <div class="alert alert-warning" role="alert">
            @foreach($errors as $error)
            <i class="fa fa-fw fa-exclamation-triangle"></i> {{ $error }}
            @endforeach
        </div>
        @endif
        
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><a href="{{ route('admin-news-home') }}"><i class="fa fa-fw fa-chevron-circle-left"></i></a> Thêm tin tức</h3>
            </div>
            
            <div class="box-body form-horizontal">
                <form class="form-horizontal" method="post" id="form-add-news" enctype="multipart/form-data">
                    {{ csrf_field() }}
                </form>
                    
                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Danh mục tin tức</label>
                    <div class="col-sm-10">
                        <select form="form-add-news" class="form-control" name="news_category_id" id="news_catgory_id" data-placeholder="Danh mục tin tức...">
                            <option></option>
                            {!! \App\Models\Admin\News\NewsCategory::GetNewsCategoriesOptionHTML(0, 0, true) !!}
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Tiêu đề tin tức</label>
                    <div class="col-sm-10">
                        <input form="form-add-news" name="news_title" type="text" class="form-control" placeholder="Tiêu đề tin tức ..." />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Mô tả tin tức</label>
                    <div class="col-sm-10">
                        <textarea form="form-add-news" name="news_description" class="form-control" rows="3" style="resize:none" placeholder="Mô tả tin tức ..."></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Nội dung tin tức</label>
                    <div class="col-sm-10">
                        <textarea form="form-add-news" name="news_content" class="form-control" rows="3" placeholder="Nội dung tin tức ..." id="news_content"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Ảnh đại diện</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input form="form-add-news" type="hidden" name="news_avatar_base64" />
                            <input form="form-add-news" type="file" class="form-control" name="news_avatar" id="news_avatar" placeholder="http:// liên kết ngoài ..." />
                            <span class="input-group-btn">
                                <button class="btn btn-default no-radius" type="button" id="btn-crop-news-avatar">
                                    <i class="ion ion-crop"></i>
                                </button>
                            </span>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle no-radius" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tải lên <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right no-radius">
                                    <li><a class="change-formality-news-avatar" data-formality="server" href="#">Liên kết ngoài</a></li>
                                    <li class="active"><a class="change-formality-news-avatar"  data-formality="url" href="#">Tải lên</a></li>
                                </ul>
                            </div><!-- /btn-group -->
                        </div><!-- /input-group -->
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="col-sm-12" style="height:300px;background-color:#000;display:none;background-position:center;background-size:contain;background-repeat:no-repeat" id="div-news-avatar-review"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Tags tin tức</label>
                    <div class="col-sm-10">
                        <input form="form-add-news" type="text" class="form-control" placeholder="Tags tin tức ..." name="news_tags" id="news_tags" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Tin liên quan</label>
                    <div class="col-sm-10">
                        <input form="form-add-news" name="news_related" id="news_related" type="text" class="form-control" placeholder="Tin liên quan ..." />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Tác giả</label>
                    <div class="col-sm-10">
                        <input form="form-add-news" name="news_author" type="text" class="form-control" placeholder="Người tạo ..." />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Tên nguồn</label>
                    <div class="col-sm-4">
                        <input form="form-add-news" name="news_source_name" type="text" class="form-control" placeholder="Tên nguồn tin tức ..." />
                    </div>
                    <label class="col-sm-2 control-label text-left">Link nguồn</label>
                    <div class="col-sm-4">
                        <input form="form-add-news" name="news_source_url" type="text" class="form-control" placeholder="http:// Link nguồn tin tức ..." />
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button form="form-add-news" class="btn btn-primary no-radius" id="btn-submit-form-add-news">Tạo tin tức</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('cropper-master.modal')
@endsection
