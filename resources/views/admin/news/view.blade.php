@extends('layouts.layout-admin')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <a href="{{ route('admin-news-home') }}"><i class="fa fa-fw fa-chevron-circle-left"></i></a> Thông tin chi tiết
                </h3>
            </div>
            <div class="box-body">
                <div class="col-sm-4">
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>{{ $news['news_view'] or 0 }}</h3>
                            <p>Lượt xem</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios-eye"></i>
                        </div>
                        <a href="#" class="small-box-footer">&nbsp</a>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>{{ $news['news_likes'] or 0 }}</h3>
                            <p>Lượt thích</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-thumbsup"></i>
                        </div>
                        <a href="#" class="small-box-footer">&nbsp</a>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>53</h3>
                            <p>Bình luận</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-chatbubble-working"></i>
                        </div>
                        <a href="#" class="small-box-footer">&nbsp</a>
                    </div>
                </div>
                
                <div class="clearfix"></div>
                
                <div class="col-sm-12">
                    <table class="table table-responsive">
                        <tbody>
                            <tr>
                                <td rowspan="4" style="width:250px;position:relative">
                                    @if($news['news_avatar_url'])
                                    <img class="img-responsive" src="{{ $news['news_avatar_url'] }}"  />
                                    @elseif($news['news_avatar_low'])
                                    <img class="img-responsive" width="75px" height="37.5px" src="{{ url($news['news_avatar_low']) }}"  />
                                    @else
                                    <img class="img-responsive" width="75px" height="37.5px" src="{{ url('public/images/no-image-available.jpg') }}"  />
                                    @endif
                                    
                                    @if($news['news_verified']=='1')
                                    <i style="position:absolute;bottom:7px;right:14px;font-size:55px" class="text-lime ion ion-ios-checkmark-outline"></i>
                                    @endif
                                </td>
                                <td style="vertical-align:middle"><b>Tiêu đề</b></td>
                                <td style="vertical-align:middle">{{ $news['news_title'] }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align:middle"><b>Danh mục</b></td>
                                <td style="vertical-align:middle">{{ $news['news_category_name'] }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align:middle"><b>Tác giả</b></td>
                                <td style="vertical-align:middle">{{ $news['news_author'] }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align:middle"><b>Ngày tạo</b></td>
                                <td style="vertical-align:middle">{{ change_vn_date(strtotime($news['created_at'])) }}</td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default" style="border-radius: 0px">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a>
                                        Nội dung tin tức
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    {!! $news['news_content'] !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

