@extends('layouts.layout-admin')

@section('javascripts')
<script src="{{ url('public/plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ url('public/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ url('public/plugins/chart-js-master/Chart.js') }}"></script>
<script src="{{ url('public/javascripts/admin/news/home-script.js') }}"></script>
<script src="{{ url('public/javascripts/admin/news/chart-script.js') }}"></script>
@endsection

@section('stylesheets')
<link href="{{ url('public/plugins/bootstrap-toggle-master/css/bootstrap-toggle.min.css') }}" rel="stylesheet" />
<link href="{{ url('public/themes/admin-lte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet"  />
<link href="{{ url('public/themes/admin-lte/plugins/iCheck/all.css') }}" rel="stylesheet" />
<link href="{{ url('public/plugins/select2/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content-header')
<h1>
    Trang tổng quát
    <small>Trình quản lý</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('admin-home') }}"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
    <li class="active"><a href="{{ route('admin-news-home') }}">Danh sách tin tức</a></li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        @if(session()->get('success'))
        <div class="alert alert-success" role="alert">
            <i class="fa fa-fw fa-check"></i>{{ session()->get('success') }}
        </div>
        @endif
        
        @if($news)
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <a href="#">
                        <i class="fa fa-fw fa-dashboard"></i>
                    </a> Thông tin tổng quan
                </h3>
            </div>
            <div class="box-body">
                <div class="col-sm-3">
                    <div class="small-box bg-green" style="height:175px">
                        <div class="inner" style="height:148px">
                            <h3>{{ $news->total() }}</h3>
                            <p>Tin tức</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-fw fa-newspaper-o"></i>
                        </div>
                        <a href="#" class="small-box-footer">&nbsp</a>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <canvas id="chart-bar-news-view" height="50"></canvas>
                    <h4>
                        Biểu đồ số lượng tin tức trong tháng&nbsp;
                        <select class="form-control" style="width:70px;display:inline-block" id="select-chart-news-views-date">
                            @for($i=1;$i<=12;$i++)
                            <option <?= $i==date('m')?'selected':'' ?> value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                        &nbsp;năm&nbsp;
                        <select class="form-control" style="width:80px;display:inline-block" id="select-chart-news-views-year">
                           @for($i=date('Y')-3;$i<=date('Y')+3;$i++)
                           <option <?= $i==date('Y')?'selected':'' ?> value='{{ $i }}'>{{ $i }}</option>
                           @endfor
                        </select>
                        &nbsp;- <span id="total-for-chart"></span>
                    </h4>
                </div>
            </div>
        </div>
        @endif
        
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <a href="{{ route('admin-news-add') }}">
                        <i class="fa fa-fw fa-plus-square"></i>
                    </a> Danh sách tin tức
                </h3>
            </div>
            
            <div class="col-sm-12 form-horizontal no-padding" style="margin:15px 0px 15px 0px">
                <label class="col-sm-2 control-label" style="text-align:left">Danh mục tin tức</label>
                <div class="col-sm-4">
                    <select form="form-add-news" class="form-control" name="news_category_id" id="news_category_id" data-placeholder="Danh mục tin tức...">
                        <option></option>
                        <option value="0">ALL</option>
                        {!! \App\Models\Admin\News\NewsCategory::GetNewsCategoriesOptionHTML(0, request()->query('ct')?request()->query('ct'):0, true) !!}
                    </select>
                </div>
                <label class="col-sm-1 control-label" style="text-align:left">Tiêu đề</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Tìm theo tiêu đề tin tức ...">
                        <span class="input-group-btn">
                            <button class="btn btn-primary no-radius" type="button">Tìm</button>
                        </span>
                    </div>
                </div>
                <div class="col-sm-1 text-right">
                    <button class="btn btn-warning no-radius">
                        <i class="fa fa-fw fa-trash"></i>
                    </button>
                </div>
            </div>
            
            <div class="box-body">
                <table class="table table-bordered" id="table_news">
                    <thead>
                        <tr>
                            <th style="width:30px">
                                <input type="checkbox" class="icheck" id="check-all-news" />
                            </th>
                            <th style="width:30px">Mã</th>
                            <th>Tên</th>
                            <th>Tác Vụ</th>
                            <th>Ngày Tạo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($news) > 0)
                        @foreach($news as $n)
                        <tr data-news-id="{{ $n['news_id'] }}">
                            <td style="width:30px;vertical-align:middle">
                                <input type="checkbox" class="icheck check-news" value="{{ $n['news_id'] }}" />
                            </td>
                            <td style="width:30px;vertical-align:middle">{{ $n['news_id'] }}</td>
                            <td style="position:relative">
                                @if($n['news_avatar_url'])
                                <img width="75px" height="37.5px" src="{{ $n['news_avatar_url'] }}"  />
                                @elseif($n['news_avatar_low'])
                                <img width="75px" height="37.5px" src="{{ url($n['news_avatar_low']) }}"  />
                                @else
                                <img width="75px" height="37.5px" src="{{ url('public/images/no-image-available.jpg') }}"  />
                                @endif
                                <a href="#" class="<?= $n['news_verified']==1?'bg-green':'bg-info'?> change-verified" data-news-id="{{ $n['news_id'] }}" data-news-verified="{{ $n['news_verified'] }}" style="position:absolute;left:65px">
                                    <i class="fa fa-fw fa-<?= $n['news_verified']==1?'check':'times'?>"></i>
                                </a>
                                &nbsp;&nbsp;
                                <a href="{{ route('admin-news-view', array('id' => $n['news_id'])) }}">{{ $n['news_title'] }}</a>
                            </td>
                            <td style="width:220px;text-align:center;vertical-align:middle">
                                <input type="checkbox" class="bootstrap-toggle change-status-news" data-news-id="{{ $n['news_id'] }}" <?= $n['news_status']==1?'checked':''?> />
                                <a href="{{ route('admin-news-edit', array('id' => $n['news_id'])) }}" class="btn btn-default btn-sm no-radius">
                                    <i class="fa fa-fw fa-pencil"></i> Sửa
                                </a>
                                <a data-news-id="{{ $n['news_id'] }}" class="btn btn-warning btn-sm no-radius btn-delete-news">
                                    <i class="fa fa-fw fa-trash"></i> Xoá
                                </a>
                            </td>
                            <td style="vertical-align:middle">{{ change_vn_date(strtotime($n['created_at'])) }}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5" class="text-center">
                                Không có tin tức!
                            </td> 
                        </tr>
                        @endif
                    </tbody>
                </table>
                
                <div class="text-right">{!! $news->appends(request()->query())->render() !!}</div>
            </div>
        </div>
    </div>
</div>
@endsection