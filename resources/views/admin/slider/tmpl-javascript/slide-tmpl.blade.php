<script type="text/x-tmpl" id="tmpl-slide">
    <div class="box">
        <div class="box-body">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-slide-id="{%= o.slide_id%}" data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div data-slide-id="{%= o.slide_id%}" class="item active">
                        <img src="<?= url()?>/{%= o.slide_image_high%}" style="height:250px; width:100%" alt="...">
                        <div class="carousel-caption">
                            <h3>{%= o.slide_title%}</h3>
                            <p>{%= o.slide_content%}</p>
                            <p>
                                <a href="<?= url()?>/admin/slider/{%= o.slide_id%}/edit.html" class="btn btn-default no-radius">
                                    <i class="fa fa-fw fa-pencil"></i> Sửa
                                </a>
                                <a data-slide-id="{%= o.slide_id%}" class="btn btn-danger no-radius btn-turn-off-slide">
                                    <i class="fa fa-fw fa-power-off"></i> Tắt
                                </a>
                            </p>
                        </div>
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</script>

<script type="text/x-tmpl" id="tmpl-carousel-indicators">
    <li data-slide-id="{%= o.slide_id%}" data-target="#carousel-example-generic" data-slide-to="{%= o.slide_to%}"></li>
</script>

<script type="text/x-tmpl" id="tmpl-carousel-inner">
    <div data-slide-id="{%= o.slide_id%}" class="item">
        <img src="<?= url()?>/{%= o.slide_image_high%}" style="height:250px; width:100%" alt="...">
        <div class="carousel-caption">
            <h3>{%= o.slide_title%}</h3>
            <p>{%= o.slide_content%}</p>
            <p>
                <a href="<?= url()?>/admin/slider/{%= o.slide_id%}/edit.html" class="btn btn-default no-radius">
                    <i class="fa fa-fw fa-pencil"></i> Sửa
                </a>
                <a data-slide-id="{%= o.slide_id%}" class="btn btn-danger no-radius btn-turn-off-slide">
                    <i class="fa fa-fw fa-power-off"></i> Tắt
                </a>
            </p>
        </div>
    </div>
</script>