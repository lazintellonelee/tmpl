@extends('layouts.layout-admin')

@section('javascripts')
<script src="{{ url('public/plugins/javascript-template/tmpl.min.js') }}"></script>
<script src="{{ url('public/plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ url('public/javascripts/admin/slider/index-script.js') }}"></script>
@endsection

@section('stylesheets')
<link href="{{ url('public/plugins/bootstrap-toggle-master/css/bootstrap-toggle.min.css') }}" rel="stylesheet" />
<link href="{{ url('public/themes/admin-lte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet"  />
<link href="{{ url('public/themes/admin-lte/plugins/iCheck/all.css') }}" rel="stylesheet" />
@endsection

@section('content-header')
<h1>
    Trang tổng quát
    <small>Trình quản lý</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('admin-home') }}"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
    <li class="active"><a href="{{ route('admin-slider-home') }}">Slide</a></li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        @if(session()->get('success'))
        <div class="alert alert-success" role="alert">
            <i class="fa fa-fw fa-check"></i>{{ session()->get('success') }}
        </div>
        @endif
        
        @if($sliders_active)
        <div class="box">
            <div class="box-body">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        @foreach($sliders_active as $slider)
                        <li data-slide-id="{{ $slider['slide_id'] }}" data-target="#carousel-example-generic" data-slide-to="{{ array_search($slider, $sliders_active) }}" class="{{ array_search($slider, $sliders_active)=='0'?'active':'' }}"></li>
                        @endforeach
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        @foreach($sliders_active as $slider)
                        <div data-slide-id="{{ $slider['slide_id'] }}" class="item {{ array_search($slider, $sliders_active)=='0'?'active':'' }}">
                            <img src="{{ url($slider['slide_image_high']) }}" style="height:250px; width:100%" alt="...">
                            <div class="carousel-caption">
                                <h3>{{ $slider['slide_title'] }}</h3>
                                <p>{{ $slider['slide_content'] }}</p>
                                <p>
                                    <a href="{{ route('admin-slider-edit', array('id' => $slider['slide_id'])) }}" class="btn btn-default no-radius">
                                        <i class="fa fa-fw fa-pencil"></i> Sửa
                                    </a>
                                    <a data-slide-id="{{ $slider['slide_id'] }}" class="btn btn-danger no-radius btn-turn-off-slide">
                                        <i class="fa fa-fw fa-power-off"></i> Tắt
                                    </a>
                                </p>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        @endif
        
        <div class="box" id="main-box">
            <div class="box-header with-border">
                <h3 class="box-title"><a href="{{ route('admin-slider-add') }}"><i class="fa fa-fw fa-plus-square"></i></a> Slide trang chủ</h3>
            </div>
            
            <div class="box-body">
                <table class="table table-bordered" id="table_sliders">
                    <thead>
                        <tr>
                            <th>
                                <input type="checkbox" class="icheck" id="check-all-slider" />
                            </th>
                            <th>STT</th>
                            <th>Tiêu đề</th>
                            <th>Tác vụ</th>
                            <th>Ngày tạo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($sliders)
                        @foreach($sliders as $slide)
                        <tr data-slide-id="{{ $slide['slide_id'] }}">
                            <td style="vertical-align:middle">
                                <input type="checkbox" class="icheck check-slider" value="{{ $slide['slide_id'] }}" />
                            </td>
                            <td style="vertical-align:middle">{{ array_search($slide, $sliders) + 1 }}</td>
                            <td style="vertical-align:middle">
                                @if($slide['slide_image_low'])
                                <img width="75px" height="37.5px" src="{{ url($slide['slide_image_low']) }}"  />
                                @else
                                <img width="75px" height="37.5px" src="{{ url('public/images/no-image-available.jpg') }}"  />
                                @endif
                                {{ $slide['slide_title'] }}
                            </td>
                            <td style="vertical-align:middle;width:220px;text-align:center">
                                <input type="checkbox" class="bootstrap-toggle change-status-slide" data-slide-id="{{ $slide['slide_id'] }}" <?= $slide['slide_status']==1?'checked':''?> />
                                <a href="{{ route('admin-slider-edit', array('id' => $slide['slide_id'])) }}" class="btn btn-default btn-sm no-radius">
                                    <i class="fa fa-fw fa-pencil"></i> Sửa
                                </a>
                                <a data-slide-id="{{ $slide['slide_id'] }}" class="btn btn-warning btn-sm no-radius btn-delete-slide">
                                    <i class="fa fa-fw fa-trash"></i> Xoá
                                </a>
                            </td>
                            <td style="vertical-align:middle">{{ change_vn_date(strtotime($slide['created_at'])) }}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('admin.slider.tmpl-javascript.slide-tmpl')
@endsection