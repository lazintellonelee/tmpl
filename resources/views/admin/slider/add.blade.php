@extends('layouts.layout-admin')

@section('javascripts')
<script src="{{ url('public/plugins/typeahead/typeahead.bundle.js') }}"></script>
<script src="{{ url('public/plugins/cropper-master/dist/cropper.min.js') }}"></script>
<script src="{{ url('public/javascripts/admin/slider/add-script.js') }}"></script>
@endsection

@section('stylesheets')
<link href="{{ url('public/plugins/cropper-master/dist/cropper.min.css') }}" rel="stylesheet" />
<style>
    .tt-menu {background-color: #fff;width: 100% !important;}
    .tt-menu .tt-suggestion {padding: 10px 10px 5px 10px;}
    .tt-cursor {background-color: #31B0D5;color: #fff;}
</style>
@endsection

@section('content-header')
<h1>
    Trang tổng quát
    <small>Trình quản lý</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('admin-home') }}"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
    <li class="active"><a href="{{ route('admin-slider-home') }}">Danh sách slider</a></li>
    <li class="active">Thêm slide</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        @if(session()->get('success'))
        <div class="alert alert-success" role="alert">
            <i class="fa fa-fw fa-check"></i>{{ session()->get('success') }}
        </div>
        @endif
        
        @if(count($errors)>0)
        <div class="alert alert-warning" role="alert">
            @foreach($errors as $error)
            <i class="fa fa-fw fa-exclamation-triangle"></i> {{ $error }}
            @endforeach
        </div>
        @endif
        
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><a href="{{ route('admin-slider-home') }}"><i class="fa fa-fw fa-chevron-circle-left"></i></a> Thêm slide</h3>
            </div>
            
            <div class="box-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-2 control-label text-left">Tiêu đề Slide</label>
                        <div class="col-sm-10">
                            <input name="slide_title" type="text" class="form-control" placeholder="Tiêu đề slide ..." />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label text-left">Nội dung Slide</label>
                        <div class="col-sm-10">
                            <textarea name="slide_content" type="text" class="form-control" placeholder="Nội dung slide ..." style="resize:none" rows="3"></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label text-left">Liên kết Slide</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default dropdown-toggle no-radius" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Liên kết <span class="caret"></span></button>
                                    <ul class="dropdown-menu no-radius">
                                        <li class="active"><a href="#" class="slider-url-type" data-sync="false" data-value="" data-text="Liên kết">Liên kết</a></li>
                                        <li><a href="#" class="slider-url-type" data-sync="true" data-send="newsCategory" data-value="news_category_url" data-display="news_category_name" data-text="Danh mục tin tức">Danh mục tin tức</a></li>
                                        <li><a href="#" class="slider-url-type" data-sync="true" data-send="news" data-value="news_url" data-display="news_title" data-text="Tin tức">Tin tức</a></li>
                                    </ul>
                                </div><!-- /btn-group -->
                                <input type="hidden" name="slide_url" id="real_slide_url" />
                                <input type="text" id="slide_url" class="form-control" placeholder="Liên kết slide ...">
                            </div><!-- /input-group -->
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label text-left">Hình ảnh Slide</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default dropdown-toggle no-radius" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Hình ảnh <span class="caret"></span></button>
                                    <ul class="dropdown-menu no-radius">
                                        <li class="active"><a href="#" class="slider-type" data-type="image" data-text="Hình ảnh">Hình ảnh</a></li>
                                        <li><a href="#" class="slider-type" data-type="video" data-text="Video">Video</a></li>
                                    </ul>
                                </span>
                                <input type="hidden" name="slide_type" value="image" />
                                <input type="hidden" name="slide_image_base64" />
                                <input type="file" class="form-control" name="slide_image" id="slide_image" />
                                <span class="input-group-btn">
                                    <button class="btn btn-default no-radius" type="button" id="btn-crop-slide-image">
                                        <i class="ion ion-crop"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="col-sm-12" style="height:300px;background-color:#000;display:none;background-position:center;background-size:contain;background-repeat:no-repeat" id="div-slide-image-review"></div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <button class="btn btn-primary no-radius">Tạo slide</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('cropper-master.modal')
@endsection