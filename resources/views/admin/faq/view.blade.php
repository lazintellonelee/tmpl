@extends('layouts.layout-admin')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <a href="{{ route('admin-faq-home') }}"><i class="fa fa-fw fa-chevron-circle-left"></i></a> Thông tin chi tiết
                    <a href="{{ route('admin-faq-edit', array("id" => $faq['faq_id'])) }}">
                        <i class="fa fa-fw fa-edit"></i>
                    </a>
                </h3>
            </div>
            <div class="box-body">
                <div class="clearfix"></div>

                <div class="col-sm-12">
                    <table class="table table-responsive">
                        <tbody>
                            <tr>
                                <td style="vertical-align:middle"><b>Câu hỏi</b></td>
                                <td style="vertical-align:middle">{{ $faq['faq_question'] }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default" style="border-radius: 0px">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a>
                                        Trả lời
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    {!! $faq['faq_description'] !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

