@extends('layouts.layout-admin')

@section('javascripts')
<script src="{{ url('public/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ url('public/plugins/ckeditor/config.js') }}"></script>
<script src="{{ url('public/javascripts/admin/faq/add-script.js') }}"></script>
@endsection

@section('stylesheets')
<style type="text/css">
    .twitter-typeahead {width:100%;}
    .tt-menu {background-color: #fff;width: 100% !important;top: 27px !important;left:-7px !important}
    .tt-menu .tt-suggestion {padding: 5px 10px 5px 10px;}
    .tt-cursor {background-color: #31B0D5;color: #fff;}
</style>
@endsection

@section('content-header')
<h1>
    Trang tổng quát
    <small>Trình quản lý</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('admin-home') }}"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
    <li class="active"><a href="{{ route('admin-faq-home') }}">Danh sách câu hỏi</a></li>
    <li class="active">Thêm câu hỏi</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        @if(session()->get('success'))
        <div class="alert alert-success" role="alert">
            <i class="fa fa-fw fa-check"></i>{{ session()->get('success') }}
        </div>
        @endif
        
        @if(count($errors)>0)
        <div class="alert alert-warning" role="alert">
            @foreach($errors as $error)
            <i class="fa fa-fw fa-exclamation-triangle"></i> {{ $error }}
            @endforeach
        </div>
        @endif
        
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><a href="{{ route('admin-faq-home') }}"><i class="fa fa-fw fa-chevron-circle-left"></i></a> Thêm câu hỏi</h3>
            </div>
            
            <div class="box-body form-horizontal">
                <form class="form-horizontal" method="post" id="form-add-faq" enctype="multipart/form-data">
                    {{ csrf_field() }}
                </form>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Câu hỏi</label>
                    <div class="col-sm-10">
                        <textarea form="form-add-faq" name="faq_question" type="text" id="faq_question" class="form-control" placeholder="Câu hỏi ..."></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Trả lời</label>
                    <div class="col-sm-10">
                        <textarea form="form-add-faq" name="faq_answer" type="text" id="faq_answer" class="form-control" placeholder="Trả lời ..." ></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button form="form-add-faq" class="btn btn-primary no-radius btn_add_faq" id="btn-submit-form-add-faq">Tạo câu hỏi</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
