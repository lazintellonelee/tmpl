@extends('layouts.layout-admin')

@section('javascripts')
<script src="{{ url('public/plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ url('public/plugins/chart-js-master/Chart.js') }}"></script>
<script src="{{ url('public/javascripts/admin/news-category/home-script.js') }}"></script>
<script src="{{ url('public/javascripts/admin/news-category/chart-script.js') }}"></script>
@endsection

@section('stylesheets')
<link href="{{ url('public/themes/admin-lte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet"  />
<link href="{{ url('public/themes/admin-lte/plugins/iCheck/all.css') }}" rel="stylesheet" />
<link href="{{ url('public/plugins/bootstrap-toggle-master/css/bootstrap-toggle.min.css') }}" rel="stylesheet" />
@endsection

@section('content-header')
<h1>
    Trang tổng quát
    <small>Trình quản lý</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('admin-home') }}"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
    <li class="active"><a href="{{ route('admin-news-category-home') }}">Danh mục tin tức</a></li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        @if(session()->get('success'))
        <div class="alert alert-success" role="alert">
            <i class="fa fa-fw fa-check"></i>{{ session()->get('success') }}
        </div>
        @endif
        
        @if($newsCategories)
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><a href="#"><i class="fa fa-fw fa-dashboard"></i></a> Thông tin tổng quan</h3>
            </div>
            
            <div class="box-body">
                <div class="col-sm-3">
                    <div class="small-box bg-green" style="height:175px">
                        <div class="inner" style="height:148px">
                            <h3>{{ count($newsCategories) }}</h3>
                            <p>Danh mục tin tức</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-list"></i>
                        </div>
                        <a href="#" class="small-box-footer">&nbsp</a>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <canvas id="chart-bar-news-in-category" height="50"></canvas>
                    <h4>
                        Biểu đồ số lượng tin tức trong mỗi danh mục gốc.
                    </h4>
                </div>
            </div>
        </div>
        @endif
        
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><a href="{{ route('admin-news-category-add') }}"><i class="fa fa-fw fa-plus-square"></i></a> Danh mục tin tức</h3>
            </div>
            
            <div class="box-body">
                <table class="table table-bordered" id="table_news_categories">
                    <thead>
                        <tr>
                            <th style="width:30px">
                                <input type="checkbox" class="icheck" id="check-all-news-category" />
                            </th>
                            <th style="width:30px">STT</th>
                            <th>Tên Danh Mục</th>
                            <th>Tác Vụ</th>
                            <th>Ngày Tạo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($newsCategories)
                        @foreach($newsCategories as $newsCategory)
                        <tr data-news-category-id="{{ $newsCategory['news_category_id'] }}">
                            <td style="vertical-align:middle">
                                <input type="checkbox" class="icheck check-news-category" value="{{ $newsCategory['news_category_id'] }}" />
                            </td>
                            <td style="vertical-align:middle">{{ array_search($newsCategory, $newsCategories) + 1 }}</td>
                            <td style="vertical-align:middle">{{ $newsCategory['news_category_name'] }}</td>
                            <td style="vertical-align:middle;width:210px;text-align:center">
                                <input type="checkbox" class="bootstrap-toggle" data-news-category-id="{{ $newsCategory['news_category_id'] }}" <?= $newsCategory['news_category_status']==1?'checked':'' ?> />
                                <a href="{{ route('admin-news-category-edit', array('id' => $newsCategory['news_category_id'])) }}" class="btn btn-default btn-sm no-radius">
                                    <i class="fa fa-fw fa-pencil"></i> Sửa
                                </a>
                                <a data-news-category-id="{{ $newsCategory['news_category_id'] }}" class="btn btn-warning btn-sm no-radius btn-delete-news-category">
                                    <i class="fa fa-fw fa-trash"></i> Xoá
                                </a>
                            </td>
                            <td style="vertical-align:middle; width:150px">
                                {{ date('H:i d/m/Y', strtotime($newsCategory['created_at'])) }}
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection