@extends('layouts.layout-admin')

@section('javascripts')
<script src="{{ url('public/plugins/cropper-master/dist/cropper.min.js') }}"></script>
<script src="{{ url('public/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ url('public/javascripts/admin/news-category/edit-script.js') }}"></script>
@endsection

@section('stylesheets')
<link href="{{ url('public/plugins/cropper-master/dist/cropper.min.css') }}" rel="stylesheet" />
<link href="{{ url('public/plugins/select2/select2.min.css') }}" rel="stylesheet" />
<style type="text/css">
    strong.select2-results__group {margin-left: 7px}
    ul.select2-results__options--nested li {padding-left: 50px !important}
</style>
@endsection

@section('content-header')
<h1>
    Trang tổng quát
    <small>Trình quản lý</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('admin-home') }}"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
    <li class="active"><a href="{{ route('admin-news-home') }}">Danh mục tin tức</a></li>
    <li class="active">Sửa danh mục tin tức</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        @if(session()->get('success'))
        <div class="alert alert-success" role="alert">
            <i class="fa fa-fw fa-check"></i>{{ session()->get('success') }}
        </div>
        @endif
        
        @if(count($errors)>0)
        <div class="alert alert-warning" role="alert">
            @foreach($errors as $error)
            <i class="fa fa-fw fa-exclamation-triangle"></i> {{ $error }}
            @endforeach
        </div>
        @endif
        
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><a href="{{ route('admin-news-category-home') }}"><i class="fa fa-fw fa-chevron-circle-left"></i></a> Sửa danh mục tin tức</h3>
            </div>
            
            <div class="box-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label text-left">Danh mục gốc</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="news_category_parent_id" id="news_catgory_parent_id" data-placeholder="Danh mục gốc...">
                                    <option></option>
                                    <option value="0">Danh mục gốc</option>
                                    {!! \App\Models\Admin\News\NewsCategory::GetNewsCategoriesOptionHTML(0, $newsCategory['news_category_parent_id'], true) !!}
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label text-left">Tên danh mục</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="news_category_name" required placeholder="Tên danh mục..." value="{{ $newsCategory['news_category_name'] }}" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label text-left">Ảnh danh mục</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="hidden" name="news_category_avatar_base64" />
                                    <input type="file" class="form-control" name="news_category_avatar" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default no-radius" type="button" id="btn-crop-news-avatar">
                                            <i class="ion ion-crop"></i>
                                        </button>
                                    </span>
                                </div><!-- /input-group -->
                            </div>
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="col-sm-12" 
                                     style="
                                        height:300px;
                                        background-color:#000;
                                        display:{{ $newsCategory['news_category_avatar_high']!=''?'block':'none' }};
                                        background-image:{{ $newsCategory['news_category_avatar_high']!=''?'url("'.url($newsCategory['news_category_avatar_high']).'")':'none' }};
                                        background-repeat:no-repeat;
                                        background-position:center;
                                        background-size:contain" 
                                        id="div-news-category-avatar-review">     
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-12 text-right">
                                <a href="{{ route('admin-news-category-home') }}" class="btn btn-default no-radius">Huỷ</a>
                                <button class="btn btn-primary no-radius">Sửa danh mục</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('cropper-master.modal')
@endsection