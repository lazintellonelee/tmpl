@extends('layouts.layout-admin')

@section('javascripts')
<script src="{{ url('public/plugins/cropper-master/dist/cropper.min.js') }}"></script>
<script src="{{ url('public/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ url('public/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ url('public/plugins/ckeditor/config.js') }}"></script>
<script src="{{ url('public/javascripts/admin/article/edit-script.js') }}"></script>
@endsection

@section('stylesheets')
<link href="{{ url('public/plugins/cropper-master/dist/cropper.min.css') }}" rel="stylesheet" />
<link href="{{ url('public/plugins/select2/select2.min.css') }}" rel="stylesheet" />
<style type="text/css">
    strong.select2-results__group {margin-left: 7px}
    ul.select2-results__options--nested li {padding-left: 50px !important}
    .bootstrap-tagsinput {width: 100%;border-radius: 0px !important;}
    .tt-menu {background-color: #fff;width: 500px !important;top: 27px !important;left:-7px !important}
    .tt-menu .tt-suggestion {padding: 5px 10px 5px 10px;}
    .tt-cursor {background-color: #31B0D5;color: #fff;}
</style>
@endsection

@section('content-header')
<h1>
    Trang tổng quát
    <small>Trình quản lý</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('admin-home') }}"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
    <li class="active"><a href="{{ route('admin-article-home') }}">Danh sách bài viết</a></li>
    <li class="active">Thêm bài viết</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        @if(session()->get('success'))
        <div class="alert alert-success" role="alert">
            <i class="fa fa-fw fa-check"></i>{{ session()->get('success') }}
        </div>
        @endif
        
        @if(count($errors)>0)
        <div class="alert alert-warning" role="alert">
            @foreach($errors as $error)
            <i class="fa fa-fw fa-exclamation-triangle"></i> {{ $error }}
            @endforeach
        </div>
        @endif
        
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><a href="{{ route('admin-article-home') }}"><i class="fa fa-fw fa-chevron-circle-left"></i></a> Cập nhật bài viết</h3>
            </div>
            
            <div class="box-body form-horizontal">
                <form class="form-horizontal" method="post" id="form-edit-article" enctype="multipart/form-data">
                    {{ csrf_field() }}
                </form>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Tiêu đề bài viết</label>
                    <div class="col-sm-10">
                        <input form="form-edit-article" 
                               name="article_title" 
                               type="text" 
                               class="form-control" 
                               placeholder="Tiêu đề bài viết ..."
                               value="{{ $article['article_title'] }}" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Mô tả bài viết</label>
                    <div class="col-sm-10">
                        <textarea form="form-edit-article" name="article_description" class="form-control" rows="3" style="resize:none" placeholder="Mô tả bài viết ...">{{ $article['article_description'] }}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Nội dung bài viết</label>
                    <div class="col-sm-10">
                        <textarea form="form-edit-article" name="article_content" class="form-control" rows="3" placeholder="Nội dung bài viết ..." id="article_content">{{ $article['article_content'] }}</textarea>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Tác giả bài viết</label>
                    <div class="col-sm-10">
                        <input form="form-edit-article" 
                               name="article_author" 
                               type="text" 
                               class="form-control" 
                               placeholder="Tác giả bài viết ..."
                               value="{{ $article['article_author'] }}" />
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Ảnh đại diện</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input form="form-edit-article" type="hidden" name="article_thumbnail_base64" />
                            <input form="form-edit-article" 
                                   type="<?= $article['article_thumbnail_url']?'text':'file'?>" 
                                   class="form-control" 
                                   name="<?= $article['article_thumbnail_url']?'article_thumbnail_url':'article_thumbnail'?>" 
                                   <?= $article['article_thumbnail_url']?'value="' . $article['article_thumbnail_url'] . '"':''?>
                                   id="article_thumbnail" 
                                   placeholder="http:// liên kết ngoài ..." />
                            <span class="input-group-btn <?= $article['article_thumbnail_image']?'':'hidden'?>">
                                <button class="btn btn-default no-radius" type="button" id="btn-crop-article-thumbnail">
                                    <i class="ion ion-crop"></i>
                                </button>
                            </span>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle no-radius" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                    @if($article['article_thumbnail_url'])
                                    Liên kết ngoài
                                    @endif
                                    @if($article['article_thumbnail_image'])
                                    Tải lên
                                    @endif
                                    &nbsp;<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right no-radius">
                                    <li <?= $article['article_thumbnail_url']?'class="active"':''?>><a class="change-formality-article-thumbnail" data-formality="server" href="#">Liên kết ngoài</a></li>
                                    <li <?= $article['article_thumbnail_image']?'class="active"':''?>><a class="change-formality-article-thumbnail"  data-formality="url" href="#">Tải lên</a></li>
                                </ul>
                            </div><!-- /btn-group -->
                        </div><!-- /input-group -->
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="col-sm-12" style="
                            height:300px;
                            background-color:#000;
                            display:none;
                            background-position:center;
                            background-size:contain;
                            background-repeat:no-repeat;
                            <?php if($article['article_thumbnail_url']):?>
                            background-image: url('<?= $article['article_thumbnail_url']?>');
                            display: block;
                            <?php endif;?>
                            <?php if($article['article_thumbnail_image']):?>
                            background-image: url('<?= url($article['article_thumbnail_image'])?>');
                            display: block;
                            <?php endif;?>
                            " 
                            id="div-article-thumbnail-review"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Tên nguồn</label>
                    <div class="col-sm-4">
                        <input form="form-edit-article" 
                               name="article_source_name" 
                               type="text" 
                               class="form-control" 
                               placeholder="Tên nguồn bài viết ..."
                               value="{{ $article['article_source_name'] }}"/>
                    </div>
                    <label class="col-sm-2 control-label text-left">Link nguồn</label>
                    <div class="col-sm-4">
                        <input form="form-edit-article" 
                               name="article_source_url" 
                               type="text" 
                               class="form-control" 
                               placeholder="http:// Link nguồn bài viết ..."
                               value="{{ $article['article_source_url'] }}" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button form="form-edit-article" class="btn btn-primary no-radius" id="btn-submit-form-edit-article">Cập nhật bài viết</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('cropper-master.modal')
@endsection
