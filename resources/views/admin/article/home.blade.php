@extends('layouts.layout-admin')

@section('javascripts')
<script src="{{ url('public/plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ url('public/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ url('public/plugins/chart-js-master/Chart.js') }}"></script>
<script src="{{ url('public/javascripts/admin/article/home-script.js') }}"></script>
<script src="{{ url('public/javascripts/admin/article/chart-script.js') }}"></script>
@endsection

@section('stylesheets')
<link href="{{ url('public/plugins/bootstrap-toggle-master/css/bootstrap-toggle.min.css') }}" rel="stylesheet" />
<link href="{{ url('public/themes/admin-lte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet"  />
<link href="{{ url('public/themes/admin-lte/plugins/iCheck/all.css') }}" rel="stylesheet" />
<link href="{{ url('public/plugins/select2/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content-header')
<h1>
    Trang tổng quát
    <small>Trình quản lý</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('admin-home') }}"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
    <li class="active"><a href="{{ route('admin-article-home') }}">Danh sách bài viết</a></li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        @if(session()->get('success'))
        <div class="alert alert-success" role="alert">
            <i class="fa fa-fw fa-check"></i>{{ session()->get('success') }}
        </div>
        @endif
        
        @if($article)
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <a href="#">
                        <i class="fa fa-fw fa-dashboard"></i>
                    </a> Thông tin tổng quan
                </h3>
            </div>
            <div class="box-body">
                <div class="col-sm-3">
                    <div class="small-box bg-green" style="height:175px">
                        <div class="inner" style="height:148px">
                            <h3>{{ count($article) }}</h3>
                            <p>Bài viết</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-fw fa-file-text-o"></i>
                        </div>
                        <a href="#" class="small-box-footer">&nbsp</a>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <canvas id="chart-bar-article-view" height="50"></canvas>
                    <h4>
                        Biểu đồ số lượng bài viết trong tháng&nbsp;
                        <select class="form-control" style="width:70px;display:inline-block" id="select-chart-article-views-date">
                            @for($i=1;$i<=12;$i++)
                            <option <?= $i==date('m')?'selected':'' ?> value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                        &nbsp;năm&nbsp;
                        <select class="form-control" style="width:80px;display:inline-block" id="select-chart-article-views-year">
                           @for($i=date('Y')-3;$i<=date('Y')+3;$i++)
                           <option <?= $i==date('Y')?'selected':'' ?> value='{{ $i }}'>{{ $i }}</option>
                           @endfor
                        </select>
                        &nbsp;- <span id="total-for-chart"></span>
                    </h4>
                </div>
            </div>
        </div>
        @endif
        
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <a href="{{ route('admin-article-add') }}">
                        <i class="fa fa-fw fa-plus-square"></i>
                    </a> Danh sách bài viết
                </h3>
            </div>
            
            <div class="box-body">
                <table class="table table-bordered" id="table_article">
                    <thead>
                        <tr>
                            <th style="width:30px">
                                <input type="checkbox" class="icheck" id="check-all-article" />
                            </th>
                            <th style="width:30px">Mã</th>
                            <th>Tên</th>
                            <th>Tác Vụ</th>
                            <th>Ngày Tạo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($article) > 0)
                        @foreach($article as $art)
                        <tr data-article-id="{{ $art['article_id'] }}">
                            <td style="width:30px;vertical-align:middle">
                                <input type="checkbox" class="icheck check-article" value="{{ $art['article_id'] }}" />
                            </td>
                            <td style="width:30px;vertical-align:middle">{{ $art['article_id'] }}</td>
                            <td style="position:relative">
                                @if($art['article_thumbnail_url'])
                                <img width="75px" height="37.5px" src="{{ $art['article_thumbnail_url'] }}"  />
                                @elseif($art['article_thumbnail_image'])
                                <img width="75px" height="37.5px" src="{{ url($art['article_thumbnail_image']) }}"  />
                                @else
                                <img width="75px" height="37.5px" src="{{ url('public/images/no-image-available.jpg') }}"  />
                                @endif
                                <a href="#" class="<?= $art['article_verified']==1?'bg-green':'bg-info'?> change-verified" data-article-id="{{ $art['article_id'] }}" data-article-verified="{{ $art['article_verified'] }}" style="position:absolute;left:65px">
                                    <i class="fa fa-fw fa-<?= $art['article_verified']==1?'check':'times'?>"></i>
                                </a>
                                &nbsp;&nbsp;
                                <a href="{{ route('admin-article-view', array('id' => $art['article_id'])) }}">{{ $art['article_title'] }}</a>
                            </td>
                            <td style="width:220px;text-align:center;vertical-align:middle">
                                <input type="checkbox" class="bootstrap-toggle change-status-article" data-article-id="{{ $art['article_id'] }}" <?= $art['article_status']==1?'checked':''?> />
                                <a href="{{ route('admin-article-edit', array('id' => $art['article_id'])) }}" class="btn btn-default btn-sm no-radius">
                                    <i class="fa fa-fw fa-pencil"></i> Sửa
                                </a>
                                <a data-article-id="{{ $art['article_id'] }}" class="btn btn-warning btn-sm no-radius btn-delete-article">
                                    <i class="fa fa-fw fa-trash"></i> Xoá
                                </a>
                            </td>
                            <td style="vertical-align:middle">{{ change_vn_date(strtotime($art['created_at'])) }}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection