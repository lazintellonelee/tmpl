@extends('layouts.layout-admin')

@section('javascripts')
<script src="{{ url('public/plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ url('public/plugins/chart-js-master/Chart.js') }}"></script>
<script src="{{ url('public/javascripts/admin/emailpromotion/home-script.js') }}"></script>
<script src="{{ url('public/javascripts/admin/emailpromotion/chart-script.js') }}"></script>
@endsection

@section('stylesheets')
<link href="{{ url('public/plugins/bootstrap-toggle-master/css/bootstrap-toggle.min.css') }}" rel="stylesheet" />
<link href="{{ url('public/themes/admin-lte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet"  />
<link href="{{ url('public/themes/admin-lte/plugins/iCheck/all.css') }}" rel="stylesheet" />
@endsection

@section('content-header')
<h1>
    Trang tổng quát
    <small>Trình quản lý</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('admin-home') }}"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
    <li class="active"><a href="{{ route('admin-emailpromotion-home') }}">Danh sách email khuyến mãi</a></li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        @if(session()->get('success'))
        <div class="alert alert-success" role="alert">
            <i class="fa fa-fw fa-check"></i>{{ session()->get('success') }}
        </div>
        @endif
        
        @if($emailpromotion)
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <a href="#">
                        <i class="fa fa-fw fa-dashboard"></i>
                    </a> Thông tin tổng quan
                </h3>
            </div>
            <div class="box-body">
                <div class="col-sm-3">
                    <div class="small-box bg-green" style="height:175px">
                        <div class="inner" style="height:148px">
                            <h3>{{ count($emailpromotion) }}</h3>
                            <p>Đối tác</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-fw fa-envelope-o"></i>
                        </div>
                        <a href="#" class="small-box-footer">&nbsp</a>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <canvas id="chart-bar-emailpromotion-view" height="50"></canvas>
                    <h4>
                        Biểu đồ số lượng email khuyến mãi trong tháng&nbsp;
                        <select class="form-control" style="width:70px;display:inline-block" id="select-chart-emailpromotion-views-date">
                            @for($i=1;$i<=12;$i++)
                            <option <?= $i==date('m')?'selected':'' ?> value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                        &nbsp;- <span id="total-for-chart"></span>
                    </h4>
                </div>
            </div>
        </div>
        @endif
        
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Danh sách email khuyến mãi
                </h3>
            </div>
            
            <div class="box-body">
                <table class="table table-bordered" id="table_emailpromotion">
                    <thead>
                        <tr>
                            <th style="width:30px">
                                <input type="checkbox" class="icheck" id="check-all-emailpromotion" />
                            </th>
                            <th style="width:30px">STT</th>
                            <th>Tên</th>
                            <th>Email</th>
                            <th>Giới tính</th>
                            <th>Tác Vụ</th>
                            <th>Ngày Tạo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($emailpromotion)
                        @foreach($emailpromotion as $e)
                        <tr data-emailpromotion-id="{{ $e['emailpromotion_id'] }}">
                            <td style="width:30px;vertical-align:middle">
                                <input type="checkbox" class="icheck check-emailpromotion" value="{{ $e['emailpromotion_id'] }}" />
                            </td>
                            <td style="width:30px;vertical-align:middle"></td>
                            <td style="position:relative">
                                <a href="{{ route('admin-emailpromotion-view', array('id' => $e['emailpromotion_id'])) }}">{{ $e['emailpromotion_name'] }}</a>
                            </td>
                            <td style="position:relative">
                                {{ $e['emailpromotion_email'] }}
                            </td>
                            <td style="position:relative">
                                {{ $e['emailpromotion_gender'] }}
                            </td>
                            <td style="width:220px;text-align:center;vertical-align:middle">
                                <a data-emailpromotion-id="{{ $e['emailpromotion_id'] }}" class="btn btn-warning btn-sm no-radius btn-delete-emailpromotion">
                                    <i class="fa fa-fw fa-trash"></i> Xoá
                                </a>
                            </td>
                            <td style="vertical-align:middle">{{ change_vn_date(strtotime($e['created_at'])) }}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection