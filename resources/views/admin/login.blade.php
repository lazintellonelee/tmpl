<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="base-url" content="{{ url()}}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>AdminLTE 2 | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?= url()?>/public/themes/admin-lte/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= url()?>/public/plugins/font-awesome-4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?= url()?>/public/plugins/ionicons-2.0.1/css/ionicons.min.css">
        <!-- custom stylesheets -->
        <link rel="stylesheet" href="<?= url()?>/public/themes/admin-lte/plugins/iCheck/all.css" />
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= url()?>/public/themes/admin-lte/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?= url()?>/public/themes/admin-lte/dist/css/skins/_all-skins.min.css">
        <!-- Animate -->
        <link rel="stylesheet" href="<?= url()?>/public/stylesheets/animate.css" />
        <!-- iCheck -->
        <link rel="stylesheet" href="<?= url()?>/public/themes/admin-lte/plugins/iCheck/flat/blue.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?= url()?>/public/themes/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="<?= url()?>/public/themes/admin-lte/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="<?= url()?>/public/themes/admin-lte/plugins/daterangepicker/daterangepicker-bs3.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="<?= url()?>/public/themes/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- jQuery 2.1.4 -->
        <script src="<?= url()?>/public/themes/admin-lte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    </head>
    <body>
        <div class="modal show" tabindex="-1" role="dialog" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">
                            <i class="fa fa-fw fa-lock"></i>
                            Đăng Nhập Trang Quản Trị Template.
                        </h4>
                    </div>
                    <div class="modal-body">
                        @if(count($errors) > 0)
                        <div class="alert alert-warning alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            @foreach($errors as $error)
                            <strong><i class="fa fa-exclamation-triangle"></i></strong> {{ $error }}
                            @endforeach
                        </div>
                        @endif
                        
                        <form class="form-horizontal" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-left">Email hoặc Tên</label>
                                <div class="col-sm-9">
                                    <input type="text" 
                                           class="form-control" 
                                           name="user_name" 
                                           required 
                                           placeholder="Email hoặc tên đăng nhập ..." 
                                           autocomplete="off"
                                           value="{{ $data['user_name'] or '' }}" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-left">Mật khẩu</label>
                                <div class="col-sm-9">
                                    <input type="password" 
                                           class="form-control" 
                                           name="user_password" 
                                           required 
                                           placeholder="Mật khẩu ..." 
                                           autocomplete="off" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-left">&nbsp;</label>
                                <div class="col-sm-9">
                                    <input type="checkbox" class="icheck" name="remember_me" <?= isset($data['remember_me'])?'checked':'' ?> />
                                    <span style="position: absolute;top: 2px;left: 41px;">Ghi nhớ đăng nhập.</span>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <button class="btn btn-primary no-radius" style="padding-left: 50px;padding-right: 50px">Đăng Nhập</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="#">Quên mật khẩu?</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
        <!-- Bootstrap 3.3.5 -->
        <script src="<?= url()?>/public/themes/admin-lte/bootstrap/js/bootstrap.min.js"></script>
        <!-- Slimscroll -->
        <script src="<?= url()?>/public/themes/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?= url()?>/public/themes/admin-lte/plugins/fastclick/fastclick.min.js"></script>
        <!-- Noty -->
        <script src="<?= url()?>/public/plugins/noty/js/noty/packaged/jquery.noty.packaged.min.js"></script>
        <!-- Noty Functions -->
        <script src="<?= url()?>/public/javascripts/noty-functions.js"></script>
        <!-- Function -->
        <script src="<?= url()?>/public/javascripts/functions.js"></script>
        <!-- Custom javascripts -->
        <script src="<?= url()?>/public/themes/admin-lte/plugins/iCheck/icheck.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?= url()?>/public/themes/admin-lte/dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?= url()?>/public/themes/admin-lte/dist/js/demo.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function() {
                $('input.icheck').iCheck({
                    checkboxClass   : 'icheckbox_minimal-blue',
                    radioClass      : 'iradio_minimal-blue'
                });
            });
        </script>
    </body>
</html>