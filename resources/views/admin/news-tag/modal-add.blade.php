<div class="modal fade" role="dialog" id="modal-add-news-tag">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Thêm tag tin tức</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-add-news-tag">
                    <div class="form-group">
                        <label class="col-sm-2 control-label text-left">Tên tags</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="news_tag_name" required placeholder="Tên tags tin tức..." />
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Huỷ</button>
                <button type="button" class="btn btn-primary" id="btn-add-news-tag">Thêm tag tin tức</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->