<script type="text/x-tmpl" id="tmpl-modal-body-edit-news-tag">
    <form class="form-horizontal" id="form-edit-news-tag">
        <input type="hidden" name="news_tag_id" value="{%= o.news_tag_id%}" />
        <div class="form-group">
            <label class="col-sm-2 control-label text-left">Tên tags</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="news_tag_name" required placeholder="Tên tags tin tức..." value="{%= o.news_tag_name%}" />
            </div>
        </div>
    </form>
</script>

<script type="text/x-tmpl" id="tmpl-tr-edit-news-tag">
    <td style="vertical-align:middle;width:30px">
        <input type="checkbox" class="icheck check-news-tag" value="{%= o.news_tag_id%}" />
    </td>
    <td style="width:30px;vertical-align:middle">{%= o.stt%}</td>
    <td style="vertical-align:middle">
        {%= o.news_tag_name%}
    </td>
    <td style="vertical-align:middle;width:125px;text-align:center">
        <a href="#" class="btn btn-default btn-sm no-radius btn-edit-news-tag" data-news-tag-id="{%= o.news_tag_id%}">
            <i class="fa fa-fw fa-pencil"></i> Sửa
        </a>
        <a href="#" class="btn btn-warning btn-sm no-radius btn-delete-news-tag" data-news-tag-id="{%= o.news_tag_id%}">
            <i class="fa fa-fw fa-trash"></i> Xoá
        </a>
    </td>
    <td style="vertical-align:middle;width:150px">{%= o.created_at%}</td>
</script>