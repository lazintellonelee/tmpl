<script type="text/x-tmpl" id="tmpl-add-news-tag">
    <tr>
        <td style="width:30px">
            <input type="checkbox" class="icheck" id="check-news-tag" />
        </td>
        <td style="width:30px">STT</td>
        <td>Tên thẻ tag</td>
        <td>Tác Vụ</td>
        <td>Ngày Tạo</td>
    </tr>
</script>

<script type="text/x-tmpl" id="tmpl-add-news-tag-td1">
    <input type="checkbox" class="icheck check-news-tag" value="{%= o.news_tag_id%}" />
</script>

<script type="text/x-tmpl" id="tmpl-add-news-tag-td2">
    {%= o.stt%}
</script>

<script type="text/x-tmpl" id="tmpl-add-news-tag-td3">
    {%= o.news_tag_name%}
</script>

<script type="text/x-tmpl" id="tmpl-add-news-tag-td4">
    <a href="#" class="btn btn-default btn-sm no-radius btn-edit-news-tag" data-news-tag-id="{%= o.news_tag_id%}">
        <i class="fa fa-fw fa-pencil"></i> Sửa
    </a>
    <a href="#" class="btn btn-warning btn-sm no-radius btn-delete-news-tag" data-news-tag-id="{%= o.news_tag_id%}">
        <i class="fa fa-fw fa-trash"></i> Xoá
    </a>
</script>

<script type="text/x-tmpl" id="tmpl-add-news-tag-td5">
    {%= o.created_at%}
</script>