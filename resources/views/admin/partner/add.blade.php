@extends('layouts.layout-admin')

@section('javascripts')
<script src="{{ url('public/plugins/cropper-master/dist/cropper.min.js') }}"></script>
<script src="{{ url('public/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ url('public/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ url('public/plugins/ckeditor/config.js') }}"></script>
<!--<script src="{{ url('public/plugins/tinymce/js/tinymce/tinymce.min.js') }}"></script>-->
<script src="{{ url('public/plugins/typeahead/typeahead.bundle.js') }}"></script>
<script src="{{ url('public/plugins/angular/angular.min.js') }}"></script>
<script src="{{ url('public/javascripts/admin/partner/add-script.js') }}"></script>
@endsection

@section('stylesheets')
<link href="{{ url('public/plugins/cropper-master/dist/cropper.min.css') }}" rel="stylesheet" />
<link href="{{ url('public/plugins/select2/select2.min.css') }}" rel="stylesheet" />
<link href="{{ url('public/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<style type="text/css">
    .twitter-typeahead {width:100%;}
    .tt-menu {background-color: #fff;width: 100% !important;top: 27px !important;left:-7px !important}
    .tt-menu .tt-suggestion {padding: 5px 10px 5px 10px;}
    .tt-cursor {background-color: #31B0D5;color: #fff;}
</style>
@endsection

@section('content-header')
<h1>
    Trang tổng quát
    <small>Trình quản lý</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('admin-home') }}"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
    <li class="active"><a href="{{ route('admin-partner-home') }}">Danh sách đối tác</a></li>
    <li class="active">Thêm đối tác</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        @if(session()->get('success'))
        <div class="alert alert-success" role="alert">
            <i class="fa fa-fw fa-check"></i>{{ session()->get('success') }}
        </div>
        @endif
        
        @if(count($errors)>0)
        <div class="alert alert-warning" role="alert">
            @foreach($errors as $error)
            <i class="fa fa-fw fa-exclamation-triangle"></i> {{ $error }}
            @endforeach
        </div>
        @endif
        
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><a href="{{ route('admin-partner-home') }}"><i class="fa fa-fw fa-chevron-circle-left"></i></a> Thêm đối tác</h3>
            </div>
            
            <div class="box-body form-horizontal">
                <form class="form-horizontal" method="post" id="form-add-partner" enctype="multipart/form-data">
                    {{ csrf_field() }}
                </form>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Tên đối tác</label>
                    <div class="col-sm-10">
                        <input form="form-add-partner" name="partner_name" type="text" class="form-control" placeholder="Tên đối tác ..." />
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Email</label>
                    <div class="col-sm-10">
                        <input form="form-add-partner" name="partner_email" type="text" class="form-control" placeholder="Email đối tác ..." />
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Điện thoại</label>
                    <div class="col-sm-10">
                        <input form="form-add-partner" name="partner_phone" type="text" class="form-control" placeholder="Số điện thoại đối tác ..." />
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Địa chỉ</label>
                    <div class="col-sm-10">
                        <input form="form-add-partner" name="partner_address" type="text" class="form-control" placeholder="Địa chỉ đối tác ..." />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Mô tả đối tác</label>
                    <div class="col-sm-10">
                        <textarea id="partner_description" form="form-add-partner" name="partner_description" class="form-control" rows="3" style="resize:none" placeholder="Mô tả đối tác ..."></textarea>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Trang chủ</label>
                    <div class="col-sm-10">
                        <input form="form-add-partner" name="partner_homepage" type="text" class="form-control" placeholder="Trang chủ của đối tác ..." />
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Link khác</label>
                    <div class="col-sm-10">
                        <input form="form-add-partner" name="partner_link" type="text" class="form-control" placeholder="Đường link khác về đối tác ..." />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">Ảnh đại diện</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input form="form-add-partner" type="hidden" name="partner_thumbnail_base64" />
                            <input form="form-add-partner" type="file" class="form-control" name="partner_thumbnail_image" id="partner_thumbnail_image" placeholder="http:// liên kết ngoài ..." />
                            <span class="input-group-btn">
                                <button class="btn btn-default no-radius" type="button" id="btn-crop-partner-thumbnail">
                                    <i class="ion ion-crop"></i>
                                </button>
                            </span>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle no-radius" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tải lên <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right no-radius">
                                    <li><a class="change-formality-partner-thumbnail" data-formality="server" href="#">Liên kết ngoài</a></li>
                                    <li class="active"><a class="change-formality-partner-thumbnail"  data-formality="url" href="#">Tải lên</a></li>
                                </ul>
                            </div><!-- /btn-group -->
                        </div><!-- /input-group -->
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="col-sm-12" style="height:300px;background-color:#000;display:none;background-position:center;background-size:contain;background-repeat:no-repeat" id="div-partner-thumbnail-review"></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button form="form-add-partner" class="btn btn-primary no-radius" id="btn-submit-form-add-partner">Tạo đối tác</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('cropper-master.modal')
@endsection
