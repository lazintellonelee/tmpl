@extends('layouts.layout-admin')

@section('javascripts')
<script src="{{ url('public/plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('public/themes/admin-lte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ url('public/plugins/chart-js-master/Chart.js') }}"></script>
<script src="{{ url('public/javascripts/admin/partner/home-script.js') }}"></script>
<script src="{{ url('public/javascripts/admin/partner/chart-script.js') }}"></script>
@endsection

@section('stylesheets')
<link href="{{ url('public/plugins/bootstrap-toggle-master/css/bootstrap-toggle.min.css') }}" rel="stylesheet" />
<link href="{{ url('public/themes/admin-lte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet"  />
<link href="{{ url('public/themes/admin-lte/plugins/iCheck/all.css') }}" rel="stylesheet" />
@endsection

@section('content-header')
<h1>
    Trang tổng quát
    <small>Trình quản lý</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('admin-home') }}"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
    <li class="active"><a href="{{ route('admin-partner-home') }}">Danh sách đối tác</a></li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        @if(session()->get('success'))
        <div class="alert alert-success" role="alert">
            <i class="fa fa-fw fa-check"></i>{{ session()->get('success') }}
        </div>
        @endif
        
        @if($partner)
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <a href="#">
                        <i class="fa fa-fw fa-dashboard"></i>
                    </a> Thông tin tổng quan
                </h3>
            </div>
            <div class="box-body">
                <div class="col-sm-3">
                    <div class="small-box bg-green" style="height:175px">
                        <div class="inner" style="height:148px">
                            <h3>{{ count($partner) }}</h3>
                            <p>Đối tác</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-fw fa-users"></i>
                        </div>
                        <a href="#" class="small-box-footer">&nbsp</a>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <canvas id="chart-bar-partner-view" height="50"></canvas>
                    <h4>
                        Biểu đồ số lượng đối tác trong tháng&nbsp;
                        <select class="form-control" style="width:70px;display:inline-block" id="select-chart-partner-views-date">
                            @for($i=1;$i<=12;$i++)
                            <option <?= $i==date('m')?'selected':'' ?> value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                        &nbsp;- <span id="total-for-chart"></span>
                    </h4>
                </div>
            </div>
        </div>
        @endif
        
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <a href="{{ route('admin-partner-add') }}">
                        <i class="fa fa-fw fa-plus-square"></i>
                    </a> Danh sách đối tác
                </h3>
            </div>
            
            <div class="box-body">
                <table class="table table-bordered" id="table_partner">
                    <thead>
                        <tr>
                            <th style="width:30px">
                                <input type="checkbox" class="icheck" id="check-all-partner" />
                            </th>
                            <th style="width:30px">STT</th>
                            <th>Tên</th>
                            <th>Tác Vụ</th>
                            <th>Ngày Tạo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($partner)
                        @foreach($partner as $p)
                        <tr data-partner-id="{{ $p['partner_id'] }}">
                            <td style="width:30px;vertical-align:middle">
                                <input type="checkbox" class="icheck check-partner" value="{{ $p['partner_id'] }}" />
                            </td>
                            <td style="width:30px;vertical-align:middle"></td>
                            <td style="position:relative">
                                <a href="{{ route('admin-partner-view', array('id' => $p['partner_id'])) }}">{{ $p['partner_name'] }}</a>
                            </td>
                            <td style="width:220px;text-align:center;vertical-align:middle">
                                <input type="checkbox" class="bootstrap-toggle change-status-partner" data-partner-id="{{ $p['partner_id'] }}" <?= $p['partner_status']==1?'checked':''?> />
                                <a href="{{ route('admin-partner-edit', array('id' => $p['partner_id'])) }}" class="btn btn-default btn-sm no-radius">
                                    <i class="fa fa-fw fa-pencil"></i> Sửa
                                </a>
                                <a data-partner-id="{{ $p['partner_id'] }}" class="btn btn-warning btn-sm no-radius btn-delete-partner">
                                    <i class="fa fa-fw fa-trash"></i> Xoá
                                </a>
                            </td>
                            <td style="vertical-align:middle">{{ change_vn_date(strtotime($p['created_at'])) }}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection