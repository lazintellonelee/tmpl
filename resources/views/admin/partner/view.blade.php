@extends('layouts.layout-admin')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <a href="{{ route('admin-partner-home') }}"><i class="fa fa-fw fa-chevron-circle-left"></i></a> Thông tin chi tiết
                    <a href="{{ route('admin-partner-edit', array("id" => $partner['partner_id'])) }}">
                        <i class="fa fa-fw fa-edit"></i>
                    </a>
                </h3>
            </div>
            <div class="box-body">
                <div class="clearfix"></div>

                <div class="col-sm-12">
                    <table class="table table-responsive">
                        <tbody>
                            <tr>
                                <td rowspan="4" style="width:250px;position:relative">
                                    @if($partner['partner_thumbnail_url'])
                                    <img class="img-responsive" src="{{ $partner['partner_thumbnail_url'] }}" style="width: 160px;"/>
                                    @elseif($partner['partner_thumbnail_image'])
                                    <img class="img-responsive" width="75px" height="37.5px" src="{{ url($partner['partner_thumbnail_image']) }}" style="width: 160px;" />
                                    @else
                                    <img class="img-responsive" width="75px" height="37.5px" src="{{ url('public/images/no-image-available.jpg') }}" style="width: 160px;" />
                                    @endif
                                    
                                    @if($partner['partner_status']=='1')
                                    <i style="position:absolute;bottom:7px;right:14px;font-size:40px" class="text-lime ion ion-ios-checkmark-outline"></i>
                                    @endif
                                </td>
                                <td style="vertical-align:middle"><b>Tên đối tác</b></td>
                                <td style="vertical-align:middle">{{ $partner['partner_name'] }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align:middle"><b>Email</b></td>
                                <td style="vertical-align:middle">{{ $partner['partner_email'] }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align:middle"><b>Điện thoại</b></td>
                                <td style="vertical-align:middle">{{ $partner['partner_phone'] }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align:middle"><b>Địa chỉ</b></td>
                                <td style="vertical-align:middle">{{ $partner['partner_address'] }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align:middle"><b>Trang chủ</b></td>
                                <td style="vertical-align:middle">{{ $partner['partner_homepage'] }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align:middle"><b>Khác</b></td>
                                <td style="vertical-align:middle">{{ $partner['partner_link'] }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align:middle"><b>Ngày tạo</b></td>
                                <td style="vertical-align:middle">{{ change_vn_date(strtotime($partner['created_at'])) }}</td>
                            </tr>
                            <tr>
                                <td style="vertical-align:middle"><b>Ngày cập nhật cuối cùng</b></td>
                                <td style="vertical-align:middle">{{ change_vn_date(strtotime($partner['updated_at'])) }}</td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default" style="border-radius: 0px">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a>
                                        Nội dung đối tác
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    {!! $partner['partner_description'] !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

