/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
//    config.filebrowserBrowseUrl         = $('meta[name=base-url]').attr('content') + '/public/plugins/ckfinder/ckfinder.html';
//    config.filebrowserImageBrowseUrl    = $('meta[name=base-url]').attr('content') + '/public/plugins/ckfinder/ckfinder.html?type=Images';
//    config.filebrowserFlashBrowseUrl    = $('meta[name=base-url]').attr('content') + '/public/plugins/ckfinder/ckfinder.html?type=Flash';

//    config.filebrowserBrowseUrl   = $('meta[name=base-url]').attr('content') + '/public/plugins/ckeditor/plugins/Explorer.aspx';
//    config.filebrowserImageBrowseUrl = $('meta[name=base-url]').attr('content') + '/public/plugins/elFinder/elfinder.html?mode=image';
//    config.filebrowserFlashBrowseUrl = $('meta[name=base-url]').attr('content') + '/public/plugins/elFinder/elfinder.html?mode=flash';
//    config.filebrowserUploadUrl         = $('meta[name=base-url]').attr('content') + '/public/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
//    config.filebrowserImageUploadUrl    = $('meta[name=base-url]').attr('content') + '/public/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
//    config.filebrowserFlashUploadUrl    = $('meta[name=base-url]').attr('content') + '/public/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
    
    config.extraPlugins             = 'lineutils';
    config.extraPlugins             = 'widget';
    config.extraPlugins             = 'image';
    config.extraPlugins             = 'image2';
    config.image2_alignClasses      = [ 'image-left', 'image-center', 'image-right' ];
    config.image2_captionedClass    = 'image-captioned';
    config.filebrowserBrowseUrl     = $('meta[name=base-url]').attr('content') + '/public/plugins/ckeditor/plugins/imageuploader/imgbrowser.php';
    
    // For Media Embed
    config.extraPlugins             = 'dialog';
    config.extraPlugins             = 'oembed';
    config.oembed_maxWidth          = '560';
    config.oembed_maxHeight         = '315';
    config.oembed_WrapperClass      = 'embededContent';
    // For Media Embed

//    config.extraPlugins                 =  'imageuploader';
    config.removePlugins            = 'elementspath';
    config.resize_enabled           = false;
    config.entities_latin           = false;
    config.allowedContent           = true;
    config.filebrowserWindowWidth   = 700;
    config.filebrowserWindowHeight  = 500;
};
