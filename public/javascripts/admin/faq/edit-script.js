function formatState(state) {
    if (!state.id) {
        return state.text;
    }
    var $state = $(
        '<span>' + $.trim(state.text) + '</span>'
    );
    return $state;
};

base_url = $('meta[name=base-url]').attr('content');

$(document).ready(function() {
    CKEDITOR.config.removePlugins               = 'elementspath';
    CKEDITOR.config.resize_enabled              = false;
    CKEDITOR.config.entities_latin              = false;
    //CKEDITOR.config.enterMode                   = CKEDITOR.ENTER_BR;
    CKEDITOR.config.extraPlugins                = 'producttmpl';
    CKEDITOR.config.allowedContent              = true;
    
    CKEDITOR.replace('faq_question', {
        height  : 275,
        toolbar : [
            {
                name    : 'document',
                items   : ['Source', 'Styles', 'Format', 'Font', 'FontSize']
            },
            {
                name    : 'basicstyles',
                items   : ['Undo', 'Redo', '-', 'Bold', 'Italic', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Image', '-', 'Maximize']
            }
        ]
    });
    
    CKEDITOR.replace('faq_answer', {
        height  : 275,
        toolbar : [
            {
                name    : 'document',
                items   : ['Source', 'Styles', 'Format', 'Font', 'FontSize']
            },
            {
                name    : 'basicstyles',
                items   : ['Undo', 'Redo', '-', 'Bold', 'Italic', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Image', '-', 'Maximize']
            }
        ]
    });
    
    $(document).on("click", ".btn_add_faq", function(event){
        event.preventDefault();
        
        var faq_id          = parseInt($(".faq_id").attr("data-id"));
        var faq_question    = CKEDITOR.instances.faq_question.getData();
        var faq_answer      = CKEDITOR.instances.faq_answer.getData();

        $.ajax({
            url: base_url + "/admin/faq/add.html",
            type: "POST",
            data: {
                faq_question: faq_question,
                faq_answer  : faq_answer,
                "_token"    : $('meta[name=csrf-token]').attr('content')
            },
            success:function(result){
                window.location.href = base_url + "/admin/faq/home.html";
            }
        });
        return true;
    });
});