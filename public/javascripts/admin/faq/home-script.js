base_url    = $('meta[name=base-url]').attr('content');
table       = null;

dataTableOptions = {
    "aoColumnDefs"  : [{
        "aTargets"  : [0, 3],
        "bSortable" : false
    }],
    "paging"            : true,
    "lengthChange"      : true,
    "searching"         : true,
    "ordering"          : true,
    "info"              : false,
    "autoWidth"         : false,
    'language'          : {
        'zeroRecords'   : '<p class="text-center">Không câu hỏi !!</p>',
        'lengthMenu'    : 'Hiển thị&nbsp; _MENU_',
        'search'        : 'Tìm _INPUT_ <button id="btn-delete-faq" class="btn btn-warning btn-sm no-radius"><i class="fa fa-fw fa-trash"></i></button>'
    }
};

var DeleteFaq = function(arrID, cb)
{
    var http    = new XMLHttpRequest();
    var form    = new FormData();
    for(var i=0;i<arrID.length;i++)
    {
        form.append('faq_id[]', arrID[i]);
    }
    form.append('_token', $('meta[name=csrf-token]').attr('content'));
    http.open('POST', base_url + '/admin/faq/delete.html', true);
    http.onload = function(event) 
    {
        HideLoadingAjax();
        cb(JSON.parse(this.responseText));
    };
    http.send(form);
    ShowLoadingAjax();
};

$(document).ready(function() {
    $('input.bootstrap-toggle').bootstrapToggle({
        size        : 'small',
        on          : 'Hiển thị',
        off         : 'Ẩn'
    });
    
    $('[data-toggle=toggle]').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-right':'none','border-bottom':'none'});
    $('label.toggle-on').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-left':'none','border-bottom':'none'});
    $('label.toggle-off').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-right':'none','border-bottom':'none'});
    
    $('input.icheck').iCheck({
        checkboxClass   : 'icheckbox_minimal-blue',
        radioClass      : 'iradio_minimal-blue'
    });
    
    table = $('#table_faq').DataTable(dataTableOptions);
    
    table.on( 'order.dt search.dt', function () {
        table.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
    
    $(document).on('ifChanged', '#check-all-faq', function(event) {
        $('.check-faq').iCheck($(this).prop('checked')?'check':'uncheck');
    });
    
    $(document).on('click', '.btn-delete-faq', function(event) {
        var that    = $(this);
        var faq_id = that.attr('data-faq-id');
        CallConfirmNoty('Bạn muốn xoá câu hỏi ' + $.trim(that.parents().eq(1).find('td:nth-child(3)').html().replace(/\&nbsp\;/g,' ')).replace(/<img[^>]+\>/i, '').replace(/<i[^>]+\><\/i\>/i, '') + '?', function(){
            DeleteFaq([faq_id], function(result) {
                if(result.error !== undefined)
                {
                    CallNoty('warning', '<i class="fa fa-fw fa-exclamation-triangle"></i> Xoá câu hỏi không thành công! Vui lòng thử lại.');
                    return true;
                }
                CallNoty('success', '<i class="fa fa-fw fa-check"></i> Xoá câu hỏi thành công.');
                table
                .row($('tr[data-faq-id=' + faq_id + ']'))
                .remove()
                .draw(false);
            });
        });
    });
    
    $(document).on('click', '#btn-delete-faq', function(event) {
        var arrID = $('input.check-faq:checked').map(function(index, element) {
            return $(element).val();
        }).get();
        
        if(arrID.length === 0)
        {
            CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Bạn cần chọn ít nhất 1 câu hỏi.');
            return true;
        }
        
        CallConfirmNoty('Bạn muốn xoá những câu hỏi này?', function() {
            DeleteFaq(arrID, function(result) {
                if(result.error !== undefined)
                {
                    CallNoty('warning', '<i class="fa fa-fw fa-exclamation-triangle"></i> Xoá câu hỏi không thành công! Vui lòng thử lại.');
                    return true;
                }
                CallNoty('success', '<i class="fa fa-fw fa-check"></i> Xoá câu hỏi thành công.');
                for(var i=0;i<arrID.length;i++)
                {
                    table
                    .row($('tr[data-faq-id=' + arrID[i] + ']'))
                    .remove()
                    .draw(false);
                }
            });
        });
    });
    
    var ChangeStatusTimeout = null;
    var OldStatusValue      = null;
    $(document).on('change', '.change-status-faq', function() {
        var that        = this;
        var faq_id      = $(this).attr('data-faq-id');
        var faq_status  = $(this).prop('checked')?1:0;
        if(OldStatusValue === null) OldStatusValue  = faq_status===1?0:1;
        clearTimeout(ChangeStatusTimeout);
        ChangeStatusTimeout = setTimeout(function(){
            if(OldStatusValue === faq_status) {
                OldStatusValue = null;
                return false;
            }
            
            var http        = new XMLHttpRequest();
            var form        = new FormData();
            form.append('faq_id', faq_id);
            form.append('faq_status', faq_status);
            form.append('_token', $('meta[name=csrf-token]').attr('content'));
            http.open('POST', base_url + '/admin/faq/change-status.html', true);
            http.onload = function(event)
            {
                HideLoadingAjax();
                var result = JSON.parse(this.responseText);
                if(result.error !== undefined)
                {
                    $(that).prop('checked', faq_status===1?false:true).change();
                    CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Cập nhật trạng thái câu hỏi không thành công! Vui lòng thử lại.');
                    return true;
                }
                OldStatusValue = null;
                CallNoty('success', '<i class="fa fa-fw fa-check"></i> Cập nhật trạng thái câu hỏi thành công.');
            };
            http.send(form);
            ShowLoadingAjax();
        }, 1000);
    });
});