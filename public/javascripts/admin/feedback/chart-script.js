base_url    = $('meta[name=base-url]').attr('content');

var GetDataForChartFeedbackViews = function(date) {
    
    var http = new XMLHttpRequest();
    http.open('POST', base_url + '/admin/feedback/get-data-chart-feedback-views.html', true);
    http.setRequestHeader('X-CSRF-TOKEN', $('meta[name=csrf-token]').attr('content'));
    
    if(date !== undefined)
    {
        var form = new FormData();
        form.append('date', date);
    }
    
    http.onload = function(event)
    {
        var data    = JSON.parse(this.responseText);
        var labels  = [];
        var yData   = [];
        var total   = 0;
        for(var i=0; i<data.length; i++)
        {
            total = total + parseInt(data[i].feedback);
            labels.push(data[i].date);
            yData.push(data[i].feedback);
        }
        
        if(data.length === 0)
        {
            labels  = ['1','2','3','4','5','6','7','8','9','10','11','12'];
            yData   = [0,0,0,0,0,0,0,0,0,0,0,0];
        }
        
        $('#total-for-chart').html('tổng số phản hồi: ' + total);
        
        var datasets = [
            {
                fillColor : "rgb(51, 153, 255)",
                strokeColor : "rgb(51, 153, 255)",
                highlightFill: "rgb(51, 153, 255)",
                highlightStroke: "rgb(51, 153, 255)",
                data : yData
            }
        ];
        
        var ctx         = document.getElementById('chart-bar-feedback-view').getContext("2d");
        window.myBar    = new Chart(ctx).Bar({
            labels      : labels,
            datasets    : datasets
        }, {
            responsive  : true,
            // String - Template string for single tooltips
            tooltipTemplate: "<%if (label){%>Số lượng phản hồi trong ngày <%=label%>: <%}%><%= value %>"
        });
    };
    if(date !== undefined)
    {
        http.send(form);
        return true;
    }
    http.send();
};

$(document).ready(function() {
    GetDataForChartFeedbackViews();
    
    $(document).on('change', '#select-chart-feedback-views-date', function() {
        var date = $(this).val();
        GetDataForChartFeedbackViews(date);
    });
});