base_url    = $('meta[name=base-url]').attr('content');
table       = null;

dataTableOptions = {
    "aoColumnDefs"  : [{
        "aTargets"  : [0, 6],
        "bSortable" : false
    }],
    "paging"            : true,
    "lengthChange"      : true,
    "searching"         : true,
    "ordering"          : true,
    "info"              : false,
    "autoWidth"         : false,
    'language'          : {
        'zeroRecords'   : '<p class="text-center">Không phản hồi !!</p>',
        'lengthMenu'    : 'Hiển thị&nbsp; _MENU_',
        'search'        : 'Tìm _INPUT_ <button id="btn-delete-feedback" class="btn btn-warning btn-sm no-radius"><i class="fa fa-fw fa-trash"></i></button>'
    }
};

var DeleteFeedback = function(arrID, cb)
{
    var http    = new XMLHttpRequest();
    var form    = new FormData();
    for(var i=0;i<arrID.length;i++)
    {
        form.append('feedback_id[]', arrID[i]);
    }
    form.append('_token', $('meta[name=csrf-token]').attr('content'));
    http.open('POST', base_url + '/admin/feedback/delete.html', true);
    http.onload = function(event) 
    {
        HideLoadingAjax();
        cb(JSON.parse(this.responseText));
    };
    http.send(form);
    ShowLoadingAjax();
};

$(document).ready(function() {
    $('[data-toggle=toggle]').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-right':'none','border-bottom':'none'});
    $('label.toggle-on').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-left':'none','border-bottom':'none'});
    $('label.toggle-off').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-right':'none','border-bottom':'none'});
    
    $('input.icheck').iCheck({
        checkboxClass   : 'icheckbox_minimal-blue',
        radioClass      : 'iradio_minimal-blue'
    });
    
    table = $('#table_feedback').DataTable(dataTableOptions);
    
    table.on( 'order.dt search.dt', function () {
        table.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
    
    $(document).on('ifChanged', '#check-all-feedback', function(event) {
        $('.check-feedback').iCheck($(this).prop('checked')?'check':'uncheck');
    });
    
    $(document).on('click', '.btn-delete-feedback', function(event) {
        var that    = $(this);
        var feedback_id = that.attr('data-feedback-id');
        CallConfirmNoty('Bạn muốn xoá phản hồi ' + $.trim(that.parents().eq(1).find('td:nth-child(3)').html().replace(/\&nbsp\;/g,' ')).replace(/<img[^>]+\>/i, '').replace(/<i[^>]+\><\/i\>/i, '') + '?', function(){
            DeleteFeedback([feedback_id], function(result) {
                if(result.error !== undefined)
                {
                    CallNoty('warning', '<i class="fa fa-fw fa-exclamation-triangle"></i> Xoá phản hồi không thành công! Vui lòng thử lại.');
                    return true;
                }
                CallNoty('success', '<i class="fa fa-fw fa-check"></i> Xoá phản hồi thành công.');
                table
                .row($('tr[data-feedback-id=' + feedback_id + ']'))
                .remove()
                .draw(false);
            });
        });
    });
    
    $(document).on('click', '#btn-delete-feedback', function(event) {
        var arrID = $('input.check-feedback:checked').map(function(index, element) {
            return $(element).val();
        }).get();
        
        if(arrID.length === 0)
        {
            CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Bạn cần chọn ít nhất 1 phản hồi.');
            return true;
        }
        
        CallConfirmNoty('Bạn muốn xoá những phản hồi này?', function() {
            DeleteFeedback(arrID, function(result) {
                if(result.error !== undefined)
                {
                    CallNoty('warning', '<i class="fa fa-fw fa-exclamation-triangle"></i> Xoá phản hồi không thành công! Vui lòng thử lại.');
                    return true;
                }
                CallNoty('success', '<i class="fa fa-fw fa-check"></i> Xoá phản hồi thành công.');
                for(var i=0;i<arrID.length;i++)
                {
                    table
                    .row($('tr[data-feedback-id=' + arrID[i] + ']'))
                    .remove()
                    .draw(false);
                }
            });
        });
    });
});