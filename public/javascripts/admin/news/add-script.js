function formatState(state) {
    if (!state.id) {
        return state.text;
    }
    var $state = $(
        '<span>' + $.trim(state.text) + '</span>'
    );
    return $state;
};

base_url = $('meta[name=base-url]').attr('content');

$(document).ready(function() {
    var newsTags = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('news_tag_name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            cache   : false,
            url     : base_url + "/admin/news/json-news-tags.html",
            filter  : function (data) {
                return $.map(data, function (tag) {
                    return {
                        news_tag_id     : tag.news_tag_id,
                        news_tag_name   : tag.news_tag_name,
                        news_tag_url    : tag.news_tag_url
                    };
                });
            }
        }
    });
    newsTags.initialize();
    
    var news = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('news_title'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            cache   : false,
            url     : base_url + "/admin/news/json-news.html",
            filter  : function (data) {
                return $.map(data, function (news) {
                    return {
                        news_id     : news.news_id,
                        news_title  : news.news_title
                    };
                });
            }
        }
    }); 
    news.initialize();
    
    $('#news_tags').tagsinput({
        typeaheadjs : {
            name        : 'newsTags',
            displayKey  : 'news_tag_name',
            valueKey    : 'news_tag_name',
            source      : newsTags.ttAdapter()
        }
    });
    
    $('#news_related').tagsinput({
        itemValue   : 'news_id',
        itemText    : 'news_title',
        typeaheadjs : {
            name        : 'news',
            displayKey  : 'news_title',
            source      : news.ttAdapter()
        }
    });
    
    $('#news_catgory_id').select2({
        templateSelection   : formatState,
        width               : '100%'
    });
    
    CKEDITOR.replace('news_content', {
        height  : 275,
        toolbar : [
            {
                name    : 'document',
                items   : ['Source', 'Styles', 'Format', 'Font', 'FontSize']
            },
            {
                name    : 'basicstyles',
                items   : ['Undo', 'Redo', '-', 'Bold', 'Italic', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Image', '-', 'Maximize']
            }
        ]
    });
    
    $(document).on('change', '#news_avatar', function(event) {
        readFileInput(this, function(e) {
            var img         = e.target.result;
            var container   = $('#div-news-avatar-review');
            container.fadeIn(500, function() {
                container.css({
                    'background-image' : 'url(\'' + img + '\')'
                });
            });
        });
    });
    
    $('#btn-crop-news-avatar').on('click', function(event) {
        var modal   = $('#modal-container-cropper-master');
        var input   = document.getElementsByName('news_avatar')[0];
        if(input.files.length === 0)
        {
            CallNoty('warning', '<i class="fa fa-fw fa-exclamation-triangle"></i> Bạn vui lòng tải hình ảnh lên!');
            return true;
        }
        var image   = $('#img-cropper-master');
        readFileInput(input, function(e) {
            image.attr('src', e.target.result);
            image.cropper({
                aspectRatio         : 6 / 3,
                minContainerWidth   : parseInt(modal.find('.modal-dialog').width()) - 30,
                minContainerHeight  : 450
            });
            modal.modal('show');
        });
    });
    
    $(document).on('click', '#btn-crop-image', function(event) {
        var container   = $('#div-news-avatar-review');
        var input       = $('input[name=news_avatar_base64]');
        var modal       = $('#modal-container-cropper-master');
        var image       = $('#img-cropper-master'); 
        var data        = image.cropper('getCroppedCanvas').toDataURL();
        container.css({'background-image':'url(\'' + data + '\')'});
        modal.modal('hide');
        input.val(data);
    });
    
    $(document).on('hidden.bs.modal', '#modal-container-cropper-master', function() {
        var image   = $('#img-cropper-master'); 
        image.cropper('destroy');
        image.attr('src', '');
    });
    
    $(document).on('click', '.change-formality-news-avatar', function(event) {
        event.preventDefault();
        var formality   = $(this).attr('data-formality');
        var button      = $(this).parents().eq(2).find('button');
        var input       = $(this).parents().eq(3).find('input[id=news_avatar]');
        var span        = $(this).parents().eq(3).find('span');
        switch(formality) {
            case 'url': 
                button.html('Tải lên <span class="caret"></span>');
                input.attr('type', 'file');
                input.attr('name', 'news_avatar');
                span.removeClass('hidden');
                break;
            case 'server':
                button.html('Liên kết ngoài <span class="caret"></span>');
                $(this).parents().eq(3).find('input[name=news_avatar_base64]').val('');
                $('#div-news-avatar-review').css({'display':'none'});
                input.attr('type', 'text');
                input.attr('name', 'news_avatar_url');
                span.addClass('hidden');
                break;
        }
        $('.change-formality-news-avatar').not(this).parent().removeClass('active');
        $(this).parent().addClass('active');
    });
});