base_url    = $('meta[name=base-url]').attr('content');
//table       = null;
//
//dataTableOptions = {
//    "aoColumnDefs"  : [{
//        "aTargets"  : [0, 3],
//        "bSortable" : false
//    }],
//    "paging"            : true,
//    "lengthChange"      : true,
//    "searching"         : true,
//    "ordering"          : true,
//    "info"              : false,
//    "autoWidth"         : false,
//    'language'          : {
//        'zeroRecords'   : '<p class="text-center">Không tin tức !!</p>',
//        'lengthMenu'    : 'Hiển thị&nbsp; _MENU_',
//        'search'        : 'Tìm _INPUT_ <button id="btn-delete-news" class="btn btn-warning btn-sm no-radius"><i class="fa fa-fw fa-trash"></i></button>'
//    }
//};

function formatState(state) {
    if (!state.id) {
        return state.text;
    }
    var $state = $(
        '<span>' + $.trim(state.text) + '</span>'
    );
    return $state;
};


var DeleteNews = function(arrID, cb)
{
    var http    = new XMLHttpRequest();
    var form    = new FormData();
    for(var i=0;i<arrID.length;i++)
    {
        form.append('news_id[]', arrID[i]);
    }
    form.append('_token', $('meta[name=csrf-token]').attr('content'));
    http.open('POST', base_url + '/admin/news/delete.html', true);
    http.onload = function(event) 
    {
        HideLoadingAjax();
        cb(JSON.parse(this.responseText));
    };
    http.send(form);
    ShowLoadingAjax();
};

$(document).ready(function() {
    $('#news_category_id').select2({
        templateSelection   : formatState,
        width               : '100%'
    });
    
    $('#news_category_id').on('change', function(event) {
        if($(this).val() === '0')
        {
            window.location = window.location.href.split('?')[0];
            return true;
        }
        window.location = window.location.href.split('?')[0] + '?ct=' + $(this).val();
    });
    
    $('input.bootstrap-toggle').bootstrapToggle({
        size        : 'small',
        on          : 'Hiển thị',
        off         : 'Ẩn'
    });
    
    $('[data-toggle=toggle]').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-right':'none','border-bottom':'none'});
    $('label.toggle-on').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-left':'none','border-bottom':'none'});
    $('label.toggle-off').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-right':'none','border-bottom':'none'});
    
    $('input.icheck').iCheck({
        checkboxClass   : 'icheckbox_minimal-blue',
        radioClass      : 'iradio_minimal-blue'
    });
    
//    table = $('#table_news').DataTable(dataTableOptions);
    
    $(document).on('ifChanged', '#check-all-news', function(event) {
        $('.check-news').iCheck($(this).prop('checked')?'check':'uncheck');
    });
    
    $(document).on('click', '.btn-delete-news', function(event) {
        var that    = $(this);
        var news_id = that.attr('data-news-id');
        CallConfirmNoty('Bạn muốn xoá tin tức ' + $.trim(that.parents().eq(1).find('td:nth-child(3)').html().replace(/\&nbsp\;/g,' ')).replace(/<img[^>]+\>/i, '').replace(/<i[^>]+\><\/i\>/i, '') + '?', function(){
            DeleteNews([news_id], function(result) {
                if(result.error !== undefined)
                {
                    CallNoty('warning', '<i class="fa fa-fw fa-exclamation-triangle"></i> Xoá tin tức không thành công! Vui lòng thử lại.');
                    return true;
                }
                CallNoty('success', '<i class="fa fa-fw fa-check"></i> Xoá tin tức thành công.');
                table
                .row($('tr[data-news-id=' + news_id + ']'))
                .remove()
                .draw(false);
            });
        });
    });
    
    $(document).on('click', '#btn-delete-news', function(event) {
        var arrID = $('input.check-news:checked').map(function(index, element) {
            return $(element).val();
        }).get();
        
        if(arrID.length === 0)
        {
            CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Bạn cần chọn ít nhất 1 tin tức.');
            return true;
        }
        
        CallConfirmNoty('Bạn muốn xoá những tin tức này?', function() {
            DeleteNews(arrID, function(result) {
                if(result.error !== undefined)
                {
                    CallNoty('warning', '<i class="fa fa-fw fa-exclamation-triangle"></i> Xoá tin tức không thành công! Vui lòng thử lại.');
                    return true;
                }
                CallNoty('success', '<i class="fa fa-fw fa-check"></i> Xoá tin tức thành công.');
                for(var i=0;i<arrID.length;i++)
                {
                    table
                    .row($('tr[data-news-id=' + arrID[i] + ']'))
                    .remove()
                    .draw(false);
                }
            });
        });
    });
    
    var ChangeStatusTimeout = null;
    var OldStatusValue      = null;
    $(document).on('change', '.change-status-news', function() {
        var that        = this;
        var news_id     = $(this).attr('data-news-id');
        var news_status = $(this).prop('checked')?1:0;
        if(OldStatusValue === null) OldStatusValue  = news_status===1?0:1;
        clearTimeout(ChangeStatusTimeout);
        ChangeStatusTimeout = setTimeout(function(){
            if(OldStatusValue === news_status) {
                OldStatusValue = null;
                return false;
            }
            
            var http        = new XMLHttpRequest();
            var form        = new FormData();
            form.append('news_id', news_id);
            form.append('news_status', news_status);
            form.append('_token', $('meta[name=csrf-token]').attr('content'));
            http.open('POST', base_url + '/admin/news/change-status.html', true);
            http.onload = function(event)
            {
                HideLoadingAjax();
                var result = JSON.parse(this.responseText);
                if(result.error !== undefined)
                {
                    $(that).prop('checked', news_status===1?false:true).change();
                    CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Cập nhật trạng thái tin tức không thành công! Vui lòng thử lại.');
                    return true;
                }
                OldStatusValue = null;
                CallNoty('success', '<i class="fa fa-fw fa-check"></i> Cập nhật trạng thái tin tức thành công.');
            };
            http.send(form);
            ShowLoadingAjax();
        }, 1000);
    }); 
    
    $(document).on('click', '.change-verified', function(event) {
        event.preventDefault();
        var that            = this;
        var news_id         = $(this).attr('data-news-id');
        var news_verified   = parseInt($(this).attr('data-news-verified'));
        CallConfirmNoty('Bạn muốn ' + (news_verified===0?'xác thực':'huỷ xác thực') + ' tin tức này?', function() {
            var http    = new XMLHttpRequest();
            var form    = new FormData();
            form.append('news_id', news_id);
            form.append('news_verified', news_verified===0?'1':'0');
            form.append('_token', $('meta[name=csrf-token]').attr('content'));
            http.open('POST', base_url + '/admin/news/change-verified.html', true);
            http.onload = function(event)
            {
                console.log(this.responseText);
                HideLoadingAjax();
                var result = JSON.parse(this.responseText);
                if(result.error !== undefined)
                {
                    CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> ' + (news_verified===0?'Xác thực':'Gỡ xác thực') + ' tin tức không thành công! Vui lòng thử lại.');
                    return true;
                }
                $(that).removeClass(news_verified===0?'bg-info':'bg-green');
                $(that).addClass(news_verified===0?'bg-green':'bg-info');
                $(that).find('i').removeClass(news_verified===0?'fa-times':'fa-check');
                $(that).find('i').addClass(news_verified===0?'fa-check':'fa-times');
                $(that).attr('data-news-verified', news_verified===0?1:0);
                CallNoty('success', '<i class="fa fa-check"></i> ' + (news_verified===0?'Xác thực':'Gỡ xác thực') + ' tin tức thành công!');
                return true;
            };
            http.send(form);
            ShowLoadingAjax();
        });
    });
});