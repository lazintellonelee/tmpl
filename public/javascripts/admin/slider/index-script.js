base_url    = $('meta[name=base-url]').attr('content');
table       = null;

dataTableOptions = {
    "aoColumnDefs"  : [{
        "aTargets"  : [0, 3],
        "bSortable" : false
    }],
    "paging"            : true,
    "lengthChange"      : true,
    "searching"         : true,
    "ordering"          : true,
    "info"              : false,
    "autoWidth"         : false,
    'language'          : {
        'zeroRecords'   : '<p class="text-center">Không slider !!</p>',
        'lengthMenu'    : 'Hiển thị&nbsp; _MENU_',
        'search'        : 'Tìm _INPUT_ <button id="btn-delete-slider" class="btn btn-warning btn-sm no-radius"><i class="fa fa-fw fa-trash"></i></button>'
    }
};

var DeleteSlider = function(arr, cb)
{
    var form = new FormData();
    for(var i=0;i<arr.length;i++)
    {
        form.append('slide_id[]', arr[i]);
    }
    var http = new XMLHttpRequest();
    http.open('POST', base_url + '/admin/slider/delete.html', true);
    http.setRequestHeader('X-CSRF-TOKEN', $('meta[name=csrf-token]').attr('content'));
    http.onload = function(event)
    {
        cb(this.responseText);
    };
    http.send(form);
};

var BuildTemplateSlider = function(result)
{
    var slide = $('#carousel-example-generic');
    if(result.slide_status === 1)
    {
        if(slide.length === 0) 
        {
            $('#main-box').before(tmpl('tmpl-slide', result));
            return true;
        }
        
        result.slide_to = $('ol.carousel-indicators').find('li').length;
        slide.find('ol.carousel-indicators').append(tmpl('tmpl-carousel-indicators', result));
        slide.find('div.carousel-inner').append(tmpl('tmpl-carousel-inner', result));
        return true;
    }
    
    var indicators  = slide.find('ol.carousel-indicators').find('li[data-slide-id=' + result.slide_id + ']');
    slide.find('ol.carousel-indicators').find('li').removeClass('active');
    slide.find('ol.carousel-indicators').find('li').not(indicators).first().addClass('active');
    indicators.remove();
    var inner       = slide.find('div.carousel-inner').find('div.item[data-slide-id=' + result.slide_id + ']');
    slide.find('div.carousel-inner').find('div.item').removeClass('active');
    slide.find('div.carousel-inner').find('div.item').not(inner).first().addClass('active');
    inner.remove();
    
    if(slide.find('ol.carousel-indicators').find('li').length === 0)
    {
        slide.parents().eq(1).remove();
    }
};  

$(document).ready(function() {
    $('input.bootstrap-toggle').bootstrapToggle({
        size        : 'small',
        on          : 'Hiển thị',
        off         : 'Ẩn'
    });
    
    $('[data-toggle=toggle]').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-right':'none','border-bottom':'none'});
    $('label.toggle-on').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-left':'none','border-bottom':'none'});
    $('label.toggle-off').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-right':'none','border-bottom':'none'});
    
    $('input.icheck').iCheck({
        checkboxClass   : 'icheckbox_minimal-blue',
        radioClass      : 'iradio_minimal-blue'
    });
    
    table = $('#table_sliders').DataTable(dataTableOptions);
    
    $(document).on('ifChanged', '#check-all-slider', function(event) {
        $('.check-slider').iCheck($(this).prop('checked')?'check':'uncheck');
    });
    
    $(document).on('click', '.btn-delete-slide', function() {
        var that        = $(this);
        var slide_id    = $(this).attr('data-slide-id');
        CallConfirmNoty('Bạn muốn xoá slide ' + $.trim(that.parents().eq(1).find('td:nth-child(3)').html().replace(/\&nbsp\;/g,' ')).replace(/<img[^>]+\>/i, '').replace(/<i[^>]+\><\/i\>/i, '') + '?', function(){
            DeleteSlider([slide_id], function(response) {
                var result = JSON.parse(response);
                if(result.error !== undefined)
                {
                    CallNoty('warning', 'Không thể xoá slide! Vui lòng thử lại.');
                    return false;
                }
                CallNoty('success', 'Xoá slide thành công.');
                table
                .row($('tr[data-slide-id=' + slide_id + ']'))
                .remove()
                .draw(false);
            });
        });
    });
    
    $(document).on('click', '#btn-delete-slider', function() {
        var arrID = $('input.check-slider:checked').map(function(index, element) {
            return $(element).val();
        }).get();
        
        if(arrID.length === 0)
        {
            CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Bạn cần chọn ít nhất 1 slide.');
            return true;
        }
        
        CallConfirmNoty('Bạn muốn xoá những slide?', function(){
            DeleteSlider(arrID, function(response) {
                var result = JSON.parse(response);
                if(result.error !== undefined)
                {
                    CallNoty('warning', 'Không thể xoá slide! Vui lòng thử lại.');
                    return false;
                }
                CallNoty('success', 'Xoá slide thành công.');
                for(var i=0;i<arrID.length;i++)
                {
                    table
                    .row($('tr[data-slide-id=' + arrID[i] + ']'))
                    .remove()
                    .draw(false);
                }
            });
        });
    });
    
    var ChangeStatusTimeout = null;
    var OldStatusValue      = null;
    $(document).on('change', '.change-status-slide', function() {
        var that            = this;
        var slide_id        = $(this).attr('data-slide-id');
        var slide_status    = $(this).prop('checked')?1:0;
        if(OldStatusValue === null) OldStatusValue  = slide_status===1?0:1;
        clearTimeout(ChangeStatusTimeout);
        ChangeStatusTimeout = setTimeout(function(){
            if(OldStatusValue === slide_status) {
                OldStatusValue = null;
                return false;
            }
            
            var http        = new XMLHttpRequest();
            var form        = new FormData();
            form.append('slide_id', slide_id);
            form.append('slide_status', slide_status);
            form.append('_token', $('meta[name=csrf-token]').attr('content'));
            http.open('POST', base_url + '/admin/slider/change-status.html', true);
            http.onload = function(event)
            {
                HideLoadingAjax();
                var result = JSON.parse(this.responseText);
                if(result.error !== undefined)
                {
                    $(that).prop('checked', slide_status===1?false:true).change();
                    CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Cập nhật trạng thái tin tức không thành công! Vui lòng thử lại.');
                    return true;
                }
                OldStatusValue = null;
                CallNoty('success', '<i class="fa fa-fw fa-check"></i> Cập nhật trạng thái tin tức thành công.');
                BuildTemplateSlider(result);
            };
            http.send(form);
            ShowLoadingAjax();
        }, 1000);
    }); 
    
    $(document).on('click', '.btn-turn-off-slide', function(event) {
        var slide_id    = $(this).attr('data-slide-id');
        var form        = new FormData();
        form.append('slide_id', slide_id);
        form.append('slide_status', 0);
        var http        = new XMLHttpRequest();
        http.open('POST', base_url + '/admin/slider/change-status.html', true);
        http.setRequestHeader('X-CSRF-TOKEN', $('meta[name=csrf-token]').attr('content'));
        http.onload = function(event)
        {
            var result = JSON.parse(this.responseText);
            if(result.error !== undefined)
            {
                CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Tắt slide không thành công! Vui lòng thử lại.');
                return true;
            }
            
            CallNoty('success', '<i class="fa fa-fw fa-check"></i> Tắt slide thành công.');
            BuildTemplateSlider(result);
        };
        http.send(form);
    });
});