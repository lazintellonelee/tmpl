base_url = $('meta[name=base-url]').attr('content');

$(document).ready(function() {
    $(document).on('change', '#slide_image', function(event) {
        if($('input[name=slide_type]').val() === 'video') return true;
        
        readFileInput(this, function(e) {
            var img         = e.target.result;
            var container   = $('#div-slide-image-review');
            container.fadeIn(500, function() {
                container.css({
                    'background-image' : 'url(\'' + img + '\')'
                });
            });
        });
    });
    
    $('#btn-crop-slide-image').on('click', function(event) {
        var modal   = $('#modal-container-cropper-master');
        var input   = document.getElementsByName('slide_image')[0];
        if(input.files.length === 0)
        {
            CallNoty('warning', '<i class="fa fa-fw fa-exclamation-triangle"></i> Bạn vui lòng tải hình ảnh lên!');
            return true;
        }
        var image   = $('#img-cropper-master');
        readFileInput(input, function(e) {
            image.attr('src', e.target.result);
            image.cropper({
                aspectRatio         : 21 / 5,
                minContainerWidth   : parseInt(modal.find('.modal-dialog').width()) - 30,
                minContainerHeight  : 450
            });
            modal.modal('show');
        });
    });
    
    $(document).on('click', '#btn-crop-image', function(event) {
        var container   = $('#div-slide-image-review');
        var input       = $('input[name=slide_image_base64]');
        var modal       = $('#modal-container-cropper-master');
        var image       = $('#img-cropper-master'); 
        var data        = image.cropper('getCroppedCanvas').toDataURL();
        container.css({'background-image':'url(\'' + data + '\')'});
        modal.modal('hide');
        input.val(data);
    });
    
    $(document).on('click', '.slider-url-type', function(event) {
        event.preventDefault();
        var that    = this;
        var sync    = $(this).attr('data-sync') === "true" ? true : false;
        var send    = $(this).attr('data-send');
        var value   = $(this).attr('data-value');
        var display = $(this).attr('data-display');
        var text    = $(this).attr('data-text');
        
        if(sync) 
        {
            if($(this).parent().hasClass('active')) return true;
            $('.slider-url-type').not(this).parent().removeClass('active');
            $(this).parent().addClass('active');
            $(this).parents().eq(3).find('button').html(text + ' <span class="caret"></span>');
            $('#real_slide_url').val('');
            $('#slide_url').val('');

            var data = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('title'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: {
                    cache   : false,
                    url     : base_url + "/admin/slider/" + send + "/get-data-slider-url.html",
                    filter  : function (data) {
                        return $.map(data, function(item) {
                            return {
                                title   : item[display],
                                url     : item[value]
                            };
                        });
                    }
                }
            });
            data.initialize();
            $('#slide_url').typeahead('destroy');
            $('#slide_url').typeahead(null, {
                name        : 'data',
                valueKey    : 'url',
                displayKey  : 'title',
                source      : data.ttAdapter(),
                templates   : {
                    suggestion : function(data) {
                        return '<div>' + data.title + '</div>';
                    }
                }
            });
            $('.twitter-typeahead').css({
                width   : '100%',
                top     : '2px'
            });
            $('.tt-menu').css({
                width   : '100% !important'
            });
            return true;
        }
        
        $('.slider-url-type').not(this).parent().removeClass('active');
        $('.slider-url-type[data-type=url]').parent().addClass('active');
        $(this).parents().eq(3).find('button').html('Liên kết <span class="caret"></span>');
        $('#real_slide_url').val(value);
        $('#slide_url').val(value);
        return true;
    });
    
    $(document).on('change', '#slide_url', function(event) {
        $('#real_slide_url').val($(this).val());
    });
        
    $('#slide_url').bind('typeahead:select', function(event, suggestion) {
        $('#real_slide_url').val(suggestion.url);
    });
    
    $(document).on('click', '.slider-type', function(event) {
        event.preventDefault();
        var that    = this;
        var button  = $(this).parents().eq(3).find('button').first();
        var li      = $(this).parent();
        var type    = $(this).attr('data-type');
        var text    = $(this).attr('data-text');
        var input   = $('#slide_image');
        
        if(li.hasClass('active')) return true;
        $(this).parents().eq(3).find('button').last().toggleClass('hidden');
        switch(type) {
            case "image":
                $(this).parents().eq(3).find('button').parent().last().removeClass('hidden');
                break;
            case "video":
                $('input[name=slide_image_base64]').val('');
                $('#div-slide-image-review').fadeOut();
                $(this).parents().eq(3).find('button').parent().last().addClass('hidden');
                break;
        }
        button.html(text + ' <span class="caret"></span>');
        $('.slider-type').parent().removeClass('active');
        $('input[name=slide_type]').val(type);
        input.attr('name', 'slide_' + type);
        li.addClass('active');
    });
});