function formatState(state) {
    if (!state.id) {
        return state.text;
    }
    var $state = $(
        '<span>' + $.trim(state.text) + '</span>'
    );
    return $state;
}
;

$(document).ready(function() {
    $('#news_catgory_parent_id').select2({
        templateSelection   : formatState,
        width               : '100%'
    });
    
    $('#btn-crop-news-avatar').on('click', function(event) {
        var modal   = $('#modal-container-cropper-master');
        var input   = document.getElementsByName('news_category_avatar')[0];
        if(input.files.length === 0)
        {
            CallNoty('warning', '<i class="fa fa-fw fa-exclamation-triangle"></i> Bạn vui lòng tải hình ảnh lên!');
            return true;
        }
        var image   = $('#img-cropper-master');
        readFileInput(input, function(e) {
            image.attr('src', e.target.result);
            image.cropper({
                aspectRatio         : 6 / 3,
                minContainerWidth   : parseInt(modal.find('.modal-dialog').width()) - 30,
                minContainerHeight  : 450
            });
            modal.modal('show');
        });
    });
    
    $(document).on('click', '#btn-crop-image', function(event) {
        var container   = $('#div-news-category-avatar-review');
        var input       = $('input[name=news_category_avatar_base64]');
        var modal       = $('#modal-container-cropper-master');
        var image       = $('#img-cropper-master'); 
        var data        = image.cropper('getCroppedCanvas').toDataURL();
        container.css({'background-image':'url(\'' + data + '\')'});
        modal.modal('hide');
        input.val(data);
    });
    
    $(document).on('hidden.bs.modal', '#modal-container-cropper-master', function() {
        var image   = $('#img-cropper-master'); 
        image.cropper('destroy');
        image.attr('src', '');
    });
    
    $(document).on('change', 'input[name=news_category_avatar]', function(event) {
        var that        = this;
        var container   = $('#div-news-category-avatar-review');
        readFileInput(that, function(e) {
            container.fadeIn('500', function() {
                container.css({
                    'background-image'      : 'url(\'' + e.target.result + '\')',
                    'background-size'       : 'contain',
                    'background-position'   : 'center',
                    'background-repeat'     : 'no-repeat'
                });
            });
        });
    });
});