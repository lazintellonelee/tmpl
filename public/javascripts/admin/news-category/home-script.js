base_url = $('meta[name=base-url]').attr('content');

$(document).ready(function() {
    $('input.bootstrap-toggle').bootstrapToggle({
        size        : 'small',
        on          : 'Hiển thị',
        off         : 'Ẩn'
    });
    
    var TimeChangeStatus    = null;
    var OldStatusValue      = null;
    $(document).on('change', 'input.bootstrap-toggle', function(event) {
        var that                = this;
        var news_category_id    = $(this).attr('data-news-category-id');
        var status              = $(this).prop('checked')?1:0;
        if(OldStatusValue === null) OldStatusValue  = status===1?0:1;
        clearTimeout(TimeChangeStatus);
        TimeChangeStatus = setTimeout(function() {
            if(OldStatusValue === status) {
                OldStatusValue = null;
                return false;
            }
            
            var http    = new XMLHttpRequest();
            var form    = new FormData();
            form.append('news_category_id', news_category_id);
            form.append('news_category_status', status);
            form.append('_token', $('meta[name=csrf-token]').attr('content'));
            http.open('POST', base_url + '/admin/news-category/change-status.html', true);
            http.onload = function(event)
            {
                HideLoadingAjax();
                var result = JSON.parse(this.responseText);
                if(result.error !== undefined)
                {
                    $(that).prop('checked', status===1?false:true).change();
                    CallNoty('warning', 'Cập nhật trạng thái không thành công! Vui lòng thử lại.');
                    return true;
                }
                OldStatusValue = null;
                CallNoty('success', 'Cập nhật trạng thái thành công!');
            };
            http.send(form);
            ShowLoadingAjax();
        }, 1000);
    });
    
    $('[data-toggle=toggle]').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-right':'none','border-bottom':'none'});
    $('label.toggle-on').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-left':'none','border-bottom':'none'});
    $('label.toggle-off').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-right':'none','border-bottom':'none'});
    
    $('input.icheck').iCheck({
        checkboxClass   : 'icheckbox_minimal-blue',
        radioClass      : 'iradio_minimal-blue'
    });
    
    var table = $('#table_news_categories').DataTable({
        "aoColumnDefs"  : [{
            "aTargets"  : [0, 3],
            "bSortable" : false
        }],
        "paging"            : false,
        "lengthChange"      : false,
        "searching"         : true,
        "ordering"          : true,
        "info"              : false,
        "autoWidth"         : false,
        "iDisplayLength"    :-1,
        'language'          : {
            'zeroRecords'   : '<p class="text-center">Không danh mục tin tức !!</p>',
            'lengthMenu'    : 'Hiển thị&nbsp; _MENU_',
            'search'        : 'Tìm _INPUT_ <button id="btn-delete-news-categories" class="btn btn-warning btn-sm no-radius"><i class="fa fa-fw fa-trash"></i></button>'
        }
    });
    
    $(document).on('ifChanged', '#check-all-news-category', function(event) {
        $('.check-news-category').iCheck($(this).prop('checked')?'check':'uncheck');
    });
    
    $(document).on('click', '#btn-delete-news-categories', function(event) {
        var arr = $('input.check-news-category:checked').map(function(index, element) {
            return $(this).val();
        }).get();
        if(arr.length === 0)
        {
            CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Bạn cần chọn ít nhất 1 danh mục tin tức');
            return true;
        }
        CallConfirmNoty('Bạn muốn xoá những danh mục tin tức này?', function() {
            DeleteNewsCategory(arr, function(result) {
                if(result.error !== undefined)
                {
                    CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Xoá danh mục không thành công! Vui lòng thử lại');
                    return true;
                }
                for(var i=0;i<arr.length;i++)
                {
                    table
                    .row($('tr[data-news-category-id=' + arr[i] + ']'))
                    .remove()
                    .draw(false);
                }
                CallNoty('success', '<i class="fa fa-check"></i> Xoá danh mục thành công.');
            });
        });
    });
    
    $(document).on('click', '.btn-delete-news-category', function(event) {
        var that                = $(this);
        var news_category_id    = that.attr('data-news-category-id');
        CallConfirmNoty('Bạn muốn xoá danh mục  ' + $.trim(that.parents().eq(1).find('td:nth-child(3)').html().replace(/\&nbsp\;/g,' ')) + '?', function(){
            DeleteNewsCategory([news_category_id], function(result) {
                if(result.error !== undefined)
                {
                    CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Xoá danh mục không thành công! Vui lòng thử lại');
                    return true;
                }
                table
                .row($('tr[data-news-category-id=' + news_category_id + ']'))
                .remove()
                .draw(false);
                CallNoty('success', '<i class="fa fa-check"></i> Xoá danh mục thành công.');
            });
        });
    });
});

var DeleteNewsCategory = function(arr, cb)
{
    var http = new XMLHttpRequest();
    var form = new FormData();
    for(var i=0;i<arr.length;i++)
    {
        form.append('news_category_id[]', arr[i]);
    }
    form.append('_token', $('meta[name=csrf-token]').attr('content'));
    http.open('POST', base_url + '/admin/news-category/delete.html', true);
    http.onload = function(event) {
        HideLoadingAjax();
        cb(JSON.parse(this.responseText));
    };
    http.send(form);
    ShowLoadingAjax();
};