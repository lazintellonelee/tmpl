base_url = $('meta[name=base-url]').attr('content');

var GetChart = function(cb)
{
    var http = new XMLHttpRequest();
    http.open('POST', base_url + '/admin/news-tag/get-data-chart.html', true);
    http.setRequestHeader('X-CSRF-TOKEN', $('meta[name=csrf-token]').attr('content'));
    http.onload = function(event)
    {
        cb(this.responseText);
    };
    http.send();
};

$(document).ready(function() {
    GetChart(function(response) {
        var data    = JSON.parse(response);
        var labels  = [];
        var yData   = [];
        
        for(var i=0; i<data.length; i++)
        {
            labels.push(data[i].news_tag_name);
            yData.push(data[i].news);
        }
        
        var datasets = [
            {
                fillColor : "rgb(51, 153, 255)",
                strokeColor : "rgb(51, 153, 255)",
                highlightFill: "rgb(51, 153, 255)",
                highlightStroke: "rgb(51, 153, 255)",
                data : yData
            }
        ];
        
        var ctx         = document.getElementById('chart-bar-news-in-tags').getContext("2d");
        window.myBar    = new Chart(ctx).Bar({
            labels      : labels,
            datasets    : datasets
        }, {
            responsive  : true,
            // String - Template string for single tooltips
            tooltipTemplate: "<%if (label){%>Số lượng tin tức trong thẻ '<%=label%>': <%}%><%= value %>"
        });
    });
});