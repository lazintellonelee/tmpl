base_url    = $('meta[name=base-url]').attr('content');
table       = null;

dataTableOptions = {
    "aoColumnDefs"  : [{
        "aTargets"  : [0, 3],
        "bSortable" : false
    }],
    "paging"            : true,
    "lengthChange"      : true,
    "searching"         : true,
    "ordering"          : true,
    "info"              : false,
    "autoWidth"         : false,
    'language'          : {
        'zeroRecords'   : '<p class="text-center">Không thẻ tags tin tức !!</p>',
        'lengthMenu'    : 'Hiển thị&nbsp; _MENU_',
        'search'        : 'Tìm _INPUT_ <button id="btn-delete-news-tags" class="btn btn-warning btn-sm no-radius"><i class="fa fa-fw fa-trash"></i></button>'
    }
};

var ReworkDataTable = function(table, options)
{
    table.destroy();
    return $('#table_news_tags').DataTable(options);
};

var DeleteNewsTag = function(arr, cb)
{
    var http = new XMLHttpRequest();
    var form = new FormData();
    for(var i=0;i<arr.length;i++)
    {
        form.append('news_tag_id[]', arr[i]);
    }
    form.append('_token', $('meta[name=csrf-token]').attr('content'));
    http.open('POST', base_url + '/admin/news-tag/delete.html', true);
    http.onload = function(event) {
        HideLoadingAjax();
        var result = JSON.parse(this.responseText);
        if(result.error !== undefined)
        {
            CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Xoá thẻ tag tin tức không thành công! Vui lòng thử lại.');
            return true;
        }
        cb(result);
    };
    http.send(form);
    ShowLoadingAjax();
};

$(document).ready(function() {
    $('input.icheck').iCheck({
        checkboxClass   : 'icheckbox_minimal-blue',
        radioClass      : 'iradio_minimal-blue'
    });
    
    $(document).on('ifChanged', '#check-all-news-tag', function(event) {
        $('.check-news-tag').iCheck($(this).prop('checked')?'check':'uncheck');
    });
    
    table = $('#table_news_tags').DataTable(dataTableOptions);
    
    $(document).on('click', '.btn-delete-news-tag', function() {
        event.preventDefault();
        var that        = $(this);
        var news_tag_id = $(this).attr('data-news-tag-id');
        CallConfirmNoty('Bạn muốn xoá tags ' + that.parents().eq(1).find('td:nth-child(3)').html() + '?', function() {
            DeleteNewsTag([news_tag_id], function(result) {
                table
                .row($('tr[data-news-tag-id=' + news_tag_id + ']'))
                .remove()
                .draw(false);
                CallNoty('success', '<i class="fa fa-check"></i> Xoá thẻ tin tức thành công.');
            });
        });
    });
    
    $(document).on('click', '#btn-delete-news-tags', function(event) {
        event.preventDefault();
        
        var arr = $('input.check-news-tag:checked').map(function(index, element) {
            return $(this).val();
        }).get();
        
        if(arr.length === 0)
        {
            CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Vui lòng chọn ít nhất 1 tag');
            return true;
        }
        CallConfirmNoty('Bạn muốn xoá những tags tin tức này?', function() {
            DeleteNewsTag(arr, function(result) {
                for(var i=0;i<arr.length;i++)
                {
                    table
                    .row($('tr[data-news-tag-id=' + arr[i] + ']'))
                    .remove()
                    .draw(false);
                }
                CallNoty('success', '<i class="fa fa-check"></i> Xoá thẻ tin tức thành công.');
            });
        });
    });
});