base_url = $('meta[name=base-url]').attr('content');

$(document).ready(function() {
    $(document).on('hidden.bs.modal', '#modal-edit-news-tag', function(event) {
        event.preventDefault();
        document.getElementById('form-edit-news-tag').reset();
    });
    
    $(document).on('click', '.btn-edit-news-tag', function(event) {
        event.preventDefault();
        var that        = $(this);
        var news_tag_id = that.attr('data-news-tag-id');
        var http        = new XMLHttpRequest();
        var form        = new FormData();
        var modal       = $('#modal-edit-news-tag');
        form.append('news_tag_id', news_tag_id);
        form.append('_token', $('meta[name=csrf-token]').attr('content'));
        http.open('POST', base_url + '/admin/news-tag/get.html', true);
        http.onload = function() 
        {
            HideLoadingAjax();
            var result = JSON.parse(this.responseText);
            if(result.error !== undefined) 
            {
                CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Xảy ra lỗi! Vui lòng thử lại.');
                return true;
            }
            var formEdit = new FormData(document.getElementById('form-edit-news-tag'));
            modal.find('div.modal-body').html(tmpl('tmpl-modal-body-edit-news-tag', result));
            modal.modal('show');
        };
        http.send(form);
        ShowLoadingAjax();
    });
    
    $(document).on('click', '#btn-edit-news-tag', function(event) {
        var http        = new XMLHttpRequest();
        var form        = new FormData(document.getElementById('form-edit-news-tag'));
        var news_tag_id = $(this).parents().eq(1).find('input[name=news_tag_id]').val();
        var modal       = $('#modal-edit-news-tag');
        form.append('_token', $('meta[name=csrf-token]').attr('content'));
        http.open('POST', base_url + '/admin/news-tag/edit.html', true);
        http.onload = function(event)
        {
            HideLoadingAjax();
            var result = JSON.parse(this.responseText);
            if(result.error !== undefined)
            {
                CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Cập nhật tag tin tức không thành công! Vui lòng thử lại');
                return true;
            }
            var tr      = $('tr[data-news-tag-id=' + news_tag_id + ']');
            result.stt  = parseInt(tr.find('td:nth-child(2)').html());
            tr.html(tmpl('tmpl-tr-edit-news-tag', result));
            CallNoty('success', '<i class="fa fa-check"></i> Cập nhật tag tin tức thành công.');
            
            $('input.icheck').iCheck({
                checkboxClass   : 'icheckbox_minimal-blue',
                radioClass      : 'iradio_minimal-blue'
            });
            
            modal.modal('hide');
        };
        http.send(form);
        ShowLoadingAjax();
    });
});