base_url = $('meta[name=base-url]').attr('content');

$(document).ready(function() {
    $(document).on('click', '#btn-call-modal-add-news-tag', function(event) {
        event.preventDefault();
        $('#modal-add-news-tag').modal('show');
    });
    
    $(document).on('hidden.bs.modal', '#modal-add-news-tag', function(event) {
        event.preventDefault();
        document.getElementById('form-add-news-tag').reset();
    });
    
    $(document).on('click', '#btn-add-news-tag', function(event) {
        event.preventDefault();
        var modal   = $('#modal-add-news-tag');
        var http    = new XMLHttpRequest();
        var form    = new FormData(document.getElementById('form-add-news-tag'));
        form.append('_token', $('meta[name=csrf-token]').attr('content'));
        http.open('POST', base_url + '/admin/news-tag/add.html', true);
        http.onload = function(event)
        {
            HideLoadingAjax();
            var result = JSON.parse(this.responseText);
            if(result.error !== undefined)
            {
                CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Thêm tag tin tức không thành công! Vui lòng thử lại.');
                return true;
            }
            modal.modal('hide');
            
            var rows    = table.rows().nodes();
            var index   = parseInt($(rows[rows.length - 1]).find('td:nth-child(2)').html()) + 1;
            if(rows.length === 0) index = 1;
            
            var row     = table.row.add([
                tmpl('tmpl-add-news-tag-td1', result),
                tmpl('tmpl-add-news-tag-td2', {'stt': index}),
                tmpl('tmpl-add-news-tag-td3', result),
                tmpl('tmpl-add-news-tag-td4', result),
                tmpl('tmpl-add-news-tag-td5', result)
            ]).draw().node();
            
            $(row).attr('data-news-tag-id', result.news_tag_id);
            
            table.page( 'last' ).draw(false);
            
            $('input.icheck').iCheck({
                checkboxClass   : 'icheckbox_minimal-blue',
                radioClass      : 'iradio_minimal-blue'
            });
            $(row).find('td').css({'vertical-align':'middle'});
            $(row).find('td:nth-child(2)').css({'width':'30px'});
            $(row).find('td:nth-child(4)').css({'width':'125px','text-align':'center'});
            $(row).find('td:nth-child(5)').css({'width':'150px'});
            
            CallNoty('success', '<i class="fa fa-fw fa-check"></i> Thêm tag tin tức thành công.');
        };
        http.send(form);
        ShowLoadingAjax();
    });
});