base_url    = $('meta[name=base-url]').attr('content');
table       = null;

dataTableOptions = {
    "aoColumnDefs"  : [{
        "aTargets"  : [0, 6],
        "bSortable" : false
    }],
    "paging"            : true,
    "lengthChange"      : true,
    "searching"         : true,
    "ordering"          : true,
    "info"              : false,
    "autoWidth"         : false,
    'language'          : {
        'zeroRecords'   : '<p class="text-center">Không email khuyến mãi !!</p>',
        'lengthMenu'    : 'Hiển thị&nbsp; _MENU_',
        'search'        : 'Tìm _INPUT_ <button id="btn-delete-emailpromotion" class="btn btn-warning btn-sm no-radius"><i class="fa fa-fw fa-trash"></i></button>'
    }
};

var DeleteEmailPromotion = function(arrID, cb)
{
    var http    = new XMLHttpRequest();
    var form    = new FormData();
    for(var i=0;i<arrID.length;i++)
    {
        form.append('emailpromotion_id[]', arrID[i]);
    }
    form.append('_token', $('meta[name=csrf-token]').attr('content'));
    http.open('POST', base_url + '/admin/emailpromotion/delete.html', true);
    http.onload = function(event) 
    {
        HideLoadingAjax();
        cb(JSON.parse(this.responseText));
    };
    http.send(form);
    ShowLoadingAjax();
};

$(document).ready(function() {
    $('[data-toggle=toggle]').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-right':'none','border-bottom':'none'});
    $('label.toggle-on').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-left':'none','border-bottom':'none'});
    $('label.toggle-off').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-right':'none','border-bottom':'none'});
    
    $('input.icheck').iCheck({
        checkboxClass   : 'icheckbox_minimal-blue',
        radioClass      : 'iradio_minimal-blue'
    });
    
    table = $('#table_emailpromotion').DataTable(dataTableOptions);
    
    table.on( 'order.dt search.dt', function () {
        table.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
    
    $(document).on('ifChanged', '#check-all-emailpromotion', function(event) {
        $('.check-emailpromotion').iCheck($(this).prop('checked')?'check':'uncheck');
    });
    
    $(document).on('click', '.btn-delete-emailpromotion', function(event) {
        var that    = $(this);
        var emailpromotion_id = that.attr('data-emailpromotion-id');
        CallConfirmNoty('Bạn muốn xoá email khuyến mãi ' + $.trim(that.parents().eq(1).find('td:nth-child(3)').html().replace(/\&nbsp\;/g,' ')).replace(/<img[^>]+\>/i, '').replace(/<i[^>]+\><\/i\>/i, '') + '?', function(){
            DeleteEmailPromotion([emailpromotion_id], function(result) {
                if(result.error !== undefined)
                {
                    CallNoty('warning', '<i class="fa fa-fw fa-exclamation-triangle"></i> Xoá email khuyến mãi không thành công! Vui lòng thử lại.');
                    return true;
                }
                CallNoty('success', '<i class="fa fa-fw fa-check"></i> Xoá email khuyến mãi thành công.');
                table
                .row($('tr[data-emailpromotion-id=' + emailpromotion_id + ']'))
                .remove()
                .draw(false);
            });
        });
    });
    
    $(document).on('click', '#btn-delete-emailpromotion', function(event) {
        var arrID = $('input.check-emailpromotion:checked').map(function(index, element) {
            return $(element).val();
        }).get();
        
        if(arrID.length === 0)
        {
            CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Bạn cần chọn ít nhất 1 email khuyến mãi.');
            return true;
        }
        
        CallConfirmNoty('Bạn muốn xoá những email khuyến mãi này?', function() {
            DeleteEmailPromotion(arrID, function(result) {
                if(result.error !== undefined)
                {
                    CallNoty('warning', '<i class="fa fa-fw fa-exclamation-triangle"></i> Xoá email khuyến mãi không thành công! Vui lòng thử lại.');
                    return true;
                }
                CallNoty('success', '<i class="fa fa-fw fa-check"></i> Xoá email khuyến mãi thành công.');
                for(var i=0;i<arrID.length;i++)
                {
                    table
                    .row($('tr[data-emailpromotion-id=' + arrID[i] + ']'))
                    .remove()
                    .draw(false);
                }
            });
        });
    });
});