base_url    = $('meta[name=base-url]').attr('content');

var GetDataForChartArticleViews = function(date, year) {
    
    var http = new XMLHttpRequest();
    http.open('POST', base_url + '/admin/article/get-data-chart-article-views.html', true);
    http.setRequestHeader('X-CSRF-TOKEN', $('meta[name=csrf-token]').attr('content'));
    
    var form = new FormData();
    if(date !== undefined)
    {
        form.append('date', date);
    }
    
    if(year !== undefined)
    {
        form.append('year', year);
    }
    
    http.onload = function(event)
    {
        var data    = JSON.parse(this.responseText);
        var labels  = [];
        var yData   = [];
        var total   = 0;
        for(var i=0; i<data.length; i++)
        {
            total = total + parseInt(data[i].article);
            labels.push(data[i].date);
            yData.push(data[i].article);
        }
        
        if(data.length === 0)
        {
            labels  = ['1','2','3','4','5','6','7','8','9','10','11','12'];
            yData   = [0,0,0,0,0,0,0,0,0,0,0,0];
        }
        
        $('#total-for-chart').html('tổng số bài viết: ' + total);
        
        var datasets = [
            {
                fillColor : "rgb(51, 153, 255)",
                strokeColor : "rgb(51, 153, 255)",
                highlightFill: "rgb(51, 153, 255)",
                highlightStroke: "rgb(51, 153, 255)",
                data : yData
            }
        ];
        
        var ctx         = document.getElementById('chart-bar-article-view').getContext("2d");
        window.myBar    = new Chart(ctx).Bar({
            labels      : labels,
            datasets    : datasets
        }, {
            responsive  : true,
            // String - Template string for single tooltips
            tooltipTemplate: "<%if (label){%>Số lượng bài viết trong ngày <%=label%>: <%}%><%= value %>"
        });
    };
    if(date !== undefined)
    {
        http.send(form);
        return true;
    }
    http.send();
};

$(document).ready(function() {
    GetDataForChartArticleViews();
    
    $(document).on('change', '#select-chart-article-views-date', function() {
        var date    = $(this).val();
        var year    = $('#select-chart-article-views-year').val();
        GetDataForChartArticleViews(date, year);
    });
    
    $(document).on('change', '#select-chart-article-views-year', function() {
        var year    = $(this).val();
        var date    = $('#select-chart-article-views-date').val();
        GetDataForChartArticleViews(date, year);
    });
});