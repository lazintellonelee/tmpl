base_url    = $('meta[name=base-url]').attr('content');
table       = null;

dataTableOptions = {
    "aoColumnDefs"  : [{
        "aTargets"  : [0, 3],
        "bSortable" : false
    }],
    "paging"            : true,
    "lengthChange"      : true,
    "searching"         : true,
    "ordering"          : true,
    "info"              : false,
    "autoWidth"         : false,
    'language'          : {
        'zeroRecords'   : '<p class="text-center">Không bài viết !!</p>',
        'lengthMenu'    : 'Hiển thị&nbsp; _MENU_',
        'search'        : 'Tìm _INPUT_ <button id="btn-delete-article" class="btn btn-warning btn-sm no-radius"><i class="fa fa-fw fa-trash"></i></button>'
    }
};

function formatState(state) {
    if (!state.id) {
        return state.text;
    }
    var $state = $(
        '<span>' + $.trim(state.text) + '</span>'
    );
    return $state;
};


var DeleteArticle = function(arrID, cb)
{
    var http    = new XMLHttpRequest();
    var form    = new FormData();
    for(var i=0;i<arrID.length;i++)
    {
        form.append('article_id[]', arrID[i]);
    }
    form.append('_token', $('meta[name=csrf-token]').attr('content'));
    http.open('POST', base_url + '/admin/article/delete.html', true);
    http.onload = function(event) 
    {
        HideLoadingAjax();
        cb(JSON.parse(this.responseText));
    };
    http.send(form);
    ShowLoadingAjax();
};

$(document).ready(function() {
    table = $('#table_article').DataTable(dataTableOptions);
    
    $('input.bootstrap-toggle').bootstrapToggle({
        size        : 'small',
        on          : 'Hiển thị',
        off         : 'Ẩn'
    });
    
    $('[data-toggle=toggle]').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-right':'none','border-bottom':'none'});
    $('label.toggle-on').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-left':'none','border-bottom':'none'});
    $('label.toggle-off').css({'border-radius':'0px','box-shadow':'none','border-top':'none','border-right':'none','border-bottom':'none'});
    
    $('input.icheck').iCheck({
        checkboxClass   : 'icheckbox_minimal-blue',
        radioClass      : 'iradio_minimal-blue'
    });
    
    
    
    $(document).on('ifChanged', '#check-all-article', function(event) {
        $('.check-article').iCheck($(this).prop('checked')?'check':'uncheck');
    });
    
    $(document).on('click', '.btn-delete-article', function(event) {
        var that    = $(this);
        var article_id = that.attr('data-article-id');
        CallConfirmNoty('Bạn muốn xoá bài viết ' + $.trim(that.parents().eq(1).find('td:nth-child(3)').html().replace(/\&nbsp\;/g,' ')).replace(/<img[^>]+\>/i, '').replace(/<i[^>]+\><\/i\>/i, '') + '?', function(){
            DeleteArticle([article_id], function(result) {
                if(result.error !== undefined)
                {
                    CallNoty('warning', '<i class="fa fa-fw fa-exclamation-triangle"></i> Xoá bài viết không thành công! Vui lòng thử lại.');
                    return true;
                }
                CallNoty('success', '<i class="fa fa-fw fa-check"></i> Xoá bài viết thành công.');
                table
                .row($('tr[data-article-id=' + article_id + ']'))
                .remove()
                .draw(false);
            });
        });
    });
    
    $(document).on('click', '#btn-delete-article', function(event) {
        var arrID = $('input.check-article:checked').map(function(index, element) {
            return $(element).val();
        }).get();
        
        if(arrID.length === 0)
        {
            CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Bạn cần chọn ít nhất 1 bài viết.');
            return true;
        }
        
        CallConfirmNoty('Bạn muốn xoá những bài viết này?', function() {
            DeleteArticle(arrID, function(result) {
                if(result.error !== undefined)
                {
                    CallNoty('warning', '<i class="fa fa-fw fa-exclamation-triangle"></i> Xoá bài viết không thành công! Vui lòng thử lại.');
                    return true;
                }
                CallNoty('success', '<i class="fa fa-fw fa-check"></i> Xoá bài viết thành công.');
                for(var i=0;i<arrID.length;i++)
                {
                    table
                    .row($('tr[data-article-id=' + arrID[i] + ']'))
                    .remove()
                    .draw(false);
                }
            });
        });
    });
    
    var ChangeStatusTimeout = null;
    var OldStatusValue      = null;
    $(document).on('change', '.change-status-article', function() {
        var that        = this;
        var article_id     = $(this).attr('data-article-id');
        var article_status = $(this).prop('checked')?1:0;
        if(OldStatusValue === null) OldStatusValue  = article_status===1?0:1;
        clearTimeout(ChangeStatusTimeout);
        ChangeStatusTimeout = setTimeout(function(){
            if(OldStatusValue === article_status) {
                OldStatusValue = null;
                return false;
            }
            
            var http        = new XMLHttpRequest();
            var form        = new FormData();
            form.append('article_id', article_id);
            form.append('article_status', article_status);
            form.append('_token', $('meta[name=csrf-token]').attr('content'));
            http.open('POST', base_url + '/admin/article/change-status.html', true);
            http.onload = function(event)
            {
                HideLoadingAjax();
                var result = JSON.parse(this.responseText);
                if(result.error !== undefined)
                {
                    $(that).prop('checked', article_status===1?false:true).change();
                    CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> Cập nhật trạng thái bài viết không thành công! Vui lòng thử lại.');
                    return true;
                }
                OldStatusValue = null;
                CallNoty('success', '<i class="fa fa-fw fa-check"></i> Cập nhật trạng thái bài viết thành công.');
            };
            http.send(form);
            ShowLoadingAjax();
        }, 1000);
    }); 
    
    $(document).on('click', '.change-verified', function(event) {
        event.preventDefault();
        var that            = this;
        var article_id         = $(this).attr('data-article-id');
        var article_verified   = parseInt($(this).attr('data-article-verified'));
        CallConfirmNoty('Bạn muốn ' + (article_verified===0?'xác thực':'huỷ xác thực') + ' bài viết này?', function() {
            var http    = new XMLHttpRequest();
            var form    = new FormData();
            form.append('article_id', article_id);
            form.append('article_verified', article_verified===0?'1':'0');
            form.append('_token', $('meta[name=csrf-token]').attr('content'));
            http.open('POST', base_url + '/admin/article/change-verified.html', true);
            http.onload = function(event)
            {
                console.log(this.responseText);
                HideLoadingAjax();
                var result = JSON.parse(this.responseText);
                if(result.error !== undefined)
                {
                    CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> ' + (article_verified===0?'Xác thực':'Gỡ xác thực') + ' bài viết không thành công! Vui lòng thử lại.');
                    return true;
                }
                $(that).removeClass(article_verified===0?'bg-info':'bg-green');
                $(that).addClass(article_verified===0?'bg-green':'bg-info');
                $(that).find('i').removeClass(article_verified===0?'fa-times':'fa-check');
                $(that).find('i').addClass(article_verified===0?'fa-check':'fa-times');
                $(that).attr('data-article-verified', article_verified===0?1:0);
                CallNoty('success', '<i class="fa fa-check"></i> ' + (article_verified===0?'Xác thực':'Gỡ xác thực') + ' bài viết thành công!');
                return true;
            };
            http.send(form);
            ShowLoadingAjax();
        });
    });
});