function formatState(state) {
    if (!state.id) {
        return state.text;
    }
    var $state = $(
        '<span>' + $.trim(state.text) + '</span>'
    );
    return $state;
};

base_url = $('meta[name=base-url]').attr('content');

$(document).ready(function() {
    CKEDITOR.replace('article_content', {
        height  : 275,
        toolbar : [
            {
                name    : 'document',
                items   : ['Source', 'Styles', 'Format', 'Font', 'FontSize']
            },
            {
                name    : 'basicstyles',
                items   : ['Undo', 'Redo', '-', 'Bold', 'Italic', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Image', 'oembed', '-', 'Maximize']
            }
        ]
    });
    
    $(document).on('change', '#article_thumbnail', function(event) {
        readFileInput(this, function(e) {
            var img         = e.target.result;
            var container   = $('#div-article-thumbnail-review');
            container.fadeIn(500, function() {
                container.css({
                    'background-image' : 'url(\'' + img + '\')'
                });
            });
        });
    });
    
    $('#btn-crop-article-thumbnail').on('click', function(event) {
        var modal   = $('#modal-container-cropper-master');
        var input   = document.getElementsByName('article_thumbnail')[0];
        if(input.files.length === 0)
        {
            CallNoty('warning', '<i class="fa fa-fw fa-exclamation-triangle"></i> Bạn vui lòng tải hình ảnh lên!');
            return true;
        }
        var image   = $('#img-cropper-master');
        readFileInput(input, function(e) {
            image.attr('src', e.target.result);
            image.cropper({
                aspectRatio         : 6 / 3,
                minContainerWidth   : parseInt(modal.find('.modal-dialog').width()) - 30,
                minContainerHeight  : 450
            });
            modal.modal('show');
        });
    });
    
    $(document).on('click', '#btn-crop-image', function(event) {
        var container   = $('#div-article-thumbnail-review');
        var input       = $('input[name=article_thumbnail_base64]');
        var modal       = $('#modal-container-cropper-master');
        var image       = $('#img-cropper-master'); 
        var data        = image.cropper('getCroppedCanvas').toDataURL();
        container.css({'background-image':'url(\'' + data + '\')'});
        modal.modal('hide');
        input.val(data);
    });
    
    $(document).on('hidden.bs.modal', '#modal-container-cropper-master', function() {
        var image   = $('#img-cropper-master'); 
        image.cropper('destroy');
        image.attr('src', '');
    });
    
    $(document).on('click', '.change-formality-article-thumbnail', function(event) {
        event.preventDefault();
        var formality   = $(this).attr('data-formality');
        var button      = $(this).parents().eq(2).find('button');
        var input       = $(this).parents().eq(3).find('input[id=article_thumbnail]');
        var span        = $(this).parents().eq(3).find('span');
        switch(formality) {
            case 'url': 
                button.html('Tải lên <span class="caret"></span>');
                input.attr('type', 'file');
                input.attr('name', 'article_thumbnail');
                span.removeClass('hidden');
                break;
            case 'server':
                button.html('Liên kết ngoài <span class="caret"></span>');
                $(this).parents().eq(3).find('input[name=article_thumbnail_base64]').val('');
                $('#div-article-thumbnail-review').css({'display':'none'});
                input.attr('type', 'text');
                input.attr('name', 'article_thumbnail_url');
                span.addClass('hidden');
                break;
        }
        $('.change-formality-article-thumbnail').not(this).parent().removeClass('active');
        $(this).parent().addClass('active');
    });
});