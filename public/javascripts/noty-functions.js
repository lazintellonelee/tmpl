var CallConfirmNoty = function(text, ok_cb, cancel_cb)
{
    return noty({
        text    : text,
        layout  : 'topCenter',
        theme   : 'relax',
        modal   : true,
        buttons : [
            {addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    $noty.close();
                    ok_cb();
                }
            },
            {addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                    if(cancel_cb !== undefined)
                        cancel_cb();
                }
            }
        ]
    });
};

var CallNoty = function(type, text, layout)
{
    return noty({
        text        : text,
        layout      : (layout!==undefined?layout:'bottomRight'),
        type        : type,
        theme       : 'relax',
        timeout     : 3000,
        maxVisible  : 2,
        killer      : true,
        animation   : {
            open        : 'animated tada', // Animate.css class names
            close       : 'animated fadeOut', // Animate.css class names
            easing      : 'swing', // easing
            speed       : 500 // opening & closing animation speed
        }
    });
};