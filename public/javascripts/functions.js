function readFileInput(input, cb) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            cb(e);
        };
        reader.readAsDataURL(input.files[0]);
    }
};

function ShowLoadingAjax()
{
    var element = document.getElementById('loading-ajax');
    element.style.display   = 'block';
    element.style.zIndex    = '99999';
}

function HideLoadingAjax()
{
    var element = document.getElementById('loading-ajax');
    element.style.display   = 'none';
    element.style.zIndex    = '-1';
}